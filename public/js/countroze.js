'use strict';
(function($) {
	$.fn.countroze = function(limit, options) {
		try {
			if(limit === undefined || typeof(limit) !== 'number') {
				throw 'Limit Required';
			}
			if(options) {
				options.initial = parseFloat(options.initial);
				options.step = parseFloat(options.step);
				options.speed = parseFloat(options.speed);

				if(isNaN(options.initial)) {
					console.log('Initial value must be a int or float type. Taking default value instead');
					delete options.initial;
				}
				if(isNaN(options.step)) {
					console.log('Step value must be a int or float type. Taking default value instead');
					delete options.step;
				}
				if(isNaN(options.speed)) {
					console.log('Speed value must be a int or float type. Taking default value instead');
					delete options.speed;
				}
			}

			var settings = {
				'initial' : 0,
				'step': 1,
				'speed': 1
			};

			if(options) {
				$.extend(settings, options);
			}
			var element = $(this);
			var counter = settings.initial;
			var timer = setInterval(function () {
				element.text(counter);
				counter += settings.step;
				if(counter > limit) {
					element.text(limit);
					clearInterval(timer);
				}
			}, settings.speed);
		}catch(err) {
			console.error(err);
		}
	}
})(jQuery);