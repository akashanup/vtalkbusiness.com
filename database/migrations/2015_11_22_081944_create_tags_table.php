<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table  -> increments('id');
            $table  -> string('tags');
            $table  -> timestamps();
        });
        DB::table('tags')->insert([['tags' => 'Distribution'], ['tags' => 'Franchise'], ['tags' => 'Sell Business'], ['tags' => 'Buy Business'], ['tags' => 'Merger And Acquisition'], ['tags' => 'Sell Products']]);
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tags');
    }
}
