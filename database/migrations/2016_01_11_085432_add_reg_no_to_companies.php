<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegNoToCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->integer('registration_type')->unsigned()->default(1);
            $table->integer('evaluation_currency_type')->unsigned()->default(1);
            $table->string('website');

            $table->foreign('registration_type')->references('id')->on('registration_no_types')->onDelete('cascade');
            $table->foreign('evaluation_currency_type')->references('id')->on('currencies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
