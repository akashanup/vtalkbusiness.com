<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpportunityCompanyRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunity_company_relationships', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  integer('opportunity_id')->unsigned();
            $table  ->  integer('company_id')->unsigned();
            $table  ->  timestamps();
            $table  ->  foreign('opportunity_id')->references('id')->on('opportunities')->onDelete('cascade');
            $table  ->  foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opportunity_company_relationships');
    }
}
