<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->text('tagline')->default('');
            $table->string('logo')->default('default_company_logo.jpg');
            $table->string('cover')->default('default_company_cover.jpg');
            $table->text('description')->default('');
            $table->text('facebook')->default('');
            $table->text('linkedin')->default('');
            $table->text('twitter')->default('');
            $table->text('video')->default('');
            $table->integer('ban')->default(0);
            $table->integer('deleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('tagline');
            $table->dropColumn('logo');
            $table->dropColumn('cover');
            $table->dropColumn('description');
            $table->dropColumn('facebook');
            $table->dropColumn('linkedin');
            $table->dropColumn('twitter');
            $table->dropColumn('video');
            $table->dropColumn('ban');
            $table->dropColumn('deleted');
        });
    }
}
