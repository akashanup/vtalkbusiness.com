<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpportunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunities', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  string('headline');
            $table  ->  string('description');
            $table  ->  integer('capital');
            $table  ->  integer('revenue');
            $table  ->  string('logo')->default('default_company_logo.jpg');
            $table  ->  integer('status');
            $table  ->  timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opportunities');
    }
}
