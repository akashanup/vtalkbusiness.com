<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTagsRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_tags_relationships', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  integer('company_id')->unsigned();
            $table  ->  integer('tags')->unsigned();
            $table  ->  timestamps();

            $table  ->  foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table  ->  foreign('tags')->references('id')->on('tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_tags_relationships');
    }
}
