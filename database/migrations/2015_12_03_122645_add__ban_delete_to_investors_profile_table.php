<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBanDeleteToInvestorsProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investor_profiles', function (Blueprint $table) {
            $table->integer('ban')->default(0);
            $table->integer('deleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investor_profiles', function (Blueprint $table) {
            $table->dropColumn('ban');
            $table->dropColumn('deleted');
        });
    }
}
