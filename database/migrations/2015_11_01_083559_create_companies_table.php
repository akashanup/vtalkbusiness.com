<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table          ->      increments('id');
            $table          ->      integer('user_id')->unsigned();
            $table          ->      string('name');
            $table          ->      string('email');
            $table          ->      string('company_cin');
            $table          ->      string('phone');
            $table          ->      string('designation');
            $table          ->      string('evaluation');
            $table          ->      timestamps();
            $table          ->      foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}



