<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToInvestor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investor_profiles', function (Blueprint $table) {
            $table->string('investor_name');
            $table->string('email');
            $table->string('phone');
            $table->string('skype');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('linkedin');
            $table->string('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investor_profiles', function (Blueprint $table) {
            $table->dropColumn('investor_name');
            $table->dropColumn('email');
            $table->dropColumn('phone');
            $table->dropColumn('skype');
            $table->dropColumn('facebook');
            $table->dropColumn('twitter');
            $table->dropColumn('linkedin');
            $table->dropColumn('image');
        });
    }
}
