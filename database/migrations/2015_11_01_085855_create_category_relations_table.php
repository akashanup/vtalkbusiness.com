<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_relations', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  integer('category_id')->unsigned();
            $table  ->  integer('company_id')->unsigned();
            $table  ->  timestamps();

            $table  ->  foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table  ->  foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category_relations');
    }
}
