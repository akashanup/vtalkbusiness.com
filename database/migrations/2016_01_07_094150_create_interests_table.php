<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('interested_table');
            $table->timestamps();
        });

        DB::table('interests')->insert([
            ['interested_table' => 'users'],
            ['interested_table' => 'companies'],
            ['interested_table' => 'investor_profiles'],
            ['interested_table' => 'company_products'],
            ['interested_table' => 'opportunities']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('interests');
    }
}
