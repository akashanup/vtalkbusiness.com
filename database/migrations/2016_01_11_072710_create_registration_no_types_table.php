<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationNoTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration_no_types', function (Blueprint $table) {
            $table-> increments('id');
            $table-> string('registration_type');
            $table-> timestamps();
        });

        DB::table('registration_no_types')->insert([
            ['registration_type' => 'CIN'], ['registration_type' => 'ACRA'], ['registration_type' => 'Others']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('registration_no_types');
    }
}
