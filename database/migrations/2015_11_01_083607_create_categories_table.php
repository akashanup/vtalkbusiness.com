<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  string('category');
            $table  ->  timestamps();
        });

        DB::table('categories')->insert([
    ['category' => 'Advertising & Media Services'],
    ['category' => 'Agriculture'],
    ['category' => 'Architectural Design Services'],
    ['category' => 'Audio & Video'],
    ['category' => 'AusAid & NZAid Tenders'],
    ['category' => 'Aviation & Related Services'],
    ['category' => 'Building Demolition & Removal'],
    ['category' => 'Business Professional Services'],
    ['category' => 'Civil Engineering & Construction'],
    ['category' => 'Cleaning'],
    ['category' => 'Clothing & Textiles & Footwear'],
    ['category' => 'Construction'],
    ['category' => 'Education Services'],
    ['category' => 'Electrical & Cabling'],
    ['category' => 'Engineering Services'],
    ['category' => 'Environmental / Heritage / Conservation'],
    ['category' => 'Equipment Disposal'],
    ['category' => 'Event Management'],
    ['category' => 'Facilities Management'],
    ['category' => 'Fencing'],
    ['category' => 'Financial Services'],
    ['category' => 'Fisheries'],
    ['category' => 'Forestry'],
    ['category' => 'Fuels & Oils'],
    ['category' => 'Grounds Maintenance'],
    ['category' => 'Health & Safety Management'],
    ['category' => 'Horticulture'],
    ['category' => 'Hospitality & FMCG'],
    ['category' => 'Household Goods'],
    ['category' => 'Industrial Chemicals'],
    ['category' => 'Information Technology'],
    ['category' => 'Landscaping'],
    ['category' => 'Machinery & Equipment'],
    ['category' => 'Marketing Services'],
    ['category' => 'Materials Handling Equipment'],
    ['category' => 'Mechanical Services & Equipment'],
    ['category' => 'Medical & Healthcare'],
    ['category' => 'Miscellaneous Equipment'],
    ['category' => 'Office Supplies & Equipment'],
    ['category' => 'Packaging & Paper Supplies'],
    ['category' => 'Painting & Corrosion'],
    ['category' => 'Pest Control'],
    ['category' => 'Photography'],
    ['category' => 'Pipe Supply'],
    ['category' => 'Plant & Labour Hire'],
    ['category' => 'Printing'],
    ['category' => 'Property & Realty Services'],
    ['category' => 'Public Private Partnerships (PPPs)'],
    ['category' => 'Quarrying / Oil / Mining'],
    ['category' => 'Recruitment & HR Services'],
    ['category' => 'Scientific & Research'],
    ['category' => 'Security & Fire'],
    ['category' => 'Shipping'],
    ['category' => 'Sign Manufacturing & Signwriting'],
    ['category' => 'Social Services'],
    ['category' => 'Telecommunications'],
    ['category' => 'Training & Development'],
    ['category' => 'Transportation & Logistics'],
    ['category' => 'Utilities - Energy & Water'],
    ['category' => 'Valuation Services'],
    ['category' => 'Vehicles & Heavy Plant'],
    ['category' => 'Waste Management & Landfill'],
    ['category' => 'Water & Sewage'],
]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
