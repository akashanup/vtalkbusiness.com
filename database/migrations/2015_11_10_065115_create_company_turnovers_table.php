<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTurnoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_turnovers', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  integer('company_id')->unsigned();
            $table  ->  decimal('turnover', 11, 3);
            $table  ->  string('year');
            $table  ->  timestamps();

            $table  ->  foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_turnovers');
    }
}
