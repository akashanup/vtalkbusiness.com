<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpportunityTagsRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunity_tags_relationships', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  integer('opportunity_id')->unsigned();
            $table  ->  integer('tags')->unsigned();
            $table  ->  timestamps();
            $table  ->  foreign('opportunity_id')->references('id')->on('opportunities')->onDelete('cascade');
            $table  ->  foreign('tags')->references('id')->on('tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opportunity_tags_relationships');
    }
}
