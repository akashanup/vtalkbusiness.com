<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpportunityCountriesRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunity_countries_relationships', function (Blueprint $table) {
            $table  ->increments('id');
            $table  ->  integer('opportunity_id')->unsigned();
            $table  ->  integer('country_id')->unsigned();
            $table  ->  timestamps();
            $table  ->  foreign('opportunity_id')->references('id')->on('opportunities')->onDelete('cascade');
            $table  ->  foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opportunity_countries_relationships');
    }
}
