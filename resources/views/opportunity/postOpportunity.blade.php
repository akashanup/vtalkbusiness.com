@extends('master')

@section('title')
<title>Post Opportunity</title>
@stop

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jcrop.css') }}">
@stop

@section('breadcrumb')
<div class="ct-site--map">
	<div class="container">
		<a href="{{ url('/dashboard') }}">Dashboard</a>
		<a href="{{ url('/opportunity') }}">Opportunity</a>
	</div>
</div>
@stop



@section('content')
<section class="ct-u-paddingTop50 ct-u-paddingBottom100">
	<div class="container">
		<div class="ct-headerText--normal text-uppercase ct-u-marginBottom40">
			<h2>Post Opportunity<br>
				<span class="ct-text--highlightGray">Opportunity Details</span>
			</h2>
		</div>
		<div class="row">
			@if(count($errors))
				@foreach($errors->all() as $error)
				<div class="errorMessage alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{$error}}
				</div>
				@endforeach
			@endif

			<form method="post"  id="frm" action="{{url('/opportunity')}}" method="POSTx" enctype="multipart/form-data" >
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				
				<div class="col-md-12 form-group">
					<label>Opportunity Headline</label>
					<input type="text" name="headline" class="form-control street custom-control">
				</div>
				<div class="col-md-12 form-group">
					<label>Opportunity Description</label>
					<textarea id="description" cols="30" rows="5" class="form-control custom-control" name="description"></textarea>
				</div>
				<div class="col-md-4 form-group">
					<label>Opportunity Picture</label>
					<span class="btn btn-default">
						<input id="logo-img" type="file" name="logo">
					</span>
				</div>
				<div class="col-md-4 form-group">
					<label>Capital Required</label>
					<input type="number" min="100000" max="1000000" name="capital" class="form-control street custom-control">
				</div>
				<div class="col-md-4 form-group">
					<label>Expected Monthly Revenue</label>
					<input type="number" min="0" max="1000000" name="revenue" class="form-control street custom-control">
				</div>
				
				<div class="col-md-4 form-group">
					<label>Opportunity By Company</label>
					<select class="" name="companies[]" id="companies" multiple placeholder="Select Company">
						@foreach($companies as $company)
							<option value="{{$company->id}}">{{$company->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-4 form-group">
					<label>Opportunity Tags</label>
					<select class="" name="tags[]" id="tags" multiple placeholder="Select Tags">
						@foreach($tags as $tag)
							<option value="{{$tag->id}}">{{$tag->tags}}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-4 form-group">
					<label>Opportunity Location</label>
					<select class="" name="country[]" id="location" multiple placeholder="Location">
						@foreach($countries as $country)
							<option value="{{$country->id}}">{{$country->country}}</option>
						@endforeach
					</select>
				</div>				
				<div class="form-group col-md-12 button">
					<button type="submit" id="submit" class="btn btn-info btn-block">Post</button>
				</div>
			</form>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('select').select2();
	});
</script>
@stop