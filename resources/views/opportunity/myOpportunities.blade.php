@extends('master')

@section('title')
<title>My Opportunities</title>
@stop

@section('breadcrumb')
<!-- BreadCrumbs -->
<div class="ct-site--map">
    <div class="container">
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <a href="{{ url('/my-opportunities') }}">My Opportunities</a>
    </div>
</div>
<!-- BreadCrumb Ends -->
@stop

@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
    <div class="ct-mediaSection-inner">
        <div class="container">
			<div class="ct-heading--main text-center">
			    <h3 class="text-uppercase ct-u-text--white">My Opportunities</h3>
			</div>
        </div>
    </div>
</header>
@stop

@section('content')
<section class="ct-u-paddingBoth70 ct-js-section text-left">
    <div class="container">
		<div class="col-md-10 col-md-offset-1">
			@if(count($opportunities))
				<p><a class="btn btn-primary btn-sm" href="{{ url('/opportunity') }}" role="button"><i class="fa fa-plus-circle"></i> Post an Oppotunity</a></p>
				<div class="company-container">
					<h3>Your Opportunities</h3>
					<ul>
						<br>
						@foreach($opportunities as $c)
							<a href="" class="details" data-id="{{ $c->id }}"><li>{{$c->headline}}</li></a>
						@endforeach
					</ul>

				</div>
			@endif
		</div>
	</div>
</section>
@stop

@section('modals')
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="opportunity-description-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                    Opportunity Description
                </h4>
            </div>
            <div class='modal-body' id="modalBody">
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('select').select2();

		$(".modal").on("hidden.bs.modal", function(){
		    $("#modalBody").html("");
		});

		var data 		= 	{'_token':'{{csrf_token()}}'};
		

		$('.details').on('click',function(e){
			e.preventDefault();
	    
	    	$.ajax({
				type:'POST',
				url: "{{ url('/opportunity-details')}}/" + $(this).data('id'),
				data : 	data,
	            success: function(data){
	            	var html ="";
	            	html=				
	            				"<div class='row'>"+
	            					"<div class='col-md-4'>"+
	            						"<h4>Headline</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
	            						"<h4>"+data.opportunity.headline+"</h4>"+
            						"</div>"+
								"</div>"+

								"<div class='row'>"+
									"<div class='col-md-4'>"+
	            						"<h4>Description</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
	            						"<h4>"+data.opportunity.description+"</h4>"+
            						"</div>"+
								"</div>"+
								
								"<div class='row'>"+
									"<div class='col-md-4'>"+
	            						"<h4>Capital</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
	            						"<h4>"+data.opportunity.capital+"</h4>"+
            						"</div>"+
								"</div>"+
								
								"<div class='row'>"+
									"<div class='col-md-4'>"+
	            						"<h4>Revenue</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
	            						"<h4>"+data.opportunity.revenue+"</h4>"+
            						"</div>"+
								"</div>"+

								"<div class='row'>"+
									"<div class='col-md-4'>"+
	            						"<h4>Location</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
            							"<ul>";
	            							for(var i=0;i<data.country.length;i++)
	            							{
	            								html+="<li><h4>"+data.country[i].country+"</h4></li>";
	            							}
            							html+="</ul>"+
            						"</div>"+
								"</div>"+
								"<div class='row'>"+
									"<div class='col-md-4'>"+
	            						"<h4>Company</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
            							"<ul>";
	            							for(var i=0;i<data.company.length;i++)
	            							{
	            								html+="<li><a href='{{ url('/comapny') }}/" + data.company[i].company_id + "'><h4>"+data.company[i].company_name+"</h4></a></li>";
	            							}
            							html+="</ul>"+
            						"</div>"+
								"</div>"+
								"<div class='row'>"+
									"<div class='col-md-4'>"+
	            						"<h4>Tags</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
            							"<ul>";
	            							for(var i=0;i<data.tags.length;i++)
	            							{
	            								html+="<li><h4>"+data.tags[i].tags+"</h4></li>";
	            							}
            							html+="</ul>"+
            						"</div>"+
								"</div>"+
								"<div class='row'>"+
									"<div class='col-md-8 col-md-offset-4'>";
										if(parseInt(data.interested.current)) {
											html += "<button class='btn btn-info ct-u-text--blue vtalk-interested-button' data-entity-id='" + data.opportunity.id + "' data-interested='" + data.interested.current + "' data-entity-type='" + data.interested.entity_type + "'><i class='fa fa-star'></i> Not Interested</button>";
										}else {
											html += "<button class='btn btn-info ct-u-text--blue vtalk-interested-button' data-entity-id='" + data.opportunity.id + "' data-interested='" + data.interested.current + "' data-entity-type='" + data.interested.entity_type + "'><i class='fa fa-star-o'></i> Interested</button>";

										}
									"</div>"+
								"</div>";


	            	$('#modalBody').append(html);
	            	$('#opportunity-description-modal').modal('toggle');
					$('#opportunity-description-modal').modal('show');
					initInterested();
	            },
	            error:function(){
				}
			});
		});
	});
</script>
@stop