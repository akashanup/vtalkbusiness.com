@extends('master')

@section('title')
<title>Post Opportunity</title>
@stop

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jcrop.css') }}">
@stop

@section('breadcrumb')
<div class="ct-site--map">
	<div class="container">
		<a href="{{ url('/dashboard') }}">Dashboard</a>
		<a href="{{ url('/opportunity') }}">Opportunity</a>
	</div>
</div>
@stop



@section('content')
<section class="ct-u-paddingTop50 ct-u-paddingBottom100">
	<div class="container">
		<div class="ct-headerText--normal text-uppercase ct-u-marginBottom40">
			<h2>Opportunities<br>
			</h2>
		</div>
		<div class="row">
			@if(count($errors))
				@foreach($errors->all() as $error)
				<div class="errorMessage alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{$error}}
				</div>
				@endforeach
			@endif

		</div>
		<div class="row">
			<table class="table table responsive">
				<thead>
					<th>S.No.</th>
					<th>Headline</th>
					<th>Opportunity By Company</th>
					<th>Option</th>
				</thead>
				<span class="hidden" value="{{$i=1}}">
				@foreach($opportunities as $opportunity)
				<tbody>
					<td>{{$i++}}</td>
					<td>{{$opportunity->headline}}</td>
					<td><a href="{{ url('/company/'.$opportunity->company_id) }}">{{$opportunity->company_name}}</a></td>
					<td><a data-id="{{$opportunity->id}}" class="btn btn-success details">View Details</a></td>
				</tbody>
				@endforeach
			</table>
		</div>
</section>
@stop

@section('modals')
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="opportunity-description-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                    Opportunity Description
                </h4>
            </div>
            <div class='modal-body' id="modalBody">
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('select').select2();

		$(".modal").on("hidden.bs.modal", function(){
		    $("#modalBody").html("");
		});

		var data 		= 	{'_token':'{{csrf_token()}}'};
		

		$('.details').on('click',function(){
	    
	    	$.ajax({
				type:'POST',
				url: "{{ url('/opportunity-details')}}/" + $(this).data('id'),
				data : 	data,
	            success: function(data){
	            	var html = "<div class='row'>"+
	            					"<div class='col-md-4'>"+
	            						"<h4>Headline</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
	            						"<h4>"+data.opportunity.headline+"</h4>"+
            						"</div>"+
								"</div>"+

								"<div class='row'>"+
									"<div class='col-md-4'>"+
	            						"<h4>Description</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
	            						"<h4>"+data.opportunity.description+"</h4>"+
            						"</div>"+
								"</div>"+
								
								"<div class='row'>"+
									"<div class='col-md-4'>"+
	            						"<h4>Capital</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
	            						"<h4>"+data.opportunity.capital+"</h4>"+
            						"</div>"+
								"</div>"+
								
								"<div class='row'>"+
									"<div class='col-md-4'>"+
	            						"<h4>Revenue</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
	            						"<h4>"+data.opportunity.revenue+"</h4>"+
            						"</div>"+
								"</div>"+

								"<div class='row'>"+
									"<div class='col-md-4'>"+
	            						"<h4>Location</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
            							"<ul>";
	            							for(var i=0;i<data.country.length;i++)
	            							{
	            								html+="<li><h4>"+data.country[i].country+"</h4></li>";
	            							}
            							html+="</ul>"+
            						"</div>"+
								"</div>"+
								"<div class='row'>"+
									"<div class='col-md-4'>"+
	            						"<h4>Company</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
            							"<ul>";
	            							for(var i=0;i<data.company.length;i++)
	            							{
	            								html+="<li><a href='{{ url('/comapny') }}/" + data.company[i].company_id + "'><h4>"+data.company[i].company_name+"</h4></a></li>";
	            							}
            							html+="</ul>"+
            						"</div>"+
								"</div>"+
								"<div class='row'>"+
									"<div class='col-md-4'>"+
	            						"<h4>Tags</h4>"+
            						"</div>"+
            						"<div class='col-md-8'>"+
            							"<ul>";
	            							for(var i=0;i<data.tags.length;i++)
	            							{
	            								html+="<li><h4>"+data.tags[i].tags+"</h4></li>";
	            							}
            							html+="</ul>"+
            						"</div>"+
								"</div>"+
								"<div class='row'>"+
									"<div class='col-md-8 col-md-offset-4'>";
										if(parseInt(data.interested.current)) {
											html += "<button class='btn btn-info ct-u-text--blue vtalk-interested-button' data-entity-id='" + data.opportunity.id + "' data-interested='" + data.interested.current + "' data-entity-type='" + data.interested.entity_type + "'><i class='fa fa-star'></i> Not Interested</button>";
										}else {
											html += "<button class='btn btn-info ct-u-text--blue vtalk-interested-button' data-entity-id='" + data.opportunity.id + "' data-interested='" + data.interested.current + "' data-entity-type='" + data.interested.entity_type + "'><i class='fa fa-star-o'></i> Interested</button>";

										}
									"</div>"+
								"</div>";


	            	$('#modalBody').append(html);
	            	$('#opportunity-description-modal').modal('toggle');
					$('#opportunity-description-modal').modal('show');
					initInterested();
	            },
	            error:function(){
				}
			});
		});
	});
</script>
@stop


