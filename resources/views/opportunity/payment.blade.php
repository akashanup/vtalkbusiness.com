@extends('master')

@section('title')
<title>Payment</title>
@stop

@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
    <div class="ct-mediaSection-inner">
        <div class="container">
			<div class="ct-heading--main text-center">
			    <h3 class="text-uppercase ct-u-text--white">Make your Payment</h3>
			</div>
        </div>
    </div>
</header>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <br>
                <form method="post"  id="frm" action="{{url('/make-payment/'.$opportunity_id)}}" method="POST" enctype="multipart/form-data" >
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group col-md-12 button">
                        <button type="submit" id="submit" class="btn btn-info btn-block">Make Payment</button>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <br>
                <br>
            </div>
        </div>
    </div>
@stop
