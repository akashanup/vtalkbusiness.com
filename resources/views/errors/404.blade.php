@extends('master')

@section('title')
<title>Future Scope</title>
@stop

@section('breadcrumb')
<!-- BreadCrumbs -->
<div class="ct-site--map">
	<div class="container">
		<a href="index.html">Home</a>
		<a href="features-buttons.html">Future</a>
	</div>
</div>
<!-- BreadCrumb Ends -->
@stop

@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
	<div class="ct-mediaSection-inner">
		<div class="container">
			<div class="ct-heading--main text-center">
				<h3 class="text-uppercase ct-u-text--white">Coming Soon</h3>
			</div>
		</div>
	</div>
</header>
@stop

@section('content')
<section class="ct-u-paddingBoth70 ct-js-section text-center">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-3 ct-u-marginBottom60">
				<a href="{{ url('/feeds') }}">
					<div class="ct-iconBox">
						<div class="ct-icon ct-iconContainer--circle center-block ct-u-marginBottom30">
							<i class="fa fa-line-chart"></i>
						</div>
						<div class="ct-iconBox--description">
							<a href="{{ url('/feeds') }}"><span class="ct-title ct-u-marginBottom20 ct-fw-600">Business Updates</span></a>
							<!-- <span class="ct-text ct-u-marginBottom20">Estato is designed a way that it was flexible for different types of business.</span> -->
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-3 ct-u-marginBottom60">
				<a href="#enquiry-modal" data-target="#enquiry-modal" data-toggle="modal">
					<div class="ct-iconBox">
						<div class="ct-icon ct-iconContainer--circle center-block ct-u-marginBottom30">
							<i class="fa fa-code-fork"></i>
						</div>
						<div class="ct-iconBox--description">
							<a href="#feature-modal" data-target="#feature-modal" data-toggle="modal"><span class="ct-title ct-u-marginBottom20 ct-fw-600">Distribution</span></a>
							<!-- <span class="ct-text ct-u-marginBottom20">Estato is designed a way that it was flexible for different types of business.</span> -->
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-3 ct-u-marginBottom60">
				<a href="#enquiry-modal" data-target="#enquiry-modal" data-toggle="modal">
					<div class="ct-iconBox">
						<div class="ct-icon ct-iconContainer--circle center-block ct-u-marginBottom30">
							<i class="fa fa-user-plus"></i>
						</div>
						<div class="ct-iconBox--description">
							<span class="ct-title ct-u-marginBottom20 ct-fw-600">Franchise</span>
							<!-- <span class="ct-text ct-u-marginBottom20">Estato is designed a way that it was flexible for different types of business.</span> -->
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-3 ct-u-marginBottom60">
				<a href="#enquiry-modal" data-target="#enquiry-modal" data-toggle="modal">
					<div class="ct-iconBox">
						<div class="ct-icon ct-iconContainer--circle center-block ct-u-marginBottom30">
							<i class="fa fa-money"></i>
						</div>
						<div class="ct-iconBox--description">
							<span class="ct-title ct-u-marginBottom20 ct-fw-600">Sell Business</span>
							<!-- <span class="ct-text ct-u-marginBottom20">Estato is designed a way that it was flexible for different types of business.</span> -->
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-3 ct-u-marginBottom60">
				<a href="#enquiry-modal" data-target="#enquiry-modal" data-toggle="modal">
					<div class="ct-iconBox">
						<div class="ct-icon ct-iconContainer--circle center-block ct-u-marginBottom30">
							<i class="fa fa-cart-plus"></i>
						</div>
						<div class="ct-iconBox--description">
							<span class="ct-title ct-u-marginBottom20 ct-fw-600">Buy Business</span>
							<!-- <span class="ct-text ct-u-marginBottom20">Estato is designed a way that it was flexible for different types of business.</span> -->
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-3 ct-u-marginBottom60">
				<a href="#enquiry-modal" data-target="#enquiry-modal" data-toggle="modal">
					<div class="ct-iconBox">
						<div class="ct-icon ct-iconContainer--circle center-block ct-u-marginBottom30">
							<i class="fa fa-users"></i>
						</div>
						<div class="ct-iconBox--description">
							<span class="ct-title ct-u-marginBottom20 ct-fw-600">Merger & Acquisition</span>
							<!-- <span class="ct-text ct-u-marginBottom20">Estato is designed a way that it was flexible for different types of business.</span> -->
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-3 ct-u-marginBottom60">
				<a href="#enquiry-modal" data-target="#enquiry-modal" data-toggle="modal">
					<div class="ct-iconBox">
						<div class="ct-icon ct-iconContainer--circle center-block ct-u-marginBottom30">
							<i class="fa fa-bar-chart"></i>
						</div>
						<div class="ct-iconBox--description">
							<span class="ct-title ct-u-marginBottom20 ct-fw-600">Sell Products</span>
							<!-- <span class="ct-text ct-u-marginBottom20">Estato is designed a way that it was flexible for different types of business.</span> -->
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-3 ct-u-marginBottom60">
				<a href="{{ url('/company/add') }}">
					<div class="ct-iconBox">
						<div class="ct-icon ct-iconContainer--circle center-block ct-u-marginBottom30">
							<i class="fa fa-sign-in"></i>
						</div>
						<div class="ct-iconBox--description">
							<span class="ct-title ct-u-marginBottom20 ct-fw-600">Company Profile</span>
							<!-- <span class="ct-text ct-u-marginBottom20">Estato is designed a way that it was flexible for different types of business.</span> -->
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
@stop

@section('modals')
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="enquiry-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<h4 class="modal-title">
					Please Submit You enquiry
				</h4>
			</div>
			<div class="modal-body">
				<form method="post" action="{{ url('/enquiry') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col-md-6 form-group">
							<label for="name">Your Name</label>
							<input type="text" class="form-control custom-control" name="name" value="{{ old('name') }}" placeholder="Your Name" required>
						</div>
						<div class="col-md-6 form-group">
							<label for="email">Your Email</label>
							<input type="text" class="form-control custom-control" name="email" value="{{ old('email') }}" placeholder="Your Email" required>
						</div>
						<div class="col-md-6 form-group">
							<label for="phone">Contact Number</label>
							<input type="text" class="form-control custom-control" name="phone" value="{{ old('phone') }}" placeholder="Contact Number">
						</div>
						<div class="col-md-6 form-group">
							<label for="enquiry">Enquiry</label>
							<select name="enquiry" class="form-control custom-control">
								<option value="Business Updates">Business Updates</option>
								<option value="Distribution">Distribution</option>
								<option value="Franchise">Franchise</option>
								<option value="Sell Business">Sell Business</option>
								<option value="Buy Business">Buy Business</option>
								<option value="Merger & Acquisition">Merger & Acquisition</option>
								<option value="Sell Products">Sell Products</option>
								<option value="Company Profile">Company Profile</option>
							</select>
						</div>
						<div class="col-md-12 form-group">
							<label for="requirement">Your Query</label>
							<textarea name="requirement" id="requirement" class="form-control custom-control" placeholder="Write what you want us to do for you"></textarea>
						</div>
						<div class="col-md-6 form-group">
							<input type="submit" class="btn btn-sm btn-info" value="Save">
						</div>
						<div class="col-md-6 form-group">
							<button class="btn btn-default btn-sm btn-block" data-dismiss="modal" aria-hidden="true">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop
