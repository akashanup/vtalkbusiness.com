@extends('master')

@section('title')
<title>Contact Us</title>
@stop

@section('breadcrumb')
<!-- BreadCrumbs -->
<div class="ct-site--map">
    <div class="container">
        <a href="index.html">Home</a>
        <a href="features-buttons.html">Contact</a>
    </div>
</div>
<!-- BreadCrumb Ends -->
@stop

@section('page-header')

@stop

@section('content')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="630" data-type="parallax" data-bg-image="assets/images/content/registration-parallax.jpg" data-bg-image-mobile="assets/images/content/registration-parallax.jpg">
    <div class="ct-mediaSection-inner">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="ct-headerText--normal">
                        <h2 class="text-uppercase ct-fw-600 ct-u-marginBottom70">
                            What will you
                            <span class="ct-u-text--motive">earn</span>
                            by registering?
                        </h2>
                    </div>
                    <div class="ct-iconBox ct-u-marginBottom40 ct-iconBox--2col">
                        <div class="ct-icon text-center ct-iconContainer--circle ct-iconContainer--circleHoverLight">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title text-uppercase ct-fw-600">Get more business</span>
                            <span class="ct-text">Thanks to our search you will get more investors drawn towards you.</span>
                        </div>
                    </div>
                    <div class="ct-iconBox ct-u-marginBottom40 ct-iconBox--2col">
                        <div class="ct-icon text-center ct-iconContainer--circle ct-iconContainer--circleHoverLight">
                            <i class="fa fa-bolt"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title text-uppercase ct-fw-600">Free business accont</span>
                            <span class="ct-text">By registering now you will get free business account for a year.</span>
                        </div>
                    </div>
                    <div class="ct-iconBox ct-iconBox--2col">
                        <div class="ct-icon text-center ct-iconContainer--circle ct-iconContainer--circleHoverLight">
                            <i class="fa fa-trophy"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title text-uppercase ct-fw-600">Reach your goal</span>
                            <span class="ct-text">VTalkBusiness was designed to become your business search engine. It’s not only a tool, it’s a working ecosystem.</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <form role="form" class="ct-formRegister pull-right" method="post" action="{{ url('/contact') }}" id="contact-form">
                        <div class="form-group">
                            <div class="ct-form--label--type2">
                                <div class="ct-u-displayTableVertical">
                                    <div class="ct-u-displayTableCell">
                                        <div class="ct-input-group-btn">
                                            <button class="btn btn-primary">
                                                <i class="fa fa-user"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="ct-u-displayTableCell text-center">
                                        <span class="text-uppercase">Contact Us now</span>
                                    </div>
                                </div>
                            </div>
                            <div id="contact-error">
                            @if($errors->any())
                                @foreach($errors->all() as $error)
                                <div class="errorMessage alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ $error }}
                                </div>
                                @endforeach
                            @endif
                            </div>
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="ct-form--item ct-u-marginBottom20 form-group">
                                <label>Your Name</label>
                                <input type="text"  class="form-control input-lg" placeholder="Name" name="name" value="{{ old('name') }}">
                            </div>
                            <div class="ct-form--item ct-u-marginBottom20">
                                <label>Your Email</label>
                                <input type="text" class="form-control input-lg" placeholder="Your Email" name="email" value="{{ old('email') }}">
                            </div>
                            <div class="ct-form--item ct-u-marginBottom20">
                                <label>Phone/Mobile</label>
                                <input type="text" class="form-control input-lg" name="phone" placeholder="Phone/Mobile">
                            </div>
                            <div class="ct-form--item ct-u-marginBottom20">
                                <label>Message</label>
                                <textarea class="form-control input-lg" placeholder="Your Message" name="message"></textarea>
                            </div>
                            <!-- <div class="ct-form--item ct-u-marginBottom20">
                                <label>Account Type</label>
                                <select class="ct-js-select ct-select-lg">
                                    <option value="month">Standard User</option>
                                    <option value="January">Premium User</option>
                                </select>
                            </div> -->
                            <div class="ct-form--item">
                                <button type="submit" class="btn btn-warning center-block">Contact Now</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="ct-u-paddingBoth60">
    <div class="container">
        <div class="col-md-6 col-lg-3">
            <div class="ct-heading ct-u-marginBottom50">
                <h4 class="text-uppercase">address details</h4>
            </div>
            <p>
                A-92B, III Floor,<br>
                Lajpat Nagar II<br>
				New Delhi – 110024,<br>
				India
            </p>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="ct-heading ct-u-marginBottom50">
                <h4 class="text-uppercase">direct contact</h4>
            </div>
            <ul class="list-unstyled ct-phoneNumbers ct-u-marginBottom30">
                <li>
                    <i class="fa fa-phone"></i>
                    <span class="ct-fw-600">011 - 4132 7798</span>
                </li>
                <li>
                    <i class="fa fa-fax"></i>
                    <span class="ct-fw-600">011 - 4132 5197</span>
                </li>
                <li>
                    <i class="fa fa-mobile"></i>
                    <span class="ct-fw-600">+91-8130894552</span>
                </li>
            </ul>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="ct-heading ct-u-marginBottom50">
                <h4 class="text-uppercase">emails</h4>
            </div>
            <div class="ct-contactList ">
                <a class="ct-u-marginBottom10" href="mailto:info@vtalkbusiness.com"><i class="fa fa-envelope-o"></i>info@vtalkbusiness.com</a>
                <a class="ct-u-marginBottom10" href="mailto:support@vtalkbusiness.com"><i class="fa fa-envelope-o"></i>support@vtalkbusiness.com</a>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="ct-heading ct-u-marginBottom50">
                <h4 class="text-uppercase">social networks</h4>
            </div>
            <ul class="ct-panel--socials ct-panel--navbar list-unstyled ct-u-marginBottom30">
                <li class="ct-u-marginBottom10"><a href="https://www.facebook.com/createITpl"><div class="ct-socials ct-socials--circle"><i class="fa fa-facebook"></i></div>Facebook Profile</a></li>
                <li class="ct-u-marginBottom10"><a href="https://twitter.com/createitpl"><div class="ct-socials ct-socials--circle"><i class="fa fa-twitter"></i></div>Twitter Profile</a></li>
                <li><a href="#"><div class="ct-socials ct-socials--circle"><i class="fa fa-linkedin"></i></div>LinkedIn Profile</a></li>
            </ul>
        </div>
    </div>
</section>
<!-- <section class="ct-u-paddingBoth70 ct-js-section text-left">
    <div class="container">
		<div class="ct-heading text-center ct-u-marginBottom60">
		    <h3 class="text-uppercase">Please leave your details. We will contact you soon.</h3>
		</div>
		@if($errors->any())
			<ul class="well text-danger">
			@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
			</ul>
		@endif
		<form method="post" action="{{ url('/contact') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group col-md-12">
		        <label for="helptext">Name</label>
		        <input type="text" class="form-control ct-input--border ct-u-text--dark" placeholder="Name" name="name">
		    </div>
		    <div class="form-group col-md-6">
		        <label for="helptext">Your Email</label>
		        <input type="text" class="form-control ct-input--border ct-u-text--dark" placeholder="Your Email" name="email">
		    </div>
		    <div class="form-group col-md-6">
		        <label for="helptext">Phone/Mobile</label>
		        <input type="text" class="form-control ct-input--border ct-u-text--dark" placeholder="Phone / Mobile" name="phone">
		    </div>
		    <div class="form-group col-md-12">
		        <label for="helptext">Message</label>
		        <textarea class="form-control ct-input--border ct-u-text--dark" placeholder="Your Message" name="message"></textarea>
		    </div>
		    <div class="form-group col-md-12 button">
		        <button class="btn btn-info btn-block" type="submit">Save</button>
		    </div>
		</form>
    </div>
</section> -->
@stop

@section('scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="{{ asset('/js/gmap3.min.js') }}"></script>
<script type="text/javascript">
$(function() {
	$('#contact-form').on('submit', function(e) {
		e.preventDefault();
		var data = $('#contact-form').serialize();
		$.ajax({
			type: 'post',
			url : '{{ url("/contact") }}',
			data: data
		}).done(function(response) {
			if(response.status) {
				bootbox.alert('Message send Successfully. Our Customer Executive will contact you soon.');
				window.location.href = '{{ url('/') }}';
			}
		}).fail(function(xhr) {
			if(xhr.status == 422) {
				errors = JSON.parse(xhr.responseText);
				myErrors = [];
				$.each(errors, function(i, v) {
					$.each(v, function(j, y) {
						myErrors.push(y);
					});
				});
				bootbox.alert('You have some errors in contact form please review and try again.');
				var html = '';
				$.each(myErrors, function(i, v) {
					html += '<div class="errorMessage alert alert-danger">\
                    	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + v +
                	'</div>';
				});
				$('#contact-error').empty().append(html);
			}
		});
	});
});
</script>
@stop