@extends('master')

@section('title')
<title>Reset Password</title>
@stop

@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
    <div class="ct-mediaSection-inner">
        <div class="container">
			<div class="ct-heading--main text-center">
			    <h3 class="text-uppercase ct-u-text--white">Reset Password</h3>
			</div>
        </div>
    </div>
</header>
@stop

@section('content')
<section class="ct-u-paddingBoth70 ct-js-section text-left">
    <div class="container">
		@if($errors->any())
            @foreach($errors->all() as $error)
            <div class="errorMessage alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ $error }}
            </div>
            @endforeach
        @endif
		<div class="col-md-6 col-md-offset-3">
			<form method="post" id="frm" >
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
	    		<div class="form-group col-md-12">
	        		<label for="helptext">Enter your New Password</label>
	        		<input type="password" class="form-control ct-input--border ct-u-text--dark" placeholder="Enter your New Password" name="password"  id="password">
	    		</div>
	    		<div class="form-group col-md-12">
	        		<label for="helptext">Confirm your New Password</label>
	        		<input type="password" class="form-control ct-input--border ct-u-text--dark" placeholder="Confirm your New Password" name="cnfPassword"  id="cnfPassword">
	    		</div>
	    		<div class="form-group col-md-12">
	    			<label for="helptext">&nbsp;</label>
	        		<input class="btn btn-success btn-block" type="button" value="Save Password" id="savePassword">
	    		</div>
			</form>
		</div>
    </div>
</section>
@stop

@section('scripts')
<script type="text/javascript">
	$(function(){
		$('#password').blur(function(){
			if($('#password').val()=="")
			{
				setError($("#password"), "This field can't be left blank.");
			}
			else
			{
				unsetError($("#password"));	
			}
		});
		$('#cnfPassword').blur(function(){
			if($('#cnfPassword').val()=="")
			{
				setError($("#cnfPassword"), "This field can't be left blank.");
			}
			else
			{
				unsetError($("#cnfPassword"));	
			}
			if($('#cnfPassword').val()!=$('#password').val())
			{
				setError($("#cnfPassword"), "Confirm Password must be same as Password.");
			}
			else
			{
				unsetError($("#cnfPassword"));	
			}
		});
	
		$('#savePassword').click(function(){
			if($('#password').val()=="")
			{
				setError($("#password"), "This field can't be left blank.");
			}
			else
			{
				unsetError($("#password"));	
			}
			if($('#cnfPassword').val()=="")
			{
				setError($("#cnfPassword"), "This field can't be left blank.");
			}
			else
			{
				unsetError($("#cnfPassword"));	
			}
			if(($('#password').val()=="")||($('#cnfPassword').val()!=$('#password').val()))
			{
				setError($("#cnfPassword"), "Confirm Password must be same as Password.");
			}
			else
			{
				unsetError($('#cnfPassword'));
				var formData = JSON.parse(JSON.stringify(jQuery('#frm').serializeArray())) ;
				$.ajax({
					type 	: 	'POST',
					url 	: 	"{{action('DefaultController@userSaveResetPassword',[$id])}}",
					data 	: 	formData,
	            	success :   function(data){
	                				alert("Your Password have been changed!");
	                				window.location = '{{ url("/login") }}';
	            	},
	            	error 	: 	function(error){
	            		console.log(error);
	           		}
				});
			}
		});
	});
	
	function setError(element, message) {
		var parent = element.parents('.form-group:eq(0)');
		if(parent.hasClass('has-error'))
			return;
		parent.addClass('has-error').append('<span class="help-block">' + message + '</span>')
	}
	function unsetError(element) {
		var parent = element.parents('.form-group:eq(0)');
		parent.removeClass('has-error');	
		parent.find('.help-block').remove();
	}

</script>
@stop