@extends('master')

@section('title')
<title>Dashboard</title>
@stop


@section('breadcrumb')
<!-- BreadCrumbs -->
<div class="ct-site--map">
    <div class="container">
        <a href="{{ url('/dashboard') }}">Dashboard</a>
    </div>
</div>
<!-- BreadCrumb Ends -->
@stop

@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
    <div class="ct-mediaSection-inner">
        <div class="container">
			<div class="ct-heading--main text-center">
			    <h3 class="text-uppercase ct-u-text--white">User Dashboard</h3>
			</div>
        </div>
    </div>
</header>
@stop

@section('content')
<section class="ct-u-paddingBoth70 ct-js-section text-left">
    <div class="container">
		@if($errors->any())
        	@foreach($errors->all() as $error)
        	<div class="errorMessage alert alert-danger">
            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            	{{ $error }}
        	</div>
        	@endforeach
        @endif
        <div class="row">
        	<div class="col-md-12">
        		<div class="dashboard tiles">
				    <!-- <div class="col-lg-1 col-md-1"></div> -->
				    @if($graphData['total_companies'] != 0)
					<a href="{{ url('/my-companies') }}"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					  <div class="tile-main-div">
					     <div class="tile bg-blue-steel">
					        <div class="tile-body">
					            <i class="fa fa-cubes"></i>
					        	<div class="tile-title companies">0</div>
					        </div>
					     </div>
					        <div class="tile-object">
					           <div class="name">Your Companies</div>
					        </div>
					  </div>
					</div></a>
					@else
					<a href="{{ url('/company/add') }}"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					  <div class="tile-main-div">
					     <div class="tile bg-blue-steel">
					        <div class="tile-body">
					        	<i class="fa fa-plus"></i>
					            <div class="tile-title" style="font-size: 20px;">Create Comapny Now</div>
					        </div>
					     </div>
					        <div class="tile-object">
					           <div class="name">Create Company Now</div>
					        </div>
					  </div>
					</div></a>
					@endif
					@if($graphData['total_opportunities'] != 0)
					<a href="{{ url('/my-opportunities') }}"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					  <div class="tile-main-div">
					     <div class="tile bg-blue-steel">
					        <div class="tile-body">
					            <i class="fa fa-line-chart"></i>
					        	<div class="tile-title opportunities">0</div>
					        </div>
					     </div>
					        <div class="tile-object">
					           <div class="name">Your Opportunities</div>
					        </div>
					  </div>
					</div></a>
					@else
					<a href="{{ url('/opportunity') }}"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					  <div class="tile-main-div">
					     <div class="tile bg-blue-steel">
					        <div class="tile-body">
					        	<i class="fa fa-plus"></i>
					            <div class="tile-title" style="font-size: 20px;">Post an Opportunity</div>
					        </div>
					     </div>
					        <div class="tile-object">
					           <div class="name">Post an Opportunity</div>
					        </div>
					  </div>
					</div></a>
					@endif
					<a href="{{ url('/investor') }}"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					  <div class="tile-main-div">
					     <div class="tile bg-blue-steel">
					        <div class="tile-body">
					        	<i class="fa fa-plus"></i>
					            <div class="tile-title" style="font-size: 20px;">&nbsp;</div>
					        </div>
					     </div>
					        <div class="tile-object">
					           <div class="name">View Investor Profile</div>
					        </div>
					  </div>
					</div></a>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					  <div class="tile-main-div">
					     <div class="tile bg-blue-steel">
					        <div class="tile-body">
					            <canvas id="cinterest" style="width: 100%;height: 100%;">
					        </div>
					     </div>
					        <div class="tile-object">
					           <div class="name">Company Interests ({{ $graphData['total_cinterest'] }})</div>
					        </div>
					  </div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					  <div class="tile-main-div">
					     <div class="tile bg-blue-steel">
					        <div class="tile-body">
					            <canvas id="ointerest" style="width: 100%;height: 100%;">
					        </div>
					     </div>
					        <div class="tile-object">
					           <div class="name">Opportunity Interests ({{ $graphData['total_ointerest'] }})</div>
					        </div>
					  </div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					  <div class="tile-main-div">
					     <div class="tile bg-blue-steel">
					        <div class="tile-body">
					            <i class="fa fa-star"></i>
					        	<div class="tile-title interests">0</div>
					        </div>
					     </div>
					        <div class="tile-object">
					           <div class="name">Total Interests</div>
					        </div>
					  </div>
					</div>

					<!-- <div class="col-lg-1 col-md-1"></div> -->
					<!-- div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					  <div class="tile-main-div">
					     <div class="tile bg-blue-steel">
					        <div class="tile-body">
					           <i class="fa fa-male"></i>
					        <a href="#"><div class="tile-title">View Details</div></a>
					        </div>
					     </div>
					        <div class="tile-object">
					           <div class="name">
					               Customers
					           </div>
					           <div class="number customers-count">21</div>
					        </div>
					  </div>
					</div> -->

			   </div>
        	</div>
        </div>
    </div>
</section>
@stop

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/Chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/countroze.js') }}"></script>
<script type="text/javascript">
	$(function() {
		$('.tile-title.companies').countroze({{ $graphData['total_companies'] }});
		$('.tile-title.opportunities').countroze({{ $graphData['total_opportunities'] }});
		$('.tile-title.interests').countroze({{ $graphData['total_interests'] }});
		var cinterest = JSON.parse('{!! $graphData['cinterest'] !!}');
        var ointerest = JSON.parse('{!! $graphData['ointerest'] !!}');
		$('#cinterest').appear(function() {
			var ctx = $(this)[0].getContext("2d");
            window.myDoughnut = new Chart(ctx).Doughnut(cinterest, {responsive: true, percentageInnerCutout : 0, showTooltips: true});
		});
		$('#ointerest').appear(function() {
			var ctx = $(this)[0].getContext("2d");
            window.myDoughnut = new Chart(ctx).Doughnut(ointerest, {responsive: true, percentageInnerCutout : 0, showTooltips: true});
		});
	});
</script>
@stop
