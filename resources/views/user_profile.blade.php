@extends('master')

@section('title')
    <title>Edit User Profile</title>
@stop

@section('breadcrumb')
<div class="ct-site--map">
    <div class="container">
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <a href="{{ url('/user_profile') }}">My Profile</a>
    </div>
</div>
@stop



@section('content')
<header class="ct-mediaSection ct-u-paddingBoth100" data-stellar-background-ratio="0.3" data-height="470" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg">
    <div class="container">
        <div class="ct-personBox ct-personBox--extended ct-personBox--extendedLight text-left">
            <div class="ct-imagePerson">
                <img src="@if($user_profile){{ $user_profile->profile_picture }}@else{{ asset('/images/default_user_image.jpg') }}@endif" alt="">
                <a class="btn btn-xs btn-edit text-uppercase ct-js-btnEdit--Agents" data-toggle="modal" data-target="#image-upload-modal">edit</a>
                <ul class="ct-panel--socials list-inline list-unstyled">
                    <li><a href="@if($user_profile){{$user_profile->facebook}}@endif" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-facebook"></i></div></a></li>
                    <li><a href="https://twitter.com/@if($user_profile){{$user_profile->twitter}}@endif" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-twitter"></i></div></a></li>
                    <li><a href="@if($user_profile){{$user_profile->instagram}}@endif" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-instagram"></i></div></a></li>
                    <li><a href="@if($user_profile){{$user_profile->linkedin}}@endif" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-linkedin"></i></div></a></li>
                </ul>
            </div>
            <div class="ct-personContent">
                <div class="ct-personName ct-u-paddingBoth10 text-uppercase">
                    <h2>{{ $user->name }}</h2>
                    <a href="">@if($user_profile) {{$user_profile->designation}} @endif<i class="fa fa-external-link"></i></a>
                </div>
                <div class="ct-personDescription pull-right ct-u-paddingBoth20">
                    <ul class="list-unstyled ct-contactPerson pull-right">
                        <li>
                            <i class="fa fa-phone"></i>
                            <span>@if($user_profile) {{$user_profile->phone}} @endif</span>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-skype"></i>@if($user_profile) {{$user_profile->skype}} @endif</a>
                        </li>
                        <li>
                            <i class="fa fa-mobile"></i>
                            <span>@if($user_profile) {{$user_profile->mobile}} @endif</span>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-envelope-o"></i>{{ $user->email }}</a>
                        </li>
                    </ul>
                </div>
                <!-- <div class="ct-buttonPanel text-capitalize ct-u-paddingBoth20 pull-right">
                    <a href="mailto:{{ $user->email }}" class="btn btn-default btn-transparent--border ct-u-text--white btn-hoverWhite">Contact User</a>
                    <a href="#" class="btn btn-default btn-transparent--border ct-u-text--white btn-hoverWhite">View Companies</a>
                </div> -->
            </div>
        </div>
    </div>
</header>
    <section class="ct-u-paddingBoth70 ct-js-section text-left">
        <div class="container">
            <div class="ct-heading text-center ct-u-marginBottom60">
                <h3 class="text-uppercase">Edit User Profile</h3>
            </div>
            @if($errors->any())
                @foreach($errors->all() as $error)
                <div class="errorMessage alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $error }}
                </div>
                @endforeach
            @endif
            <form method="post" name="investorRegistration" id="frm" action="{{ url('/user_profile') }}">            
                <div class="form-group col-md-6">
                    <label for="helptext">Designation</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="designation" placeholder="Designation" name="designation" value="@if($user_profile){{$user_profile->designation}}@endif">
                </div>
                <div class="form-group col-md-6">
                    <label for="helptext">Phone</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="phone" placeholder="phone" name="phone" value="@if($user_profile){{ $user_profile->phone }}@endif">
                </div>  
                <div class="form-group col-md-6">
                    <label for="helptext">Mobile</label>
                    <input type="number" class="form-control ct-input--border ct-u-text--dark" id="mobile" placeholder="mobile"  name="mobile" value="@if($user_profile){{ $user_profile->mobile }}@endif">
                    <span id="mobileError" style="display:none; color:red"></span>
                </div>              
                <div class="form-group col-md-6">
                    <label for="helptext">Skype</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="skype" placeholder="skype" name="skype" value="@if($user_profile){{$user_profile->skype}}@endif">
                </div>
                <div class="form-group col-md-6">
                    <label for="helptext">Facebook URL</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="facebook" placeholder="Facebook URL" name="facebook" value="@if($user_profile){{$user_profile->facebook}}@endif">
                </div>
                <div class="form-group col-md-6">
                    <label for="helptext">Twitter Name</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="twitter" placeholder="Twitter Name" name="twitter" value="@if($user_profile){{$user_profile->twitter}}@endif">
                </div>
                <div class="form-group col-md-6">
                    <label for="helptext">Instagram UserID</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="instagram" placeholder="Instagram UserID" name="instagram" value="@if($user_profile){{$user_profile->instagram}}@endif">
                </div>
                <div class="form-group col-md-6">
                    <label for="helptext">Linkedin URL</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="linkedin" placeholder="Linkedin URL" name="linkedin" value="@if($user_profile){{$user_profile->linkedin}}@endif">
                </div>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">             
                <div class="form-group col-md-12 button">
                    <input type="submit" id="submit"class="btn btn-info btn-block"value="Save" >
                </div>
            </form>
        </div>
    </section>
@stop

@section('modals')
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="image-upload-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                    Upload Image
                </h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('/user/picture/upload') }}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <span class="btn btn-default btn-block">
                                <input type="file" id="image" name="image">
                            </span>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="submit" class="btn btn-sm btn-info" value="Save">
                        </div>
                        <div class="col-md-6 form-group">
                            <button class="btn btn-default btn-sm btn-block" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
   <script type="text/javascript">
     $(function(){
        $("#mobile").on("blur",function(){
            if($("#mobile").val().length == 10)
            {
                $("#mobileError").css("display","none");
            }
            else
            {
                $("#mobileError").css("display","block");
                $("#mobileError").html("Mobile Number must be of 10 digits");
            }
        });

        $("#submit").on("click",function(e){
            if($("#mobile").val().length == 10)
            {
                //$("form#frm").submit();
            }
            else
            {
                $("#mobileError").css("display","block");
                $("#mobileError").html("Mobile Number must be of 10 digits");
                e.preventDefault();
            }
        });
    });
   </script>
@stop