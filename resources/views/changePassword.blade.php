@extends('master')

@section('title')
<title>Change Password</title>
@stop

@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
    <div class="ct-mediaSection-inner">
        <div class="container">
			<div class="ct-heading--main text-center">
			    <h3 class="text-uppercase ct-u-text--white">Change Password</h3>
			</div>
        </div>
    </div>
</header>
@stop

@section('content')
<section class="ct-u-paddingBoth70 ct-js-section text-left">
    <div class="container">
		@if($errors->any())
        	@foreach($errors->all() as $error)
        	<div class="errorMessage alert alert-danger">
            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            	{{ $error }}
        	</div>
        	@endforeach
        @endif
		<div class="col-md-6 col-md-offset-3">
			<form method="post" id="frm" action="{{action('UserController@userSaveChangePassword')}}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
	    		<div class="form-group col-md-12">
	        		<label for="helptext">Enter your old password</label>
	        		<input type="password" class="form-control ct-input--border ct-u-text--dark" placeholder="Your Old Password" name="oldPassword"  id="oldPassword">
	    		</div>		    
	    		<div class="form-group col-md-12">
	        		<label for="helptext">Enter a new password</label>
	        		<input type="password" class="form-control ct-input--border ct-u-text--dark" placeholder="****New Password****" name="newPassword" id="newPassword">		    
				</div>
				<div class="form-group col-md-12">
	        		<label for="helptext">Re-enter the new password</label>
	        		<input type="password" class="form-control ct-input--border ct-u-text--dark" placeholder="****Confirm Password****" name="confirmNewPassword" id="confirmNewPassword">		    
				</div>
	    		<div class="form-group col-md-12">
	    			<label for="helptext">&nbsp;</label>
	        		<input class="btn btn-info btn-block" type="submit" value="Change Password" id="changePassword">
	    		</div>
			</form>
		</div>
    </div>
</section>
@stop

@section('scripts')
<script type="text/javascript">
	$(function(){
		$('#oldPassword').blur(function(){
			if($('#oldPassword').val()=="")
			{
				setError($("#oldPassword"), "This field can't be left blank.");
			}
			else
			{
				unsetError($("#oldPassword"));	
			}
		});
	
		$('#newPassword').blur(function(){
			if($('#newPassword').val()=="")
			{
				setError($("#newPassword"), "This field can't be left blank.");
			}
			else
			{
				unsetError($("#newPassword"));	
			}
		});
		
		$('#confirmNewPassword').blur(function(){
			if($('#confirmNewPassword').val()=="")
			{
				setError($("#confirmNewPassword"), "This field can't be left blank.");
			}
			else
			{
				unsetError($("#confirmNewPassword"));	
			}
		});

		$('#changePassword').click(function(e){
			if($('#oldPassword').val()=="")
			{
				e.preventDefault();
				setError($("#oldPassword"), "This field can't be left blank.");
			}
			else
			{
				unsetError($("#oldPassword"));	
			}
			if($('#newPassword').val()=="")
			{
				e.preventDefault();
				setError($("#newpassword"), "This field can't be left blank.");
			}
			else
			{
				unsetError($("#newpassword"));	
			}
			if($('#confirmNewPassword').val()=="")
			{
				e.preventDefault();
				setError($("#confirmNewpassword"), "This field can't be left blank.");
			}
			else
			{
				unsetError($("#confirmNewpassword"));	
			}
			if($('#newPassword').val()!=$('#confirmNewPassword').val())
			{
				e.preventDefault();	
				setError($("#confirmNewPassword"), "Password and Confirm Password must be same.");
			}
			else
			{
				unsetError($("#confirmNewPassword"));	
			}
		});
	});
	function setError(element, message) {
		var parent = element.parents('.form-group:eq(0)');
		if(parent.hasClass('has-error'))
			return;
		parent.addClass('has-error').append('<span class="help-block">' + message + '</span>')
	}
	function unsetError(element) {
		var parent = element.parents('.form-group:eq(0)');
		parent.removeClass('has-error');	
		parent.find('.help-block').remove();
	}

</script>
@stop