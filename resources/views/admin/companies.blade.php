@extends('master')

@section('title')
	<title> Companies </title>
@stop

@section('styles')
	<style type="text/css">
		@media only screen and (max-width: 800px) {
			
			#flip-scroll .cf:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
			#flip-scroll * html .cf { zoom: 1; }
			#flip-scroll *:first-child+html .cf { zoom: 1; }
			
			#flip-scroll table { width: 100%; border-collapse: collapse; border-spacing: 0; }
		 
			#flip-scroll th,
			#flip-scroll td { margin: 0; vertical-align: top; }
			#flip-scroll th { text-align: left; }
			
			#flip-scroll table { display: block; position: relative; width: 100%; }
			#flip-scroll thead { display: block; float: left; }
			#flip-scroll tbody { display: block; width: auto; position: relative; overflow-x: auto; white-space: nowrap; }
			#flip-scroll thead tr { display: block; }
			#flip-scroll th { display: block; text-align: right; }
			#flip-scroll tbody tr { display: inline-block; vertical-align: top; }
			#flip-scroll td { display: block; min-height: 1.25em; text-align: left; }
		 
		 
			/* sort out borders */
		 
			#flip-scroll th { border-bottom: 0; border-left: 0; }
			#flip-scroll td { border-left: 0; border-right: 0; border-bottom: 0; }
			#flip-scroll tbody tr { border-left: 1px solid #babcbf; }
			#flip-scroll th:last-child,
			#flip-scroll td:last-child { border-bottom: 1px solid #babcbf; }
		}
		table tr {
			font-size: 12px;
		}
	</style>
@stop

@section('breadcrumb')
	<!-- BreadCrumbs -->
	<div class="ct-site--map">
	    <div class="container">
	        <a href="{{ url('/dashboard') }}"> Dashboard </a>
	        <a href="{{ url('/admin/companies') }}">Companies</a>
	    </div>
	</div>
	<!-- BreadCrumb Ends -->
@stop

@section('page-header')
	<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
	    <div class="ct-mediaSection-inner">
	        <div class="container">
				<div class="ct-heading--main text-center">
				    <h3 class="text-uppercase ct-u-text--white"> Manage Companies </h3>
				</div>
	        </div>
	    </div>
	</header>
@stop

@section('content')
	<section class="ct-u-paddingBoth70 ct-js-section text-left">
	    <div class="container">
			<!-- <div class="ct-heading text-center ct-u-marginBottom60">
			    <h3 class="text-uppercase"> Companies </h3>
			</div> -->

			
			@if($errors->any())
				<ul class="well text-danger">
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
			<div id="flip-scroll" class="table-responsive">
				<table class="table table-striped text-center table-hover">
					<thead>
						<tr>
							<!-- <th> S/N </th> -->
							<th>User Name</th>
							<th>User E-mail</th>
							<th>Company Name</th>
							<th>Company E-mail</th>
							<th>Company Phone no.</th>
							<th>Compony Evaluation</th>
							<th>CIN</th>
							<th>Featured</th>
							<th>Action</th>
						</tr>

					</thead>
					<tbody>
						@foreach($companies as $x=>$company)
							<tr data-id="{{ $company->id }}">
								<!-- <td> {{ $x + 1 }} </td> -->
								<td> {{ $company->users_name }} </td>
								<td> {{ $company->users_email }} </td>
								<td> {{ $company->name }} </td>
								<td> {{ $company->email }} </td>
								<td> {{ $company->phone }} </td>
								<td> {{ $company->company_cin }} </td>
								<td> {{ $company->evaluation }} </td>
								<td>
									<a class="add-featured"> 
										@if($company->featured == 1)
											<i title="Not Featured" class="fa fa-times"></i> 
										@else
											<i title="Featured" class="fa fa-check-square-o"></i>
										@endif
									</a>
								</td>
								<td>
									<!-- <a href=""> <i class="fa fa-eye"></i> </a> |  -->
									<a class="ban-this-company">
									@if($company->ban == 1)
										<i title="Banned" class="fa fa-times"></i> 
									@else
										<i title="Not Banned" class="fa fa-check-square-o"></i>
									@endif
									</a> | <a class="delete-company"> <i class="fa fa-trash"></i> </a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right pagination-container">{!! $companies->render() !!}</div>
	    </div>
	</section>
@stop


@section('scripts')
<script type="text/javascript">
	$(".delete-company").off('click');
	$(".delete-company").on('click', function(e) {
		e.preventDefault();
		name = $(this).parents('tr:eq(0)').find('td:eq(1)').text()
		tr = $(this).parents('tr:eq(0)');
		id = $(this).parents('tr:eq(0)').data('id');
		bootbox.confirm("Are you sure want to delete: "+name, function(e) {		
			if (e) {
				var req = {'_token': "{{ csrf_token() }}"};
				req.company_id = id;
				$.ajax({
					type	:	'post',
					url 	:	'{{ url('/admin/delete-companies') }}',
					contentType	: "appalication/json", 
					data	:	JSON.stringify(req)
				}).done(function(res) {
					// console.log(res);
					if(res.status == 1) {
						$(tr).hide('slow');
						// bootbox.alert(res.message, function(){});
						// toastr.success(res.message);
					}
					else {
						// bootbox.alert(res.message, function(){});
					}
				});
			};
		});
	});

	$('.ban-this-company').off('click');
	$('.ban-this-company').on('click', function(e) {
		e.preventDefault();
		a = $(this);
		id = $(this).parents('tr:eq(0)').data('id');
		var req = {'_token': "{{ csrf_token() }}"};
		req.action = 'changeCompanyBanStatus';
		req.status = 1;
		if ($(this).find('i').hasClass('fa-times')) {
			req.status = 0;
		};
		req.company_id = id;
		$.ajax({
			type	:	'post',
			url		:	'{{ url('/admin/company-ban-status') }}',
			data	:	JSON.stringify(req),
			contentType	: "appalication/json", 
		}).done(function(res) {
			// console.log(res);
			// res = $.parseJSON(res);
			if(res.status == 1) {
				if (req.status == 1) {
					$(a).find('i').removeClass('fa-check-square-o').addClass('fa-times').attr('title', 'Banned');;
				}else{
					$(a).find('i').removeClass('fa-times').addClass('fa-check-square-o').attr('title', 'Not Banned');;
				};
				// bootbox.alert(res.message, function(){});
				// toastr.info(res.message);
				// Product.fetchAllProducts();
			}
			else {
				bootbox.alert(res.message, function(){});
				// toastr.error(res.message);
			}
		});	
	});

	$('.add-featured').off('click');
	$('.add-featured').on('click', function(e) {
		e.preventDefault();
		a = $(this);
		id = $(this).parents('tr:eq(0)').data('id');
		var req = {'_token': "{{ csrf_token() }}"};
		req.status = 1;
		if ($(this).find('i').hasClass('fa-times')) {
			req.status = 0;
		};
		req.company_id = id;
		$.ajax({
			type	:	'post',
			url		:	'{{ url('/admin/add-featured') }}',
			data	:	JSON.stringify(req),
			contentType	: "appalication/json", 
		}).done(function(res) {
			// console.log(res);
			// res = $.parseJSON(res);
			if(res.status == 1) {
				if (req.status == 1) {
					$(a).find('i').removeClass('fa-check-square-o').addClass('fa-times').attr('title', 'Not Featured');
				}else {
					$(a).find('i').removeClass('fa-times').addClass('fa-check-square-o').attr('title', 'Featured');
				};
				// bootbox.alert(res.message, function(){});
				// toastr.info(res.message);
				// Product.fetchAllProducts();
			}
			else {
				bootbox.alert(res.message, function(){});
				// toastr.error(res.message);
			}
		});	
	});

</script>

@stop







