@extends('master')

@section('title')
	<title> Contacts </title>
@stop

@section('breadcrumb')
	<!-- BreadCrumbs -->
	<div class="ct-site--map">
	    <div class="container">
	        <a href="{{ url('/dashboard') }}"> Dashboard </a>
	        <a href="{{ url('/admin/contacts') }}">Contacts</a>
	    </div>
	</div>
	<!-- BreadCrumb Ends -->
@stop

@section('page-header')
	<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
	    <div class="ct-mediaSection-inner">
	        <div class="container">
				<div class="ct-heading--main text-center">
				    <h3 class="text-uppercase ct-u-text--white"> Manage Contact </h3>
				</div>
	        </div>
	    </div>
	</header>
@stop

@section('content')
	<section class="ct-u-paddingBoth70 ct-js-section text-left">
	    <div class="container">
			<!-- <div class="ct-heading text-center ct-u-marginBottom60">
			    <h3 class="text-uppercase"> Companies </h3>
			</div> -->

			
			@if($errors->any())
				<ul class="well text-danger">
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif

			<table class="table table-striped">
				<thead>
					<tr>
						<th> S/N </th>
						<th> Name </th>
						<th> E-mail </th>
						<th> Phone No. </th>
						<th> Message </th>
					</tr>

				</thead>
				<tbody>
					@foreach($contacts as $x=>$contact)
						<tr>
							<td> {{ $x + 1 }} </td>
							<td> {{ $contact->name }} </td>
							<td> {{ $contact->email }} </td>
							<td> {{ $contact->phone }} </td>
							<td> {{ $contact->message }} </td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class="pull-right pagination-container">{!! $contacts->render() !!}</div>
	    </div>
	</section>
@stop


@section('scripts')
	
@stop

















