@extends('master')

@section('title')
	<title> Youtube Videos </title>
@stop

@section('breadcrumb')
	<!-- BreadCrumbs -->
	<div class="ct-site--map">
	    <div class="container">
	        <a href="{{ url('/dashboard') }}"> Dashboard </a>
	        <a href="{{ url('/admin/videos') }}"> Video Listing and Management</a>
	    </div>
	</div>
	<!-- BreadCrumb Ends -->
@stop

@section('page-header')
	<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
	    <div class="ct-mediaSection-inner">
	        <div class="container">
				<div class="ct-heading--main text-center">
				    <h3 class="text-uppercase ct-u-text--white">  Video Listing and Management </h3>
				</div>
	        </div>
	    </div>
	</header>
@stop

@section('content')
	<section class="ct-u-paddingBoth70 ct-js-section text-left">
	    <div class="container">
			<!-- <div class="ct-heading text-center ct-u-marginBottom60">
			    <h3 class="text-uppercase"> Companies </h3>
			</div> -->

			
			@if($errors->any())
				<ul class="well text-danger">
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
			<div class="row">
				<div class="col-md-2 col-md-offset-10">
					<p><a class="btn btn-primary" id="addVideo" role="button"><i class="fa fa-plus-circle"></i> Add Video</a></p>
				</div>
				<div class="row" id="addVideoDiv" style="display:none">
					<form id="frm">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group col-md-6">
		        			<label for="helptext">Title</label>
		        			<input type="text" class="form-control ct-input--border ct-u-text--dark" placeholder="Title" name="title">
		    			</div>
		    			<div class="form-group col-md-6">
		        			<label for="helptext">URL</label>
		        			<input type="text" class="form-control ct-input--border ct-u-text--dark" placeholder="url" name="url">
		    			</div>
		    			<div class="form-group col-md-4">
		    			</div>
		    			<div class="form-group col-md-4 button">
		        			<a class="btn btn-success btn-block" id="saveVideo">Save Video</a>
		    			</div>
					</form>
				</div>
				<br>
			</div>

			<table class="table table-striped">
				<thead>
					<tr>
						<!-- <th> S/N </th> -->
						<th> Title </th>
						<th> URL </th>
						<th> Action </th>
					</tr>

				</thead>
				<tbody>
					@foreach($videos as $x=>$video)
						<tr data-id="{{ $video->id }}">
							<!-- <td> {{ $x + 1 }} </td> -->
							<td> {{ $video->Title }} </td>
							<td> {{ $video->URL }} </td>
							<td> <a class="delete-video"><i class="fa fa-trash"></i> </a> </td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class="pull-right pagination-container">{!! $videos->render() !!}</div>
	    </div>
	</section>
@stop


@section('scripts')
<script type="text/javascript">
	$(function(){
		$("#addVideo").on("click",function(){
			$("#addVideo").css("display","none");
			$("#addVideoDiv").css("display","block");
		});
		$("#saveVideo").on("click",function(){
			var formData=JSON.parse(JSON.stringify($("#frm").serializeArray()));
			$.ajax({

				type:'Post',
				url:'{{url("/admin/videos/save")}}',
				data: formData,
				success:function(){
						window.location = '{{ url("/admin/videos") }}';
				},
				error:function(){
					alert("Error");
				}
			});
		});
	})

	
	$(".delete-video").off('click');
	$(".delete-video").on('click', function(e) {
		e.preventDefault();
		name = $(this).parents('tr:eq(0)').find('td:eq(0)').text()
		tr = $(this).parents('tr:eq(0)');
		id = $(this).parents('tr:eq(0)').data('id');
		bootbox.confirm("Are you sure want to delete: "+name, function(e) {		
			if (e) {
				var req = {'_token': "{{ csrf_token() }}"};
				req.video_id = id;
				$.ajax({
					type	:	'post',
					url 	:	'{{ url('/admin/delete-video') }}',
					contentType	: "appalication/json", 
					data	:	JSON.stringify(req)
				}).done(function(res) {
					// console.log(res);
					if(res.status == 1) {
						$(tr).hide('slow');
						// bootbox.alert(res.message, function(){});
						// toastr.success(res.message);
					}
					else {
						// bootbox.alert(res.message, function(){});
					}
				});
			};
		});
	});
</script>	
@stop
