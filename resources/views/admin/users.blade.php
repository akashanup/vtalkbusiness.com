@extends('master')

@section('title')
	<title> Users </title>
@stop

@section('breadcrumb')
	<!-- BreadCrumbs -->
	<div class="ct-site--map">
	    <div class="container">
	        <a href="{{ url('/dashboard') }}"> Dashboard </a>
	        <a href="{{ url('/admin/users') }}">Users</a>
	    </div>
	</div>
	<!-- BreadCrumb Ends -->
@stop

@section('page-header')
	<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
	    <div class="ct-mediaSection-inner">
	        <div class="container">
				<div class="ct-heading--main text-center">
				    <h3 class="text-uppercase ct-u-text--white"> Manage Users </h3>
				</div>
	        </div>
	    </div>
	</header>
@stop

@section('content')
	<section class="ct-u-paddingBoth70 ct-js-section text-left">
	    <div class="container">
			<!-- <div class="ct-heading text-center ct-u-marginBottom60">
			    <h3 class="text-uppercase"> Companies </h3>
			</div> -->

			
			@if($errors->any())
				<ul class="well text-danger">
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif

			<table class="table table-striped">
				<thead>
					<tr>
						<!-- <th> S/N </th> -->
						<th> Name </th>
						<th> E-mail </th>
						<th> Role </th>
						<th> Action </th>
					</tr>

				</thead>
				<tbody>
					@foreach($users as $x=>$user)
						<tr data-id="{{ $user->id }}">
							<!-- <td> {{ $x + 1 }} </td> -->
							<td> {{ $user->name }} </td>
							<td> {{ $user->email }} </td>
							<td> {{ $user->role }} </td>
							<td>
								<!-- <a href=""> <i class="fa fa-eye"></i> </a> |  -->
								<a class="ban-this-user">
								@if($user->ban == 1)
									<i title="Banned" class="fa fa-times"></i> 
								@else
									<i title="Not Banned" class="fa fa-check-square-o"></i>
								@endif
								</a> | <a class="delete-user"> <i class="fa fa-trash"></i> </a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class="pull-right pagination-container">{!! $users->render() !!}</div>
	    </div>
	</section>
@stop


@section('scripts')
	<script type="text/javascript">
		$(".delete-user").off('click');
		$(".delete-user").on('click', function(e) {
			e.preventDefault();
			name = $(this).parents('tr:eq(0)').find('td:eq(0)').text()
			tr = $(this).parents('tr:eq(0)');
			id = $(this).parents('tr:eq(0)').data('id');
			bootbox.confirm("Are you sure want to delete: "+name, function(e) {	
				if (e) {
					var req = {'_token': "{{ csrf_token() }}"};
					req.user_id = id;
					$.ajax({
						type	:	'post',
						url 	:	'{{ url('/admin/delete-user') }}',
						contentType	: "appalication/json", 
						data	:	JSON.stringify(req)
					}).done(function(res) {
						// console.log(res);
						if(res.status == 1) {
							$(tr).hide('slow');
							// bootbox.alert(res.message, function(){});
							// toastr.success(res.message);
						}
						else {
							// bootbox.alert(res.message, function(){});
						}
					});
				};
			});
		});

		$('.ban-this-user').off('click');
		$('.ban-this-user').on('click', function(e) {
			e.preventDefault();
			a = $(this);
			id = $(this).parents('tr:eq(0)').data('id');
			var req = {'_token': "{{ csrf_token() }}"};
			req.action = 'changeuserBanStatus';
			req.status = 1;
			if ($(this).find('i').hasClass('fa-times')) {
				req.status = 0;
			};
			req.user_id = id;
			$.ajax({
				type	:	'post',
				url		:	'{{ url('/admin/user-ban-status') }}',
				data	:	JSON.stringify(req),
				contentType	: "appalication/json", 
			}).done(function(res) {
				// console.log(res);
				// res = $.parseJSON(res);
				if(res.status == 1) {
					if (req.status == 1) {
						$(a).find('i').removeClass('fa-check-square-o').addClass('fa-times').attr('title', 'Banned');;
					}else{
						$(a).find('i').removeClass('fa-times').addClass('fa-check-square-o').attr('title', 'Not Banned');;
					};
					// bootbox.alert(res.message, function(){});
					// toastr.info(res.message);
					// Product.fetchAllProducts();
				}
				else {
					bootbox.alert(res.message, function(){});
					// toastr.error(res.message);
				}
			});	
		});

	</script>
@stop

















