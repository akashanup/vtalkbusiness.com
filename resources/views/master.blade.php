<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="description" content="Estato - Developers HTML Template">
	<meta name="author" content="CreateIT">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	@yield('meta')
	<link rel="icon" href="{{ asset('/images/logo.png') }}">
	@yield('title')
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/font-awesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
	@yield('styles')
</head>
<body class="ct-headroom--scrollUpBoth cssAnimate">
	<nav class="ct-menuMobile">
	    <ul class="ct-menuMobile-navbar">
	    	<li><a href="{{ url() }}">Home</a></li>
	    	<li class="dropdown"><a>Opportunities<span class="caret"></span></a>
	        	<ul class="dropdown-menu">
	        		<li><a href="{{ url('/search?name=&category=&city=&country=&tags=1,2&minPrice=0&maxPrice=10000000000') }}">Distribution/Franchise</a></li>
	        		<li><a href="{{ url('/search?name=&category=&city=&country=&tags=3&minPrice=0&maxPrice=10000000000') }}">Sell Business</a></li>
	        		<li><a href="{{ url('/search?name=&category=&city=&country=&tags=4&minPrice=0&maxPrice=10000000000') }}">Buy Business</a></li>
	        		<li><a href="{{ url('/search?name=&category=&city=&country=&tags=5&minPrice=0&maxPrice=10000000000') }}">Merger & Acqusition</a></li>
	        		<li><a href="{{ url('/search?name=&category=&city=&country=&tags=7&minPrice=0&maxPrice=10000000000') }}">Technology Transfer</a></li>
	        	</ul>
	        </li>
	    	<!-- <li><a href="{{ url('/future') }}">Sell/Buy Products</a></li> -->
	    	<li><a href="{{ url('/opportunity') }}">Post Opportunity</a></li>
	        <li><a href="{{ url('/companies') }}">Companies</a></li>
	        <li><a href="{{ url('/investors') }}">Investors</a></li>
	        @if(Session::has('user_role') && Session::get('user_role') == 4)
	        <li class="dropdown"><a>Admin<span class="caret"></span></a>
	        	<ul class="dropdown-menu">
	        		<li><a href="{{ url('/admin/companies') }}">Companies</a></li>
	        		<li><a href="{{ url('/admin/investors') }}">Investors</a></li>
	        		<li><a href="{{ url('/admin/users') }}">Users</a></li>
	        		<li><a href="{{ url('/admin/videos') }}">Youtube videos</a></li>
	        		<li><a href="{{ url('/admin/contacts') }}">Contact details</a></li>
	        		<li><a href="{{ url('/admin/enquiries') }}">Enquiries</a></li>
	        	</ul>
	        </li>
	        @endif
		    @if(Session::has('user_id'))
            <li><a href="{{ url('/userProfile') }}">My Profile</a></li>
            <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            <li><a href="{{ url('/logout') }}">Logout</a></li>
            @else
            <li><a href="{{ url('/register') }}">Register Now</a></li>
            <li><a href="{{ url('/login') }}">Login</a></li>
            <li><a href="{{ url('/contact') }}">Contact Us</a></li>
            @endif
	    </ul>
	</nav>
	<!-- <form class="ct-searchFormMobile ct-u-marginBottom50" role="form">
	    <div class="form-group ">
	        <div class="ct-form--label--type1">
	            <div class="ct-u-displayTableVertical">
	                <div class="ct-u-displayTableCell">
	                    <div class="ct-input-group-btn">
	                        <button class="btn btn-primary">
	                            <i class="fa fa-search"></i>
	                        </button>
	                    </div>
	                </div>
	                <div class="ct-u-displayTableCell text-center">
	                    <span class="text-uppercase">Search for property</span>
	                </div>
	            </div>
	        </div>
	        <div class="ct-u-displayTableVertical ct-u-marginBottom20">
	            <div class="ct-u-displayTableCell">
	                <div class="ct-form--item">
	                    <label>Property id</label>
	                    <input type="text" required class="form-control input-lg" placeholder="Any">
	                </div>
	            </div>
	            <div class="ct-u-displayTableCell">
	                <div class="ct-form--item">
	                    <label>Location</label>
	                    <select class="ct-js-select ct-select-lg">
	                        <option value="any">Any</option>
	                        <option value="1">New York</option>
	                        <option value="2">New Jersey</option>
	                        <option value="3">Newark</option>
	                        <option value="4">Philadelphia</option>
	                    </select>
	                </div>
	            </div>
	            <div class="ct-u-displayTableCell">
	                <div class="ct-form--item">
	                    <label>Sub-Location</label>
	                    <select class="ct-js-select ct-select-lg">
	                        <option value="any">Any</option>
	                        <option value="1">New York</option>
	                        <option value="2">Jersey</option>
	                        <option value="3">Newark</option>
	                        <option value="4">Philadelphia</option>
	                    </select>
	                </div>
	            </div>
	            <div class="ct-u-displayTableCell">
	                <div class="ct-form--item">
	                    <label>Property Status</label>
	                    <select class="ct-js-select ct-select-lg">
	                        <option value="any">Any</option>
	                        <option value="1">Sale</option>
	                        <option value="2">New</option>
	                        <option value="3">Loan</option>
	                    </select>
	                </div>
	            </div>
	            <div class="ct-u-displayTableCell">
	                <div class="ct-form--item">
	                    <label>Property type </label>
	                    <select class="ct-js-select ct-select-lg">
	                        <option value="any">Any</option>
	                        <option value="1">Houses</option>
	                        <option value="2">Industrial</option>
	                        <option value="3">Retail</option>
	                        <option value="4">Apartments</option>
	                    </select>
	                </div>
	            </div>
	        </div>
	        <div class="ct-u-displayTableVertical ct-slider--row">
	            <div class="ct-u-displayTableCell">
	                <div class="ct-form--item">
	                    <label>Bedrooms</label>
	                    <select class="ct-js-select ct-select-lg">
	                        <option value="any">Any</option>
	                        <option value="1">1</option>
	                        <option value="2">2</option>
	                        <option value="3">3+</option>
	                    </select>
	                </div>
	            </div>
	            <div class="ct-u-displayTableCell">
	                <div class="ct-form--item">
	                    <label>Bathrooms</label>
	                    <select class="ct-js-select ct-select-lg">
	                        <option value="any">Any</option>
	                        <option value="1">1</option>
	                        <option value="2">2</option>
	                        <option value="3">3+</option>
	                    </select>
	                </div>
	            </div>
	            <div class="ct-u-displayTableCell ct-u-marginBottom40">
	                <div class="ct-form--item ct-sliderAmount">
	                    <label>Price ($)</label>
	                    <input type="text" value="1000000" required class="form-control input-lg ct-js-slider-min ct-u-marginBottom20" placeholder="">
	                    <input type="text" class="slider ct-js-sliderAmount" value="" data-slider-tooltip="hide" data-slider-handle="square" data-slider-min="5000" data-slider-max="5000000" data-slider-step="5000" data-slider-value="[1000000,2000000]">
	                    <label class="pull-left">Min</label>
	                    <label class="pull-right">Max</label>
	                    <input type="text" value="2000000" required class="form-control input-lg ct-js-slider-max" placeholder="">
	                </div>
	            </div>
	            <div class="ct-u-displayTableCell">
	                <input type="text" class="slider ct-js-sliderTicks" value="" data-slider-handle="square" data-slider-min="0" data-slider-max="200" data-slider-step="20" data-slider-value="[60,120]"/>
	                <label class="text-center center-block">Area (m2)</label>
	            </div>
	            <div class="ct-u-displayTableCell">
	                <button type="submit" class="btn btn-warning text-capitalize pull-right">search now</button>
	            </div>
	        </div>
	    </div>
	</form>  -->
	<div id="ct-js-wrapper" class="ct-pageWrapper" style="">

		<div class="ct-navbarMobile">
		    <button type="button" class="navbar-toggle">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		    </button>
		    <a class="navbar-brand" href="{{ url() }}"><img src="{{ asset('/images/vtalk-logo.png') }}" alt="VTalk Business"> </a>
		    <button type="button" class="searchForm-toggle">
		        <span class="sr-only">Toggle navigation</span>
		        <span><i class="fa fa-search"></i></span>
		    </button>
		</div>

		<div class="ct-topBar  ct-js-headroom animatedHeadroom fadeInDown headroom--top">
		    <div class="container">
		        <div class="row">
		            <div class="col-sm-7 col-md-5">
		                <div class="ct-panel--contact ct-panel--left">
		                    <div class="ct-panel--item ct-email">
		                        <a href="mailto:support@vtalkbusiness.com"><i class="fa fa-envelope-o"></i> support@vtalkbusiness.com</a>
		                    </div>
		                    <!-- <ul class="nav navbar-nav ct-switcher--language">
		                        <li class="dropdown">
		                            <a href="#">
		                                <i class="fa fa-globe"></i>
		                                English
		                                <i class="fa fa-angle-down"></i>
		                            </a>
		                            <ul class="dropdown-menu">
		                                <li>
		                                    <a href="#">
		                                        English
		                                    </a>
		                                </li>
		                                <li>
		                                    <a href="#">
		                                        Spanish
		                                    </a>
		                                </li>
		                                <li>
		                                    <a href="#">
		                                        German
		                                    </a>
		                                </li>
		                            </ul>
		                        </li>
		                    </ul> -->
		                    <!-- <div id="ct-js-navSearch">
		                        <i class="fa fa-search"></i>
		                        <input placeholder="Search ..." required="" type="text" name="field[]" class="form-control input-lg ct-input--search">
		                    </div>
		                    <div class="ct-navbar-search" style="display: none;">
		                        <form role="form">
		                            <button class="ct-navbar-search-button" type="submit">
		                                <i class="fa fa-search fa-fw"></i>
		                            </button>
		                            <div class="form-group">
		                                <input id="search" placeholder="Search ..." required="" type="text" class="form-control input-lg">
		                            </div>
		                        </form>
		                    </div> -->
		                </div>
		            </div>
		            <div class="col-sm-5 col-md-7">
		                <div class="ct-panel--user ct-panel--right text-right">
		                	@if(Session::has('user_id'))
		                	<div class="ct-panel--item ct-email">
		                		<a href="{{ url('/investor') }}"><i class="fa fa-user"></i> Investor Profile</a>
		                	</div>
		                	<div class="ct-panel--item ct-email">
		                		<a href="{{ url('/user_profile') }}"><i class="fa fa-user"></i> My Profile</a>
		                	</div>
		                    <div class="ct-panel--item ct-email">
		                        <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i> My Dashboard</a>
		                    </div>
		                    <div class="ct-panel--item ct-email">
		                        <a href="{{ url('/logout') }}">Logout</a>
		                    </div>
		                    @else
		                    <div class="ct-panel--item ct-email">
		                        <a href="{{ url('/login') }}"><i class="fa fa-user"></i> Login</a>
		                    </div>
		                    <div class="ct-panel--item ct-email">
		                        <a href="{{ url('/register') }}"><i class="fa fa-plus-square"></i> Register</a>
		                    </div>
		                    @endif
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
		<nav class="navbar yamm ct-navbar--noDecoration ct-js-headroom animatedHeadroom fadeInDown headroom--top pull-right" role="navigation" data-heighttopbar="40px" data-startnavbar="0" style="top: auto;">
		    <div class="container">
		        <div class="navbar-header ct-panel--navbar">
		            <a href="{{ url() }}">
		                <img src="{{ asset('/images/vtalk-logo.png') }}" style="width: 100px;">
		            </a>
		        </div>
		        <div class="ct-panelBox">
		            <div class="ct-panel--text ct-panel--navbar ct-fw-600">30+ Companies</div>
		            <ul class="ct-panel--socials ct-panel--navbar list-inline list-unstyled">
		                <li><a href="https://www.facebook.com/createITpl" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-facebook"></i></div></a></li>
		                <li><a href="https://twitter.com/createitpl" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-twitter"></i></div></a></li>
		                <li><a href="#" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-linkedin"></i></div></a></li>
		            </ul>
		        </div>
		        <div class="collapse navbar-collapse">
		            <ul class="nav navbar-nav ct-navbar--fadeInUp pull-right">
		            	<li><a href="{{ url() }}">Home</a></li>
		            	<li class="dropdown"><a>Opportunities<span class="caret"></span></a>
				        	<ul class="dropdown-menu">
				        		<li><a href="{{ url('/search?name=&category=&city=&country=&tags=1,2&minPrice=0&maxPrice=10000000000') }}">Distribution/Franchise</a></li>
				        		<li><a href="{{ url('/search?name=&category=&city=&country=&tags=3&minPrice=0&maxPrice=10000000000') }}">Sell Business</a></li>
				        		<li><a href="{{ url('/search?name=&category=&city=&country=&tags=4&minPrice=0&maxPrice=10000000000') }}">Buy Business</a></li>
				        		<li><a href="{{ url('/search?name=&category=&city=&country=&tags=5&minPrice=0&maxPrice=10000000000') }}">Merger & Acqusition</a></li>
				        		<li><a href="{{ url('/search?name=&category=&city=&country=&tags=7&minPrice=0&maxPrice=10000000000') }}">Technology Transfer</a></li>
				        	</ul>
				        </li>
		            	<li><a href="{{ url('/opportunity') }}">Post Opportunity</a></li>
				        <li><a href="{{ url('/companies') }}">Companies</a></li>
				        <li><a href="{{ url('/investors') }}">Investors</a></li>
				        @if(!Session::has('user_id'))
				        <li><a href="{{ url('/contact') }}">Contact Us</a></li>
				        @endif
				        @if(Session::has('user_role') && Session::get('user_role') == 4)
				        <li class="dropdown"><a>Admin<span class="caret"></span></a>
				        	<ul class="dropdown-menu">
				        		<li><a href="{{ url('/admin/companies') }}">Companies</a></li>
				        		<li><a href="{{ url('/admin/investors') }}">Investors</a></li>
				        		<li><a href="{{ url('/admin/users') }}">Users</a></li>
				        		<li><a href="{{ url('/admin/videos') }}">Youtube videos</a></li>
				        		<li><a href="{{ url('/admin/contacts') }}">Contact details</a></li>
				        		<li><a href="{{ url('/admin/enquiries') }}">Enquiries</a></li>
				        	</ul>
				        </li>
				        @endif
		            </ul>
		            <!-- @if(Session::has('user_id'))
		            <a class="btn btn-primary btn-transparent--border ct-u-text--motive" href="{{ url('/dashboard') }}">Dashboard</a>
		            <a class="btn btn-primary btn-transparent--border ct-u-text--motive" href="{{ url('/logout') }}">Logout</a>
		            @else
		            <a class="btn btn-primary btn-transparent--border ct-u-text--motive" href="{{ url('/register') }}">Register Now</a>
		            <a class="btn btn-primary btn-transparent--border ct-u-text--motive" href="{{ url('/login') }}">Login</a>
		            @endif -->
		        </div>
		        <div class="ct-shapeBottom"></div>
		    </div>
		</nav>
		<!-- Nav Bar ends here -->
		@yield('breadcrumb')

		<!-- Page Header Starts -->
		@yield('page-header')
		<!-- Page Header ends -->
		@yield('content')

		<!-- Footer starts -->
		<footer>
		    <!-- <div class="ct-footer--extended ct-u-paddingBoth60">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-6 col-md-6 col-lg-3">
		                    <h4 class="text-uppercase ct-u-marginBottom30">About estato</h4>
		                    <div class="ct-u-marginBottom30">
		                        <span>Estato is new and powerfull real estate theme. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum nulla vel.</span>
		                    </div>
		                    <ul class="list-unstyled">
		                        <li class="ct-u-marginBottom10">
		                            <i class="fa fa-home"></i>
		                            <span class="ct-fw-600">Address:</span><span> 1234 Street Name, San Fransico, United States</span>
		                        </li>
		                        <li class="ct-u-marginBottom10">
		                            <i class="fa fa-envelope-o"></i>
		                            <span class="ct-fw-600">E-mail:</span><span> example@example.com</span>
		                        </li>
		                        <li>
		                            <i class="fa fa-fax"></i>
		                            <span class="ct-fw-600">Fax:</span><span> 0 123 456 7890</span>
		                        </li>
		                    </ul>
		                </div>
		                <div class="col-sm-6 col-md-6 col-lg-3">
		                    <h4 class="text-uppercase ct-u-marginBottom30">Contact us</h4>
		                    <ul class="list-unstyled ct-phoneNumbers ct-u-marginBottom30">
		                        <li>
		                            <i class="fa fa-phone"></i>
		                            <span class="ct-fw-600">0 800 123 4567</span>
		                        </li>
		                        <li>
		                            <i class="fa fa-fax"></i>
		                            <span class="ct-fw-600">0 800 123 4562</span>
		                        </li>
		                        <li>
		                            <i class="fa fa-mobile"></i>
		                            <span class="ct-fw-600">+1 500 700 800</span>
		                        </li>
		                    </ul>
		                    <ul class="ct-panel--socials ct-panel--navbar list-inline list-unstyled ct-u-marginBottom30">
		                        <li><a href="https://www.facebook.com/createITpl"><div class="ct-socials ct-socials--circle"><i class="fa fa-facebook"></i></div></a></li>
		                        <li><a href="https://twitter.com/createitpl"><div class="ct-socials ct-socials--circle"><i class="fa fa-twitter"></i></div></a></li>
		                        <li><a href="#"><div class="ct-socials ct-socials--circle"><i class="fa fa-instagram"></i></div></a></li>
		                    </ul>
		                    <div class="ct-contactList ">
		                        <a href="#"><i class="fa fa-envelope-o"></i>questions@estato.com</a>
		                        <a href="#"><i class="fa fa-envelope-o"></i>sales@estato.com</a>
		                    </div>
		                </div>
		                <div class="col-sm-6 col-md-6 col-lg-3 ">
		                    <h4 class="text-uppercase ct-u-marginBottom30">Quick links</h4>
		                    <div class="row">
		                        <div class="ct-links">
		                            <div class="col-md-6">
		                                <a class="text-capitalize" href="#">List Property</a>
		                                <a class="text-capitalize" href="#">Home</a>
		                                <a class="text-capitalize" href="#">Properties</a>
		                                <a class="text-capitalize" href="#">Agents</a>
		                                <a class="text-capitalize" href="#">Fratures</a>
		                                <a class="text-capitalize" href="#">About Us</a>
		                                <a class="text-capitalize" href="#">Contact Us</a>
		                            </div>
		                            <div class="col-md-6">
		                                <a class="text-capitalize" href="#">Login</a>
		                                <a class="text-capitalize" href="#">Register</a>
		                                <a class="text-capitalize" href="#">Pricing</a>
		                                <a class="text-capitalize" href="#">Terms &amp; Conditions</a>
		                                <a class="text-capitalize" href="#">Privacy Policy</a>
		                                <a class="text-capitalize" href="#">Purchase Estato</a>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class="col-sm-6 col-md-6 col-lg-3">
		                    <h4 class="text-uppercase ct-u-marginBottom30">Latest Listings</h4>
		                    <div class="ct-itemProducts--small ct-itemProducts--small-type2">
		                        <a href="product-single.html">
		                            <div class="ct-main-content">
		                                <div class="ct-imageBox">
		                                    <img src="assets/images/content/recently-reduced-thumbnail-1.jpg" alt="">
		                                </div>
		                                <div class="ct-main-text">
		                                    <div class="ct-product--tilte">
		                                        9544 Umber View, Inspiration
		                                    </div>
		                                    <div class="ct-product--price">
		                                        <span>$ 650/month</span>
		                                    </div>
		                                </div>
		                            </div>
		                        </a>
		                    </div>
		                    <div class="ct-itemProducts--small ct-itemProducts--small-type2">
		                        <a href="product-single.html">
		                            <div class="ct-main-content">
		                                <div class="ct-imageBox">
		                                    <img src="assets/images/content/recently-reduced-thumbnail-2.jpg" alt="">
		                                </div>
		                                <div class="ct-main-text">
		                                    <div class="ct-product--tilte">
		                                        15421 Southwest 39th Terrace
		                                    </div>
		                                    <div class="ct-product--price">
		                                        <span>$ 650/month</span>
		                                    </div>
		                                </div>
		                            </div>
		                        </a>
		                    </div>
		                    <div class="ct-itemProducts--small ct-itemProducts--small-type2">
		                        <a href="product-single.html">
		                            <div class="ct-main-content">
		                                <div class="ct-imageBox">
		                                    <img src="assets/images/content/recently-reduced-thumbnail-3.jpg" alt="">
		                                </div>
		                                <div class="ct-main-text">
		                                    <div class="ct-product--tilte">
		                                        4079 Broad Falls, Turkeytown
		                                    </div>
		                                    <div class="ct-product--price">
		                                        <span>$ 650/month</span>
		                                    </div>
		                                </div>
		                            </div>
		                        </a>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div> -->
		    <div class="ct-postFooter ct-u-paddingBoth20">
		        <a href="#" class="ct-scrollUpButton ct-js-btnScrollUp">
		           <span class="ct-sectioButton--square">
		               <i class="fa fa-angle-double-up"></i>
		           </span>
		        </a>
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-6 col-md-6 col-sm-push-6">
		                	<a class="btn btn-primary btn-transparent--border ct-u-text--motive text-capitalize pull-right" href="{{ url('/contact') }}">Contact Us</a>
		                    <!-- <div class="ct-newsletter text-right">
		                        <span class="text-uppercase ct-u-text--white ct-fw-600">Join our newsletter</span>
		                        <input id="newsletter" placeholder="Your E-mail Address" required="" type="text" name="field[]" class="form-control input-lg">
		                        <button type="submit" class="btn btn-primary btn-transparent--border ct-u-text--motive text-capitalize">Subscribe</button>
		                    </div> -->
		                </div>
		                <div class="col-sm-6 col-md-6 col-sm-pull-6">
		                    <span class="ct-copyright">© 2016 VTalk Business. All rights reserved.</span><!-- We accept:  </span>
		                    <ul class="icons list-unstyled list-inline">
		                        <li>
		                            <i class="fa fa-cc-stripe" data-toggle="tooltip" data-placement="top" title="" data-original-title="Stripe"></i>
		                        </li>
		                        <li>
		                            <i class="fa fa-cc-paypal" data-toggle="tooltip" data-placement="top" title="" data-original-title="PayPal"></i>
		                        </li>
		                        <li>
		                            <i class="fa fa-cc-mastercard" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mastercard"></i>
		                        </li>
		                        <li>
		                            <i class="fa fa-cc-visa" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visa"></i>
		                        </li>
		                        <li>
		                            <i class="fa fa-cc-discover" data-toggle="tooltip" data-placement="top" title="" data-original-title="Discover"></i>
		                        </li>
		                        <li>
		                            <i class="fa fa-cc-amex" data-toggle="tooltip" data-placement="top" title="" data-original-title="Amex"></i>
		                        </li>
		                    </ul> -->
		                </div>
		            </div>
		        </div>
		    </div>
		</footer>
	</div>
	@yield('modals')
	<script type="text/javascript" src="{{ asset('/js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/bootbox.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/dependencies.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/bootstrap-slider.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/contact-form.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/jquery.stellar.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/main.js') }}"></script>
	<script type="text/javascript">
		var rootUrl = "{{ url() }}";
		var _token = "{{ csrf_token() }}"
	</script>
	<span role="status" aria-live="polite" class="select2-hidden-accessible"></span>
	@yield('scripts')
</body>
</html>