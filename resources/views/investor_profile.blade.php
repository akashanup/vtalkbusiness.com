@extends('master')

@section('title')
	<title>Investor Profile</title>
@stop

@section('breadcrumb')
	<!-- BreadCrumbs -->
	<div class="ct-site--map">
	    <div class="container">
	        <a href="index.html">Features</a>
	        <a href="features-buttons.html">Buttons</a>
	    </div>
	</div>
	<!-- BreadCrumb Ends -->
@stop

@section('page-header')
	<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
	    <div class="ct-mediaSection-inner">
	        <div class="container">
				<div class="ct-heading--main text-center">
				    <h3 class="text-uppercase ct-u-text--white">Create Investor Profile</h3>
				</div>
	        </div>
	    </div>
	</header>
@stop

@section('content')
	<section class="ct-u-paddingBoth70 ct-js-section text-left">
	    <div class="container">
			<div class="ct-heading text-center ct-u-marginBottom60">
			    <h3 class="text-uppercase">Investor Profile</h3>
			</div>

			
			@if($errors->any())
	        	@foreach($errors->all() as $error)
	        	<div class="errorMessage alert alert-danger">
	            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            	{{ $error }}
	        	</div>
	        	@endforeach
	        @endif

			<form method="post" name="investorRegistration" id="frm" action="{{ url('/investor-profile') }}" enctype="multipart/form-data"><!-- action="{{ url('/company/create') }}">-->
				
				<div class="form-group col-md-4">
			        <label for="helptext">Venture Name</label>
			        <input type="text" class="form-control ct-input--border ct-u-text--dark" id="venture_name" placeholder="Venture Name" name="venture_name">
			    </div>
			    <div class="form-group col-md-4">
			        <label for="helptext">Contact Person <span>*</span></label>
			        <input type="text" class="form-control ct-input--border ct-u-text--dark" id="investor_name" placeholder="Contact Person" name="investor_name" value="{{ $user->name }}">
			    </div>
			    <div class="form-group col-md-4">
			        <label for="helptext">Contact Email <span>*</span></label>
			        <input type="text" class="form-control ct-input--border ct-u-text--dark" id="email" placeholder="Venture Email" name="email" value="{{ $user->email }}">
			    </div>
			    <div class="form-group col-md-4">
			        <label for="helptext">Mobile Number <span>*</span></label>
			        <input type="number" class="form-control ct-input--border ct-u-text--dark" id="mobile" placeholder="Mobile Number" name="phone">
			    </div>
			    <div class="form-group col-md-4">
			        <label for="helptext">Skype Username</label>
			        <input type="text" class="form-control ct-input--border ct-u-text--dark" id="skype" placeholder="Skype Username" name="skype">
			    </div>
			    <div class="form-group col-md-4">
			        <label for="helptext">Facebook URL</label>
			        <input type="text" class="form-control ct-input--border ct-u-text--dark" id="facebook" placeholder="Facebook URL" name="facebook">
			    </div>
			    <div class="form-group col-md-4">
			        <label for="helptext">Twitter URL</label>
			        <input type="text" class="form-control ct-input--border ct-u-text--dark" id="twitter" placeholder="Twitter URL" name="twitter">
			    </div>
			    <div class="form-group col-md-4">
			        <label for="helptext">LinkedIn Profile</label>
			        <input type="text" class="form-control ct-input--border ct-u-text--dark" id="linkedin" placeholder="LinkedIn Profile" name="linkedin">
			    </div>
			    <div class="form-group col-md-4">
			        <label for="helptext">Profile Image</label>
			        <span class="btn btn-default">
						<input id="imgage" type="file" name="image">
					</span>
			    </div>
			    <dic class="col-md-12">
			    	<label for="helptext">Interested Industries <span>*</span></label>
			        <select class="ct-input--border ct-u-text-dark" id="interests" name="interests[]" multiple>
			        	@foreach($categories as $category)
			        	<option value="{{ $category->id }}">{{ $category->category }}</option>
			        	@endforeach
			        </select>
			    </dic>
				<div class="col-md-4">
		                <div class="ct-form--item">
		                    <label>Price (INR) <span>*</span></label>
		                    <input type="text" required class="form-control custom-control input-lg ct-js-slider-min" id="min_capital" placeholder="Minimum Capital" name="min_capital" value="100000">
		                </div>
		            </div>
		            <div class="col-md-4">
		                <label>&nbsp;</label>
		                <input type="text" class="ct-js-sliderAmount" value="" data-slider-tooltip="hide" data-slider-handle="square" data-slider-min="100000" data-slider-max="1000000" data-slider-step="100000" data-slider-value="[5000, 10000000000]" id="slider">
		                <label>Min</label>
		                <label class="pull-right">Max</label>
		            </div>
		            <div class="col-md-4">
		                <div class="ct-form--item">
		                	<label>&nbsp;</label>
		                    <input type="text" required class="form-control custom-control input-lg ct-js-slider-max" id="max_capital" placeholder="Maximum Capital" name="max_capital" value="1000000">
		                </div>
		        </div>


			    <!-- <div class="form-group col-md-6">
			        <label for="helptext">Minmum Capital</label>
			        <input type="number" class="form-control ct-input--border ct-u-text--dark" id="min_capital" placeholder="Minimum Capital" name="min_capital">
			    </div>
			    <div class="form-group col-md-6">
			        <label for="helptext">Maximum Capital</label>
			        <input type="number" class="form-control ct-input--border ct-u-text--dark" id="max_capital" placeholder="Maximum Capital" name="max_capital">
			    </div>	-->    
			    
			    <input type="hidden" name="_token" value="{{csrf_token()}}">
			    <div class="form-group col-md-12 button">
			        <input type="Submit"id="submit"class="btn btn-info btn-block"value="Save" >
			    </div>
			</form>
	    </div>
	</section>
@stop


@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$('#interests').select2();
			$('#slider').slider().on('slide', function(){
				var data = $('#slider').data('slider').getValue();
				$('#min_capital').val(data[0]);
				$('#max_capital').val(data[1]);
			});
			$('#submit').click(function(e){
				
	    		if(!($("#min_capital").val()))    	
	    		{
	    			setError($("#min_capital"), "Minimu Capital field can't be blank.");
	    			e.preventDefault();
	    		}
	    		else
				{
	    			unsetError($("#min_capital"));			
	    		}


	    		if(!($("#max_capital").val()))    	
	    		{
	    			setError($("#max_capital"), "Maximum Capital cannot be blank.");
	    			e.preventDefault();
	    		}
	    		else
				{
	    			unsetError($("#max_capital"));			
	    		}

	    		
				if($("#mobile").val().length != 10)
				{
				 	setError($("#mobile"), "Mobile Number Must Be Of 10 Digits.");
				 	e.preventDefault();
				}
				else
				{
					unsetError($("#mobile"));
				}

			});
			$('#min_capital').on('blur', function(){
				if($(this).val() == '')
				{
				 	setError($(this), "Minimum Capital cannot be blank.");
				}
				else
				{
					unsetError($(this));
				}
			});

			$('#mobile').on('blur', function(){
				if($(this).val().length != 10)
				{
				 	setError($(this), "Mobile Number Must Be Of 10 Digits.");
				}
				else
				{
					unsetError($(this));
				}
			});

			$('#max_capital').on('blur', function(){
				if($(this).val() == '')
				{
				 	setError($(this), "Maximum Capital cannot be blank.");
				}
				else
				{
					unsetError($(this));
				}
			});
		});

	</script>
@stop

















