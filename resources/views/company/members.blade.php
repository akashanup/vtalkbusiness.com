@extends('master')

@section('title')
<title>Add Team Member</title>
@stop

@section('breadcrumb')
<!-- BreadCrumbs -->
<div class="ct-site--map">
    <div class="container">
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <a href="{{ url('/dashboard/'.$company->id) }}">{{ $company->name }}</a>
        <a href="#">Add Memebers</a>
    </div>
</div>
<!-- BreadCrumb Ends -->
@stop

@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
    <div class="ct-mediaSection-inner">
        <div class="container">
			<div class="ct-heading--main text-center">
			    <h3 class="text-uppercase ct-u-text--white">Add Members</h3>
			</div>
        </div>
    </div>
</header>
@stop

@section('content')
<section class="ct-u-paddingBoth70 ct-js-section text-left">
    <div class="container">
		<div class="ct-heading text-center ct-u-marginBottom60">
		    <h3> Add new Team Members for {{ $company->name }}</h3>
		</div>
		@if($errors->any())
        	@foreach($errors->all() as $error)
        	<div class="errorMessage alert alert-danger">
            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            	{{ $error }}
        	</div>
        	@endforeach
        @endif
		<form action="{{ url('/company/'.$company->id.'/add-member') }}" method="post">
			<div class="row">
				<div class="form-group col-md-6">
					<label>Member Name</label>
					<input type="text" name="name[]" value="{{ old('name') }}" id="name" class="form-control ct-input--border ct-u-text--dark">
				</div>
				<div class="form-group col-md-6">
					<label>Member Email</label>
					<input type="email" name="email[]" value="{{ old('email') }}" id="email" class="form-control ct-input--border ct-u-text--dark">
				</div>
				<div class="form-group col-md-6">
					<label>Member Phone</label>
					<input type="text" name="phone[]" value="{{ old('phone') }}" id="phone" class="form-control ct-input--border ct-u-text--dark">
				</div>
				<div class="form-group col-md-6">
					<label>Member Designation</label>
					<input type="text" name="designation[]" value="{{ old('designation') }}" id="designation" class="form-control ct-input--border ct-u-text--dark">
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token">
				<div class="form-group col-md-6">
					<label>&nbsp;</label>
					<input type="submit" class="btn btn-info" value="Save">
				</div>
				<div class="form-group col-md-6">
					<label>&nbsp;</label>
					<button class="btn btn-primary form-control  ct-input--border ct-u-text--dark" id="add-more"><i class="fa fa-plus-circle"></i>  Add more members</button>
				</div>
			</div>
		</form>
    </div>
</section>
@stop

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('#add-more').on('click', function(e) {
			e.preventDefault();
			var html = '<div class="form-group col-md-6">'
					+ '<label>Member Name</label>'
					+ '<input type="text" name="name[]" value="{{ old('name') }}" id="name" class="form-control ct-input--border ct-u-text--dark">'
				+ '</div>'
				+ '<div class="form-group col-md-6">'
					+ '<label>Member Email</label>'
					+ '<input type="email" name="email[]" value="{{ old('email') }}" id="email" class="form-control ct-input--border ct-u-text--dark">'
				+ '</div>'
				+ '<div class="form-group col-md-6">'
					+ '<label>Member Phone</label>'
					+ '<input type="text" name="phone[]" value="{{ old('phone') }}" id="phone" class="form-control ct-input--border ct-u-text--dark">'
				+ '</div>'
				+ '<div class="form-group col-md-6">'
					+ '<label>Member Designation</label>'
					+ '<input type="text" name="designation[]" value="{{ old('designation') }}" id="designation" class="form-control ct-input--border ct-u-text--dark">'
				+ '</div>';
			$('#_token').before(html);
		});
	});
</script>
@stop