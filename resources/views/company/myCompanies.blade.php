@extends('master')

@section('title')
<title>My Companies</title>
@stop

@section('breadcrumb')
<!-- BreadCrumbs -->
<div class="ct-site--map">
    <div class="container">
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <a href="{{ url('/my-companies') }}">My Companies</a>
    </div>
</div>
<!-- BreadCrumb Ends -->
@stop

@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
    <div class="ct-mediaSection-inner">
        <div class="container">
			<div class="ct-heading--main text-center">
			    <h3 class="text-uppercase ct-u-text--white">My Companies</h3>
			</div>
        </div>
    </div>
</header>
@stop

@section('content')
<section class="ct-u-paddingBoth70 ct-js-section text-left">
    <div class="container">
		<div class="col-md-10 col-md-offset-1">
			@if(count($company))
				<p><a class="btn btn-primary btn-sm" href="{{ url('/company/add') }}" role="button"><i class="fa fa-plus-circle"></i> Add Company</a></p>
				<div class="company-container">
					<h3>Registered Companies</h3>
					<ul>
						<br>
						@foreach($company as $c)
							<a href="{{ url('/company/'. $c->id) }}"><li>{{$c->name}}</li></a>
						@endforeach
					</ul>

				</div>
			@else
				<div class="jumbotron">
					<h1>Investors are waiting</h1>
					<p>Create a company profile with us to make your business better</p>
					<p>
						<a class="btn btn-primary btn-lg" href="{{ url('/company/add') }}" role="button">Create Company Profile</a>
					</p>
				</div>
			@endif
		</div>
	</div>
</section>
@stop