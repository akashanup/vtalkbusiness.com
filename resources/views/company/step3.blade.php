@extends('master')

@section('title')
<title>Create Company - Step 3</title>
@stop

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jcrop.css') }}">
@stop

@section('breadcrumb')
<div class="ct-site--map">
    <div class="container">
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <a href="{{ url('/company/add') }}">Create Company</a>
        <a href="{{ url('/company/add/3') }}">Step 3</a>
    </div>
</div>
@stop

@extends('company.header')


@section('content')
<section class="ct-u-paddingTop50 ct-u-paddingBottom100">
    <div class="container">
        <div class="ct-headerText--normal text-uppercase ct-u-marginBottom40">
            <h2>Step 3<br>
                <span class="ct-text--highlightGray">Company Image and Tags</span>
            </h2>
        </div>
        <div class="row">
            @if($errors->any())
                @foreach($errors->all() as $error)
                <div class="errorMessage alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $error }}
                </div>
                @endforeach
            @endif

            <form method="post" name="companyRegistration" id="frm" action="{{ url('/company/add/3') }}">
                <input type="hidden" name="company_id" value="{{ Session::get('company_id') }}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="col-md-12 custom-panel">
                    <label>Your Designation in Company *</label>
                    <input type="text" name="designation" class="form-control custom-control" value="{{ old('designation') }}">
                </div>
                <div class="col-md-12 product-container custom-panel">
                    <div class="col-md-12">
                        <h4>Company Products</h4>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4 form-group">
                            <label for="product">Product Name</label>
                            <input type="text" class="form-control product custom-control" name="products[]">
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="product">Product Information</label>
                            <input type="text" class="form-control info custom-control" name="info[]">
                        </div>
                        <div class="col-md-4 form-group">
                            <label>&nbsp;</label>
                            <button class="btn btn-info btn-block" id="add-products"><i class="fa fa-plus-circle"></i> Add More Products</button>
                        </div>
                    </div>
                    <input type="hidden" id="more-products">
                </div>
                <div class="col-md-12 custom-panel">
                    <div class="col-md-12 team-member"><h4>Team Members</h4></div>
                    <div class="col-md-3 form-group">
                        <label>Member name</label>
                        <input type="text" name="names[]" class="form-control name custom-control">
                    </div>
                    <div class="col-md-3 form-group">
                        <label>Memeber Designation</label>
                        <input type="text" name="designations[]" class="form-control designation custom-control">
                    </div>
                    <div class="col-md-3 form-group">
                        <label>Member Email</label>
                        <input type="text" name="emails[]" class="form-control email custom-control">
                    </div>
                    <div class="col-md-3 form-group">
                        <label>Member Phone</label>
                        <input type="text" name="phones[]" class="form-control phone custom-control">
                    </div>
                </div>
                <input type="hidden" id="more-members">

                <div class="col-md-6 form-group">
                   <button class="btn btn-info btn-block" id="add-members"><i class="fa fa-plus-circle"></i> Add More Members</button>
                </div>
                <div class="form-group col-md-6 button">
                    <button type="submit" id="submit" class="btn btn-info btn-block">Save</button>
                </div>
            </form>
        </div>
    </div>
</section>
@stop

@section('scripts')
<script type="text/javascript">
    $(function() {
        $('.ct-steps--submission .ct-steps--item:eq(0)').addClass('ct-steps--past');
        $('.ct-steps--submission .ct-steps--item:eq(1)').addClass('ct-steps--progress');
        $('.ct-steps--submission .ct-steps--item:eq(2)').find('.ct-steps--counter').addClass('ct-steps--active');

        $('#add-products').on('click', function(e) {
            e.preventDefault();
            var html = '<div class="col-md-12">\
                    <div class="col-md-4 form-group">\
                        <label for="product">Product Name</label>\
                        <input type="text" class="form-control product custom-control" name="products[]">\
                    </div>\
                    <div class="col-md-4 form-group">\
                        <label for="product">Product Information</label>\
                        <input type="text" class="form-control info custom-control" name="info[]">\
                    </div></div>';
            $('#more-products').before(html);
        });

        $('#add-members').on('click', function(e) {
            e.preventDefault();
            var a = $('.team-member');
            if(a.length >= 2) {
                $(this).addClass('disabled');
                return;
            }
            var html = '<div class="col-md-12 team-member"><div class="col-md-3 form-group">\
                        <label>Member name</label>\
                        <input type="text" name="names[]" class="form-control name custom-control">\
                    </div>\
                    <div class="col-md-3 form-group">\
                        <label>Memeber Designation</label>\
                        <input type="text" name="designations[]" class="form-control designation custom-control">\
                    </div>\
                    <div class="col-md-3 form-group">\
                        <label>Member Email</label>\
                        <input type="text" name="emails[]" class="form-control email custom-control">\
                    </div>\
                    <div class="col-md-3 form-group">\
                        <label>Member Phone</label>\
                        <input type="text" name="phones[]" class="form-control phone custom-control">\
                    </div></div>';
            $('#more-members').before(html);
        });
    });
</script>
@stop