@extends('master')

@section('title')
<title>All Companies</title>
@stop

@section('breadcrumb')
<!-- BreadCrumbs -->
<div class="ct-site--map">
    <div class="container">
    </div>
</div>
<!-- BreadCrumb Ends -->
@stop

@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
    <div class="ct-mediaSection-inner">
        <div class="container">
            <div class="ct-heading--main text-center">
                <h3 class="text-uppercase ct-u-text--white">All Companies</h3>
            </div>
        </div>
    </div>
</header>
@stop

@section('content')
    <section class="ct-u-paddingBoth70 ct-js-section text-left">
        <div class="container">
            @if($errors->any())
                @foreach($errors->all() as $error)
                <div class="errorMessage alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $error }}
                </div>
                @endforeach
            @endif

            <div class="row">
                <div class="col-md-12 result-container" id="relatedCompany">
                    @foreach($companies as $company)
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="ct-itemProducts ct-u-marginBottom30 ct-hover">
                                @if(Session::has('user_id'))
                                    <a class="authUser" href="{{url('/company/'.$company->id)}}" >
                                @else
                                    <a class="authUser" data-toggle="modal" data-target="#user-modal">
                                @endif
                                    <div class="ct-main-content">
                                        <div class="ct-imageBox">
                                            <div class="logo-middle-container"><img src="{{ $company->logo }}" alt=""></div><i class='fa fa-eye'></i>
                                        </div>
                                        <div class="ct-main-text">
                                            <div class="ct-product--tilte one-line-elipsis" title="{{ $company->name }}">
                                                {{$company->name}}
                                            </div>
                                            <div class="ct-product--price">
                                                <p class="one-line-elipsis">{{$company->countries}}</p>
                                            </div>
                                            <div class="ct-product--description one-line-elipsis">
                                                {{$company->tagline}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ct-product--meta">
                                        <div class="ct-text">
                                            <span><i class="fa fa-user"></i> {{$company->users_name}}</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-sm-12">
                    <div class="pagination-container pull-right">
                        {!! $companies->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('modals')
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="user-modal" class="modal fade">
    <br><br>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h3 class="modal-title">
                    Please <a class="btn btn-info" href="{{ url('/login') }}">Login</a> OR <a class="btn btn-info" href="{{ url('/register') }}">Register</a> To Continue!!!
                </h3>
            </div>
        </div>
    </div>
</div>
@stop








