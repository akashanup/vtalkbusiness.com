@extends('master')

@section('title')
<title>{{$company->name}}</title>
@stop

@section('breadcrumb')
<!-- BreadCrumbs -->
<div class="ct-site--map">
    <div class="container">
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <a href="{{ url('/company/'.$company->id) }}">{{ $company->name }}</a>
    </div>
</div>
<!-- BreadCrumb Ends -->
@stop


@section('content')
<header class="ct-mediaSection ct-u-paddingBoth100" data-stellar-background-ratio="0.3" data-height="470" data-type="parallax" data-bg-image="{{ $company->cover }}" data-bg-image-mobile="{{ $company->cover }}">
@if($company->user_id == Session::get('user_id'))
    <a class="btn btn-xs btn-edit text-uppercase ct-js-btnEdit--Agents" data-toggle="modal" data-target="#cover-upload-modal" style="margin-top: 25px; right: 25px;">edit</a>
@endif
    <div class="container">
        <div class="ct-personBox ct-personBox--extended ct-personBox--extendedLight text-left">
            <div class="ct-imagePerson">
                <div class="logo-middle-container"><img src="{{ $company->logo }}" alt=""></div>
                @if($company->user_id == Session::get('user_id'))
                    <a class="btn btn-xs btn-edit text-uppercase ct-js-btnEdit--Agents" data-toggle="modal" data-target="#image-upload-modal">edit</a>
                @endif
                <ul class="ct-panel--socials list-inline list-unstyled">
                    <li><a href="{{ $company->facebook }}" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-facebook"></i></div></a></li>
                    <li><a href="{{ $company->twitter }}" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-twitter"></i></div></a></li>
                    <li><a href="{{ $company->linkedin }}" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-linkedin"></i></div></a></li>
                </ul>
            </div>
            <div class="ct-personContent">
                <div class="ct-personName ct-u-paddingBoth10 text-uppercase">
                    <h2 class="one-line-elipsis" title="{{ $company->name }}" style="color:black;line-height: 25px;">{{ $company->name }}@if($company->user_id==Session::get('user_id'))<a class="btn btn-xs btn-edit text-uppercase ct-js-btnEdit--Agents" data-toggle="modal" data-target="#company-edit-modal">edit</a>@endif</h2>
                    <a href="" style="color:black">{{ $company->tagline }}</a>
                </div>
                <div class="ct-personDescription pull-right ct-u-paddingBoth20">
                    <ul class="list-unstyled ct-contactPerson pull-right">
                        <li>
                            <i class="fa fa-phone"></i>
                            <span style="color:black">{{ $company->phone }}</span>
                        </li>
                        <li>
                            <span style="color:black">CIN: {{ $company->company_cin }}</span>
                        </li>
                        <li style="color:black">
                            <i class="fa fa-envelope-o"></i>
                            {{ $company->email }}
                        </li>
                        <li style="color:black">
                             {{ $company->evaluation }}</a>
                        </li>
                    </ul>
                </div>
                <div class="ct-buttonPanel text-capitalize ct-u-paddingBoth20 pull-right">
                    <!-- <a href="{{ $company->website }}" class="btn btn-default btn-transparent--border ct-u-text--white btn-hoverWhite">Visit Our Website</a> -->
                    @if($interested['current'])
                    <button class="btn btn-default btn-transparent--border ct-u-text--white btn-hoverWhite vtalk-interested-button" data-entity-id="{{ $company->id }}" data-entity-type="{{ $interested['entity_type'] }}" data-interested="1" style="color:black"><i class="fa fa-star"></i> Not Interested</button>
                    @else
                    <button class="btn btn-default btn-transparent--border ct-u-text--white btn-hoverWhite vtalk-interested-button" data-entity-id="{{ $company->id }}" data-entity-type="{{ $interested['entity_type'] }}" data-interested="0" style="color:black"><i class="fa fa-star-o"></i> Interested</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>
<section class="ct-u-paddingTop60 ct-u-paddingBottom30 text-center">
    <div class="container">
        @if($errors->any())
            @foreach($errors->all() as $error)
            <div class="errorMessage alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ $error }}
            </div>
            @endforeach
        @endif
        <div class="ct-heading ct-u-marginBottom60">
            <h3 class="text-uppercase">Our Team</h3>
        @if($currentUser->role!=1 || ($currentUser->role==1 && ($company->user_id==Session::get('user_id'))))
            @if($company->user_id==Session::get('user_id'))
                <a class="btn btn-xs btn-edit text-uppercase ct-js-btnEdit--Agents" data-toggle="modal" data-target="#team-edit-modal">edit</a>
            @endif
        </div>
        @else
        <div class="row" style="background-color:lightpink">
            
            <span>Confidential</span>
            
        </div>
        @endif
        <div class="row">
            <div class="col-sm-9">
                @if($currentUser->role!=1 || ($currentUser->role==1 && ($company->user_id==Session::get('user_id'))))
                    @foreach($leadership as $leader)
                    <div class="col-sm-6 col-md-4">
                        <div class="ct-personBox text-left">
                            <div class="ct-personContent">
                                <div class="ct-personName ct-u-paddingBottom10 ct-u-marginBottom20">
                                    <h5 class="ct-fw-600">{{ $leader->name }}</h5>
                                    <span>{{ $leader->designation }}</span>
                                </div>
                                <div class="ct-personDescription">
                                    <ul class="list-unstyled ct-contactPerson">
                                        <li>
                                            <i class="fa fa-phone"></i>
                                            <span>{{ $leader->phone }}</span>
                                        </li>
                                        <li>
                                            <a href=""><i class="fa fa-envelope-o"></i> {{ $leader->email }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif

                <div class="col-sm-12" style="text-align: left;">
                    <div class="ct-heading ct-u-marginTop20 ct-u-marginBottom20">
                        <h3 class="text-uppercase">Company Intrested In</h3>
                        @if($company->user_id==Session::get('user_id'))
                            <a class="btn btn-xs btn-edit text-uppercase ct-js-btnEdit--Details" data-toggle="modal" data-target="#tags-edit-modal">edit</a>
                        @endif
                    </div>
                    <div class="ct-u-displayTableVertical ct-productDetails--type2 ct-u-marginBottom20">
                        <?php $len = count($tags) / 3; ?>
                        <div class="ct-u-displayTableCell">
                            <ul class="list-unstyled">
                                @for($x = 0; $x < $len; $x++)
                                <li>
                                    @if(in_array($tags[$x]->id, $companyTags))
                                    <i class="fa fa-check-circle"></i>
                                    @else
                                    <i class="fa fa-check-circle negative"></i>
                                    @endif
                                    <span class="text-capitalize">{{ $tags[$x]->tags }}</span>
                                </li>
                                @endfor
                            </ul>
                        </div>
                        <div class="ct-u-displayTableCell">
                            <ul class="list-unstyled">
                                @for($x = $len; $x < $len*2; $x++)
                                <li>
                                    @if(in_array($tags[$x]->id, $companyTags))
                                    <i class="fa fa-check-circle"></i>
                                    @else
                                    <i class="fa fa-check-circle negative"></i>
                                    @endif
                                    <span class="text-capitalize">{{ $tags[$x]->tags }}</span>
                                </li>
                                @endfor
                            </ul>
                        </div>
                        <div class="ct-u-displayTableCell">
                            <ul class="list-unstyled">
                                @for($x = $len*2; $x < $len*3; $x++)
                                <li>
                                    @if(in_array($tags[$x]->id, $companyTags))
                                    <i class="fa fa-check-circle"></i>
                                    @else
                                    <i class="fa fa-check-circle negative"></i>
                                    @endif
                                    <span class="text-capitalize">{{ $tags[$x]->tags }}</span>
                                </li>
                                @endfor
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <!-- user Details -->
                <div class="widget">
                    <div class="widget-inner">
                        <br>
                        <h4 class="text-uppercase" style="margin-top: -22px;">Contact Person
                            @if(($currentUser->role!=1 || ($currentUser->role==1 && ($company->user_id==Session::get('user_id'))))&&($company->user_id==Session::get('user_id')))
                            <a class="btn btn-xs btn-edit text-uppercase ct-js-btnEdit--Agents" href="{{ url('/user_profile') }}">edit</a>
                            @endif
                        </h4>
                      @if($currentUser->role!=1 || ($currentUser->role==1 && ($company->user_id==Session::get('user_id'))))
                        <div class="ct-personBox text-left">
                            <div class="ct-imagePerson">
                                <img src="{{ $user->profile_picture }}" alt="">
                                <ul class="ct-panel--socials list-inline list-unstyled">
                                    <li><a href="{{ $user->facebook }}" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-facebook"></i></div></a></li>
                                    <li><a href="{{ $user->twitter }}" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-twitter"></i></div></a></li>
                                    <li><a href="{{ $user->linkedin }}" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-linkedin"></i></div></a></li>
                                    <li><a href="{{ $user->instagram }}" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-instagram"></i></div></a></li>
                                </ul>
                            </div>
                            <div class="ct-personContent">
                                <div class="ct-personName ct-u-paddingBottom10 ct-u-marginBottom20">
                                    <h5 class="ct-fw-600">{{ $user->name }}</h5>
                                    <span>{{ $user->designation }}</span>
                                </div>
                                <div class="ct-personDescription  ct-u-paddingBottom10 ct-u-marginBottom20">
                                    <ul class="list-unstyled ct-contactPerson">
                                        <li>
                                            <i class="fa fa-phone"></i>
                                            <span>{{ $user->phone }}</span>
                                        </li>
                                        <li>
                                            <i class="fa fa-mobile"></i>
                                            <span>{{ $user->mobile }}</span>
                                        </li>
                                        <li>
                                            <i class="fa fa-skype"></i> <a href="">{{ $user->skype }}</a>
                                        </li>
                                    </ul>
                                </div>
                               <!--  <div class="ct-personContact">
                                   <div class="successMessage alert alert-success" style="display: none">
                                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                       Thank You!
                                   </div>
                                   <div class="errorMessage alert alert-danger" style="display: none">
                                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                       Ups! An error occured. Please try again later.
                                   </div>
                                   <form role="form" action="assets/form/send.php" method="post" class="contactForm validateIt" data-email-subject="Contact Form" data-show-errors="true">
                                       <div class="form-group">
                                           <div class="ct-form--item">
                                               <input id="contact_name" type="text" required class="form-control input-lg ct-u-marginBottom10" name="field[]" placeholder="Name">
                                               <input id="contact_email" type="email" required class="form-control input-lg ct-u-marginBottom10" name="field[]" placeholder="Email">
                                               <input id="contact_phone" type="tel" required class="form-control input-lg ct-u-marginBottom10" name="field[]" placeholder="Phone">
                                               <textarea id="contact_message" placeholder="Message" class="form-control input-lg" rows="4" name="field[]" required=""></textarea>
                                           </div>
                                           <button type="submit" class="btn btn-warning text-capitalize center-block">Send Message</button>
                                       </div>
                                   </form>
                               </div> -->
                            </div>
                        </div>
                        @else
                            <div class="row" style="background-color:lightpink">
                                <span>Confidential</span>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="ct-heading ct-u-marginBottom20">
                    <h3 class="text-uppercase">Turnovers</h3>
                    @if($company->user_id==Session::get('user_id'))
                        <a class="btn btn-xs btn-edit ct-js-btnEdit--Main text-uppercase" data-toggle="modal" data-target="#turnover-edit-modal">edit</a>
                    @endif
                </div>
                <div class="ct-u-displayTableVertical ct-productDetails">
                    @foreach($turnover as $turn)
                    <div class="ct-u-displayTableRow">
                        <div class="ct-u-displayTableCell">
                            <span class="ct-fw-600">{{ $turn->year }}</span>
                        </div>
                        <div class="ct-u-displayTableCell text-right">
                            <span class="ct-price">$ {{ $turn->turnover }}</span>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            </div>
        </div>
    </div>
</section>
<!--
    
    Modal
    <div id="change-logo-modal" class="modal fade" role="dialog">
      <div class="modal-dialog ">
    
        Modal content
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
          </div>
          <div class="modal-body">
            <p>Some text in the modal.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
    
      </div>
    </div> -->
@stop

@section('modals')
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="image-upload-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                    Upload Logo
                </h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('/company/'.$company->id.'/saveProfilePicture') }}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <span class="btn btn-default btn-block">
                                <input type="file" id="image" name="image">
                            </span>
                        </div>
                        <div class="col-md-12"><br></div>
                        <div class="col-md-6 form-group">
                            <input type="submit" class="btn btn-sm btn-info" value="Save">
                        </div>
                        <div class="col-md-6 form-group">
                            <button class="btn btn-default btn-sm btn-block" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="cover-upload-modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                    Upload Cover Image
                </h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('/company/'.$company->id.'/saveCoverImage') }}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <span class="btn btn-default btn-block">
                                <input type="file" id="cover-image" name="image">
                            </span>
                        </div>
                        <div class="col-md-12">
                            <img src="" id="preview">
                            <input type="hidden" name="x" value="0" id="x">
                            <input type="hidden" name="y" value="0" id="y">
                            <input type="hidden" name="w" value="0" id="w">
                            <input type="hidden" name="h" value="0" id="h">
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="submit" class="btn btn-sm btn-info" value="Save">
                        </div>
                        <div class="col-md-6 form-group">
                            <button class="btn btn-default btn-sm btn-block" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="company-edit-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                    Edit Company
                </h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('/company/'.$company->id.'/editCompany') }}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="helptext">Company Name</label>
                            <input type="text" class="form-control ct-input--border ct-u-text--dark" id="name" value="{{$company->name}}" name="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="helptext">Company Email</label>
                            <input type="email" class="form-control ct-input--border ct-u-text--dark" id="email" value="{{$company->email}}" name="email">
                        </div>
                        <div class="col-md-12"><br></div>
                        <div class="form-group col-md-6">
                            <label for="helptext">Company CIN</label>
                            <input type="text" disabled class="form-control ct-input--border ct-u-text--dark" value="{{$company->company_cin}}">
                            <input type="hidden" name="cin">
                        </div>
                        <div class="form-group col-md-6">
                            <p>To edit CIN please write a mail to <a href="mailto:support@vtalkbusiness">support@vtalkbusiness.com</a></p>
                        </div>
                        <div class="col-md-12"><br></div>
                        <div class="form-group col-md-6">
                            <label for="helptext">Company Phone No.</label>
                            <input type="text" class="form-control ct-input--border ct-u-text--dark" id="phone" value="{{$company->phone}}" name="phone">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="helptext">Company Evaluation( SGD )</label>
                            <input type="text" class="form-control ct-input--border ct-u-text--dark" value="{{$company->evaluation}}" name="evaluation">
                        </div>
                        <div class="col-md-12"><br></div>
                        <div class="col-md-4 form-group">
                            <label for="helptext">Facebook URL</label>
                            <input type="text" class="form-control ct-input--border ct-u-text--dark" id="facebook" name="facebook" value="{{$company->facebook}}">
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="helptext">LinkedIn URL</label>
                            <input type="text" class="form-control ct-input--border ct-u-text--dark" id="linkedin" name="linkedin" value="{{$company->linkedin}}">
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="helptext">Twitter URL</label>
                            <input type="text" class="form-control ct-input--border ct-u-text--dark" id="twitter" name="twitter" value="{{$company->twitter}}">
                        </div>
                        <div class="col-md-12"><br></div>
                        <div class="col-md-6 form-group">
                            <input type="submit" class="btn btn-sm btn-info" value="Save">
                        </div>
                        <div class="col-md-6 form-group">
                            <button class="btn btn-default btn-sm btn-block" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="team-edit-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                    Edit Team
                </h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('/company/'.$company->id.'/editTeam') }}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        @foreach($leadership as $leader)
                            <div class="col-sm-6 col-md-6 team-member">
                                <div class="ct-personBox text-left">
                                    <div class="ct-personContent">
                                        <div class="ct-personName ct-u-paddingBottom10 ct-u-marginBottom20">
                                            <input type="text" class="form-control ct-input--border ct-u-text--dark" id="name" value="{{$leader->name}}" name="names[]" placeholder="Name">
                                            <input type="text" class="form-control ct-input--border ct-u-text--dark" id="designation" value="{{$leader->designation}}" name="designations[]" placeholder="Designation">
                                        </div>
                                        <div class="ct-personDescription">
                                            <ul class="list-unstyled ct-contactPerson">
                                                <li>
                                                    <i class="fa fa-phone"></i>
                                                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="phone" value="{{$leader->phone}}" name="phones[]" placeholder="Phone">
                                                </li>
                                                <li>
                                                    <i class="fa fa-envelope-o"></i>
                                                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="email" value="{{$leader->email}}" name="emails[]" placeholder="Email">
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div id="addTeamMemberDiv"></div>
                        <div class="col-md-12"><br></div>
                        <div class="col-md-4 col-md-offset-4">
                            <input type="button" class="btn btn-sm btn-success" id="addTeamMemberButton" value="Add Member">
                        </div>
                        <div class="col-md-12"><br></div>
                        <div class="col-md-6 form-group">
                            <input type="submit" class="btn btn-sm btn-info" value="Save">
                        </div>
                        <div class="col-md-6 form-group">
                            <button class="btn btn-default btn-sm btn-block" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="tags-edit-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                    Edit Tags
                </h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('/company/'.$company->id.'/editTags') }}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-6 form-group col-md-offset-2">
                            <label>Select Tags</label>
                            <select class="" name="tags[]" id="tags" multiple placeholder="Select Tags">
                                @foreach($tags as $tag)
                                    <option value="{{ $tag->id }}">{{ $tag->tags }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12"><br></div>
                        <div class="col-md-6 form-group">
                            <input type="submit" class="btn btn-sm btn-info" value="Save">
                        </div>
                        <div class="col-md-6 form-group">
                            <button class="btn btn-default btn-sm btn-block" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="turnover-edit-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                    Edit Turnover
                </h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('/company/'.$company->id.'/editTurnover') }}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Company Turnovers</h4>
                        </div>
                        @if(count($turnover)>0)
                        @foreach($turnover as $turn)
                        <div class="form-group col-md-4">
                            <label for="helptext">Turnover {{ $turn->year }}</label>
                            <input type="hidden" name="year[]" value="{{ $turn->year }}">
                            <input type="number" class="form-control custom-control" name="turnover[]" value="{{$turn->turnover}}">
                        </div>
                        @endforeach
                        @else
                            @for($year = (int)Carbon\Carbon::now()->format('Y'); $year >= Carbon\Carbon::now()->subYears(2)->format('Y'); $year--)
                                <div class="form-group col-md-4">
                                    <label for="helptext">Turnover {{ $year }}</label>
                                    <input type="hidden" name="year[]" value="{{ $year }}">
                                    <input type="number" class="form-control custom-control" name="turnover[]">
                                </div>
                            @endfor
                        @endif
                        <div class="col-md-12"><br></div>
                            <div class="col-md-6 form-group">
                            <input type="submit" class="btn btn-sm btn-info" value="Save">
                        </div>
                        <div class="col-md-6 form-group">
                            <button class="btn btn-default btn-sm btn-block" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop


@section('scripts')
<script type="text/javascript" src="{{ asset('/js/jcrop.js') }}"></script>
<script type="text/javascript">
    var jcrop_api;
    function updateCoords(details) {
        $('#x').val(details.x);
        $('#y').val(details.y);
        $('#w').val(details.w);
        $('#h').val(details.h);
    }
    $(function(){
        $("#addTeamMemberButton").on('click',function(){
            var a = $('.team-member');
            if(a.length >= 2) {
                $(this).addClass('disabled');
                return;
            }
            var html=
                "<div class='col-sm-6 col-md-6 team-member'>"+
                    "<div class='ct-personBox text-left'>"+
                        "<div class='ct-personContent'>"+
                            "<div class='ct-personName ct-u-paddingBottom10 ct-u-marginBottom20'>"+
                                "<input type='text' class='form-control ct-input--border ct-u-text--dark' name='names[]' placeholder='Name'>"+
                                "<input type='text' class='form-control ct-input--border ct-u-text--dark' name='designations[]'' placeholder='Designation'>"+
                            "</div>"+
                        "<div class='ct-personDescription'>"+
                            "<ul class='list-unstyled ct-contactPerson'>"+
                                "<li>"+
                                    "<i class='fa fa-phone'></i>"+
                                    "<input type='text' class='form-control ct-input--border ct-u-text--dark' name='phones[]' placeholder='Phone'>"+
                                "</li>"+
                                "<li>"+
                                    "<i class='fa fa-envelope-o'></i>"+
                                    "<input type='text' class='form-control ct-input--border ct-u-text--dark'  name='emails[]' placeholder='Email'>"+
                                "</li>"+
                            "</ul>"+
                        "</div>"+
                    "</div>"+
                "</div>";
            $('#addTeamMemberDiv').append(html);
        });
        $('select').val("{{ implode(',', $companyTags) }}").select2();

        $('#cover-image').on('change', function() {
            var _URL = window.URL || window.webkitURL;
            var img = new Image();
            var file = this;
            img.onload = function() {
                if(!(img.width <= 510 || img.height <= 189)) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview').attr('src', e.target.result);
                        $('#preview').Jcrop({
                            onSelect: updateCoords,
                            bgOpacity:   .4,
                            boxWidth: 850,
                            minSize: [510, 189],
                            setSelect:   [0, 0, 510, 189],
                            aspectRatio: 850/315
                        }, function () {
                            jcrop_api = this;
                        });
                    }
                    reader.readAsDataURL(file.files[0]);
                }
                else {
                    alert('Image must be at least 510x189');
                }
            }
            img.src = _URL.createObjectURL(this.files[0]);
        });

    });
</script>
@stop