@extends('master')

@section('title')
<title>Create Company - Step 1</title>
@stop

@section('breadcrumb')
<div class="ct-site--map">
    <div class="container">
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <a href="{{ url('/company/add/1') }}">Create Company</a>
        <a href="{{ url('/company/add/1') }}">Step 1</a>
    </div>
</div>
@stop

@extends('company.header')


@section('content')
<section class="ct-u-paddingTop50 ct-u-paddingBottom100">
    <div class="container">
        <div class="ct-headerText--normal text-uppercase ct-u-marginBottom40">
            <h2>Step 1<br>
                <span class="ct-text--highlightGray">Company Details</span>
            </h2>
        </div>
        <div class="row">
            @if($errors->any())
                @foreach($errors->all() as $error)
                <div class="errorMessage alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $error }}
                </div>
                @endforeach
            @endif

            <form method="post" name="companyRegistration" id="frm" action="{{ url('/company/add/1') }}" ><!-- action="{{ url('/company/create') }}">-->
                <div class="form-group col-md-3">
                    <label for="helptext">Company Name</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="name" placeholder="Company Name" name="name">
                </div>
                <div class="form-group col-md-3">
                    <label for="helptext">Company Email</label>
                    <input type="email" class="form-control ct-input--border ct-u-text--dark" id="email" placeholder="Company Email" name="email">
                </div>
                <div class="form-group col-md-3">
                    <label for="helptext">Company Website</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="tagline" placeholder="Website" name="website">
                </div>
                <div class="form-group col-md-3">
                    <label for="helptext">Company Phone No.</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="phone" placeholder="Company Phone No" name="phone">
                </div>
                <div class="form-group col-md-3">
                    <label for="helptext">Registration Type</label>
                    <select class="form-control ct-input--border ct-u-text--dark" name="registration_type" id="registrationType" placeholder="Select Registration Type">
                        @foreach($regType as $reg)
                            <option value="{{$reg->id}}">{{$reg->registration_type}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="helptext">Registration Number</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="regNo" placeholder="Registration No" name="company_cin">
                </div>
                <div class="form-group col-md-3">
                    <label for="helptext">Company Value</label>
                    <input type="number" class="form-control ct-input--border ct-u-text--dark" id="evaluation" min="100000" max="1000000" placeholder="Company Value" name="evaluation">
                </div>
                <div class="form-group col-md-3">
                    <label for="helptext">Currency</label>
                    <select class="form-control ct-input--border ct-u-text--dark" name="currency" id="registrationType" placeholder="Select Registration Type">
                        @foreach($currency as $currency)
                            @if($currency->currency == 'INR')
                            <option value="{{$currency->id}}" selected>{{$currency->currency}}</option>
                            @else
                            <option value="{{$currency->id}}">{{$currency->currency}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="helptext">Tagline</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="tagline" placeholder="Tagline" name="tagline">
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                <div class="form-group col-md-6 button">
                    <label>&nbsp;</label>
                    <button type="submit" id="submit" class="btn btn-info btn-block">Save</button>
                </div>
            </form>
        </div>
    </div>
</section>
@stop

@section('scripts')
<script type="text/javascript">
    $(function() {
        $('#phone').on("blur change keyup paste keypress keydown",function(e){
                if(!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode == 8) || (e.keyCode == 9) || (e.keyCode>=35 && e.keyCode <= 37) || (e.keyCode == 39) || (e.keyCode == 46)))
                {   
                    event.preventDefault();
                    return false;                       
                }
            });
    });
</script>
@stop