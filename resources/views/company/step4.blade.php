@extends('master')

@section('title')
<title>Create Company - Step 4</title>
@stop

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jcrop.css') }}">
@stop

@section('breadcrumb')
<div class="ct-site--map">
    <div class="container">
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <a href="{{ url('/company/add') }}">Create Company</a>
        <a href="{{ url('/company/add/4') }}">Step 4</a>
    </div>
</div>
@stop

@extends('company.header')


@section('content')
<section class="ct-u-paddingTop50 ct-u-paddingBottom100">
    <div class="container">
        <div class="ct-headerText--normal text-uppercase ct-u-marginBottom40">
            <h2>Step 4<br>
                <span class="ct-text--highlightGray">Additional Details</span>
            </h2>
        </div>
        <div class="row">
            @if(count($errors))
                @foreach($errors as $error)
                <div class="errorMessage alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    @foreach($error as $e)
                    {{ $e }}
                    @endforeach
                </div>
                @endforeach
            @endif

            <form method="post" name="companyRegistration" id="frm" >
                <input type="hidden" name="company_id" value="{{ Session::get('company_id') }}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="col-md-3 form-group">
                    <label for="employee">No of Employees</label>
                    <select type="text" class="form-control custom-control" id="employee" name="employee">
                        <option value="1-10">1-10</option>
                        <option value="11-25">11-25</option>
                        <option value="26-50">26-50</option>
                        <option value="51-100">51-100</option>
                        <option value="101-500">101-500</option>
                        <option value=">500">>500</option>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label for="facebook">Facebook URL</label>
                    <input type="text" class="form-control custom-control" id="facebook" name="facebook">
                </div>
                <div class="col-md-3 form-group">
                    <label for="facebook">LinkedIn URL</label>
                    <input type="text" class="form-control custom-control" id="linkedin" name="linkedin">
                </div>
                <div class="col-md-3 form-group">
                    <label for="facebook">Twitter URL</label>
                    <input type="text" class="form-control custom-control" id="twitter" name="twitter">
                </div>
                <div class="col-md-12 form-group">
                    <label for="description">Description</label>
                    <textarea id="description" cols="30" rows="5" class="form-control custom-control" name="description"></textarea>
                </div>
                <div class="form-group col-md-12 button">
                    <input type="button" id="submit" class="btn btn-info btn-block" value="Save">
                </div>
            </form>
        </div>
    </div>
</section>
@stop

@section('modals')
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="opportunity-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                    Congratulations!!! You have done a great job!
                </h4>
            </div>
            <div class='modal-body' id="modalBody">
                <h4>Do you want to register an opportunity?</h4>
                <div class="row">
                    <div class="col-md-6"><a class="btn btn-primary btn-md" href="{{ url('/opportunity') }}" role="button">Register</a></div>
                    <div class="col-md-6"><a class="btn btn-primary btn-md" href="{{ url('/dashboard') }}" role="button">Cancel</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop


@section('scripts')
<script type="text/javascript">
    $(function() {
        $('.ct-steps--submission .ct-steps--item:eq(0)').addClass('ct-steps--past');
        $('.ct-steps--submission .ct-steps--item:eq(1)').addClass('ct-steps--past');
        $('.ct-steps--submission .ct-steps--item:eq(2)').addClass('ct-steps--progress');
        $('.ct-steps--submission .ct-steps--item:eq(3)').find('.ct-steps--counter').addClass('ct-steps--active');

        $('#submit').on('click',function(){
            var formData = JSON.parse(JSON.stringify(jQuery('#frm').serializeArray())) ;
            $.ajax({
                type:'POST',
                url: "{{ url('/company/add/4') }}",
                data:formData,
                success: function(data){
                    $('#opportunity-modal').modal('toggle');
                    $('#opportunity-modal').modal('show');
                    $(".modal").on("hidden.bs.modal", function(){
                        window.location = '{{ url("/dashboard") }}';
                    });
                },
                error:function(){
                }
            });
        });
    });
</script>
@stop