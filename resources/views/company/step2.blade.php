@extends('master')

@section('title')
<title>Create Company - Step 2</title>
@stop

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jcrop.css') }}">
@stop

@section('breadcrumb')
<div class="ct-site--map">
	<div class="container">
		<a href="{{ url('/dashboard') }}">Dashboard</a>
		<a href="{{ url('/company/add') }}">Create Company</a>
		<a href="{{ url('/company/add/2') }}">Step 2</a>
	</div>
</div>
@stop

@extends('company.header')


@section('content')
<section class="ct-u-paddingTop50 ct-u-paddingBottom100">
	<div class="container">
		<div class="ct-headerText--normal text-uppercase ct-u-marginBottom40">
			<h2>Step 2<br>
				<span class="ct-text--highlightGray">Company Image and Tags</span>
			</h2>
		</div>
		<div class="row">
			@if(count($errors))
				@foreach($errors as $error)
				<div class="errorMessage alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					@foreach($error as $e)
					{{ $e }}
					@endforeach
				</div>
				@endforeach
			@endif

			<form method="post" name="companyRegistration" id="frm" action="{{ url('/company/add/2') }}" enctype="multipart/form-data">
				<input type="hidden" name="company_id" value="{{ Session::get('company_id') }}">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<div class="col-md-4 form-group">
					<label>Select Industry Categories</label>
					<select  class="select" name="category[]" id="category" multiple placeholder="Select Categories">
						@foreach($categories as $category)
							<option value="{{ $category->id }}">{{ $category->category }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-4 form-group">
					<label>Select Categories</label>
					<select class="select" name="tags[]" id="tags" multiple placeholder="Select Tags">
						@foreach($tags as $tag)
							<option value="{{ $tag->id }}">{{ $tag->tags }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-4 form-group">
					<label>Company Logo</label>
					<span class="btn btn-default">
						<input id="logo-img" type="file" name="logo">
					</span>
				</div>
				<div class="col-md-12 form-group">
					<label>Company Cover</label>
					<span class="btn btn-default form-control">
						<input id="cover-img" type="file" name="cover">
					</span>
				</div>
				<div class="col-md-12 form-group">
					<img src="" id="preview">
					<input type="hidden" name="x" value="0" id="x">
					<input type="hidden" name="y" value="0" id="y">
					<input type="hidden" name="w" value="0" id="w">
					<input type="hidden" name="h" value="0" id="h">
				</div>
				<div class="col-md-12 custom-panel">
					<div class="col-md-12"><h4>Company Address</h4></div>
					<div class="col-md-4 form-group">
						<label>Flat/Street</label>
						<input type="text" name="street[]" class="form-control street custom-control">
					</div>
					<div class="col-md-4 form-group">
						<label>City</label>
						<input type="text" name="city[]" class="form-control city custom-control">
					</div>
					<div class="col-md-4 form-group">
						<label>State</label>
						<input type="text" name="state[]" class="form-control state custom-control">
					</div>
					<div class="col-md-4 form-group">
						<label>Zip Code</label>
						<input type="text" name="pin[]" class="form-control pin custom-control">
					</div>
					<div class="col-md-4 form-group">
						<label>Country</label>
						<select class="form-control ct-input--border ct-u-text--dark" name="country[]" placeholder="Country">
						<?php
							$html = '';
							foreach($country as $country) {
								$html .= "<option value='{$country->id}'>{$country->country}</option>";
							}
						?>
						{!! $html !!}
						</select>
					</div>
					<div class="col-md-4 form-group">
						<label>&nbsp;</label>
						<button class="btn btn-info btn-block" id="add-address"><i class="fa fa-plus-circle"></i> Add More Address</button>
					</div>
				</div>
				<input type="hidden" id="more-address">
				<div class="col-md-12 custom-panel">
					<div class="col-md-12">
						<h4>Company Turnovers</h4>
					</div>
	        		@for($year = (int)Carbon\Carbon::now()->format('Y'); $year >= Carbon\Carbon::now()->subYears(2)->format('Y'); $year--)
					<div class="form-group col-md-4">
	        			<label for="helptext">Turnover {{ $year }}</label>
	        			<input type="hidden" name="year[]" value="{{ $year }}">
		        		<input type="number" class="form-control custom-control" name="turnover[]" min="0" step="1">
		    		</div>
		    		@endfor
	    		</div>

				
				<div class="form-group col-md-12 button">
					<button type="submit" id="submit" class="btn btn-info btn-block">Save</button>
				</div>
			</form>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/jcrop.js') }}"></script>
<script type="text/javascript">
	var jcrop_api;
	function updateCoords(details) {
		$('#x').val(details.x);
		$('#y').val(details.y);
		$('#w').val(details.w);
		$('#h').val(details.h);
	}
	$(function() {
		$('.ct-steps--submission .ct-steps--item:eq(0)').addClass('ct-steps--progress');
		$('.ct-steps--submission .ct-steps--item:eq(1)').find('.ct-steps--counter').addClass('ct-steps--active');

		$('.select').select2();

		$('#cover-img').on('change', function() {
			var _URL = window.URL || window.webkitURL;
			var img = new Image();
			var file = this;
			img.onload = function() {
				if(!(img.width <= 510 || img.height <= 189)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						jcrop_api.setImage(e.target.result);
						// $('#preview').attr('src', e.target.result);
					}
					reader.readAsDataURL(file.files[0]);
				}
				else {
					$('#cover-img').val('');
					bootbox.alert('Image must be at least 510x189');
				}
			}
			img.src = _URL.createObjectURL(this.files[0]);
		});

		$('#preview').Jcrop({
			onSelect: updateCoords,
			bgOpacity:   .4,
			boxWidth: 850,
			minSize: [510, 189],
			setSelect:   [0, 0, 510, 189],
			aspectRatio: 850/315
		}, function () {
			jcrop_api = this;
		});

		$('#add-address').on('click', function(e) {
			e.preventDefault();
			var html = '<div class="col-md-12 custom-panel">\
					<div class="col-md-12"><h4 class="">Company Address</h4></div>\
					<div class="col-md-4 form-group">\
						<label>Flat/Street</label>\
						<input type="text" name="street[]" class="form-control street custom-control">\
					</div>\
					<div class="col-md-4 form-group">\
						<label>City</label>\
						<input type="text" name="city[]" class="form-control city custom-control">\
					</div>\
					<div class="col-md-4 form-group">\
						<label>State</label>\
						<input type="text" name="state[]" class="form-control state custom-control">\
					</div>\
					<div class="col-md-4 form-group">\
						<label>Zip Code</label>\
						<input type="text" name="pin[]" class="form-control pin custom-control">\
					</div>\
					<div class="col-md-4 form-group">\
						<label>Country</label>'+"\
						<select class='form-control ct-input--border ct-u-text--dark' name='country[]' placeholder='Country'>\
						{!! $html !!}\
						</select>\
					</div></div>";
			$('#more-address').before(html);
		});
	});
</script>
@stop