@extends('master')

@section('title')
<title>Find Companies</title>
@stop

@section('breadcrumb')
<!-- BreadCrumbs -->
<div class="ct-site--map">
    <div class="container">
    </div>
</div>
<!-- BreadCrumb Ends -->
@stop

@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
    <div class="ct-mediaSection-inner">
        <div class="container">
			<div class="ct-heading--main text-center">
			    <h3 class="text-uppercase ct-u-text--white">Search Companies</h3>
			</div>
        </div>
    </div>
</header>
@stop

@section('content')
	<section class="ct-u-paddingBoth70 ct-js-section text-left">
    	<div class="container">
			<div class="col-md-12">
				@if($errors->any())
                	@foreach($errors->all() as $error)
                	<div class="errorMessage alert alert-danger">
                    	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    	{{ $error }}
                	</div>
                	@endforeach
                @endif
				<div class="row">
					
					<div class="col-md-9">
						<ul class="nav nav-tabs" role="tablist" id="myTabs">
							<li role="presentation" class="active">
								<a href="#companies-container" aria-controls="companies-container" role="tab" data-toggle="tab">Companies</a>
							</li>
							<li role="presentation">
								<a href="#opportunities-container" aria-controls="opportunities-container" role="tab" data-toggle="tab">Opportunities</a>
							</li>
						</ul>
						<div class="clearfix"></div>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="companies-container">
								<div class="col-md-12" style="text-align:center">
									<h4 class="my-heading">Search Results for Companies</h4>
								</div>
								<div class="col-md-12 result-container" id="relatedCompany">
									<!--Search Result-->
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="opportunities-container">
								<div class="col-md-12" style="text-align:center">
									<h4 class="my-heading">Search Results for Opportunities</h4>
								</div>
								<div class="col-md-12 result-container" id="relatedOpportunities">
									<!--Search Result-->
									
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="param-container widget">
		                    <div class="widget-inner">
		                        <form class="ct-formSearch--extended" role="form">
		                            <div class="ct-form--label--type3">
		                                <div class="ct-u-displayTableVertical">
		                                    <div class="ct-u-displayTableCell">
		                                        <div class="ct-input-group-btn">
		                                            <i class="fa fa-search"></i>
		                                        </div>
		                                    </div>
		                                    <div class="ct-u-displayTableCell text-center">
		                                        <span class="text-uppercase">Search for Business/Opportunity</span>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="form-group ">
		                                <div class="ct-form--item ct-u-marginBoth10">
		                                    <label>Name</label>
		                                    <input type="text" class="search form-control custom-control input-lg" id="search-box" placeholder="Search...">
		                                </div>
		                                <div class="ct-form--item ct-u-marginBottom10">
		                                    <label>Industry Categories</label>
											<select class="" id="category" multiple>
												@foreach($categories as $category)
												<option value="{{ $category->id }}">{{ $category->category }}</option>
												@endforeach
											</select>
		                                </div>
		                                <div class="ct-form--item ct-u-marginBottom10">
		                                    <label>Tags</label>
											<select class="" id="tags" multiple>
												@foreach($tags as $tag)
												<option value="{{ $tag->id }}">{{ $tag->tags }}</option>
												@endforeach
											</select>
		                                </div>
		                                <div class="ct-form--item ct-u-marginBottom10">
		                                    <label>Country</label>
											<select class="" id="country" multiple>
												@foreach($countries as $country)
												<option value="{{ $country->countryId }}">{{ $country->country }}</option>
												@endforeach
											</select>
		                                </div>
		                                <div class="ct-form--item ct-u-marginBoth40">
		                                    <input type="text" class="ct-js-sliderTicks" value="" data-slider-handle="square" data-slider-min="100000" data-slider-max="1000000" data-slider-step="50000" data-slider-value="[100000,1000000]" id="slider"/>
		                                    <label class="text-center center-block">Price (INR)</label>
		                                    <input type="hidden" name="minPrice" id="minPrice" value="100000">
		                                    <input type="hidden" name="maxPrice" id="maxPrice" value="1000000">
		                                </div>
		                                <button class="btn btn-warning ct-u-marginTop10 btn-block" id="search-button">Search Now</button>
		                            </div>
		                        </form>
		                    </div>
						</div>
					</div>
				</div>
			</div>
    	</div>
	</section>
@stop


@section('scripts')
<script type="text/javascript">
	@if(Session::has('user_role'))
	var userRole = {{ Session::get('user_role') }};
	@else
	var userRole = 0;
	@endif
	function initPage() {
		//console.log(window.location.href);
		var name 		= 	decodeURIComponent(getUrlParameter('name'));
		var category 	= 	decodeURIComponent(getUrlParameter('category')).split(',');
		var country  	= 	decodeURIComponent(getUrlParameter('country')).split(',');
		var tags 		= 	decodeURIComponent(getUrlParameter('tags')).split(',');
		var minPrice 	= 	decodeURIComponent(getUrlParameter('minPrice'));
		var maxPrice	= 	decodeURIComponent(getUrlParameter('maxPrice'));

		$('#search-box').val(name);
		$('#category').val(category).select2();
		$('#tags').val(tags).select2();
		$('#country').val(country).select2();
		$('#minPrice').val(minPrice);
		$('#maxPrice').val(maxPrice);
		$('#slider').attr('data-slider-value', '[' + minPrice + ',' + maxPrice + ']').slider();

		search();
	}

	function search() {
		var name = $('#search-box').val();
		var category = $('#category').val();
		if(category) {
			category = category.toString();
		}else {
			category = '';
		}
		var country = $('#country').val();
		if(country) {
			country = country.toString()
		}else {
			country = '';
		}
		var tags = $('#tags').val();
		if(tags) {
			tags = tags.toString();
		}else {
			tags = '';
		}
		var minPrice = $('#minPrice').val();
		var maxPrice = $('#maxPrice').val();

		if(!minPrice)
		{
			minPrice = '100000';
			$('#minPrice').val('100000');
		}
		if(!maxPrice)
		{
			maxPrice = '1000000';
			$('#maxPrice').val('1000000');
		}

		window.history.pushState({"html": '',"pageTitle": 'Searching'},"Search State", 'search?name=' + encodeURIComponent(name) + '&category=' + encodeURIComponent(category) + '&country=' + encodeURIComponent(country) + '&tags=' + encodeURIComponent(tags) + '&minPrice=' + encodeURIComponent(minPrice) + '&maxPrice=' + encodeURIComponent(maxPrice));

		var data 		= 	{'_token':'{{csrf_token()}}'};
		data.name 		=  	name;
		data.category 	= 	category;
		data.country 	= 	country;
		data.tags 		= 	tags;
		data.minPrice	= 	minPrice;
		data.maxPrice 	=	maxPrice;
		data 			= 	JSON.stringify(data);
		console.log(data);

		$.ajax({
				contentType : 'application/json',
				type 		: 	'POST',
				url 		: 	"{{action('CompanyController@findCompany')}}",
				data 		: 	data
		}).done(function(data) {
			var html = '';
			//console.log(data.related);
			if(data.related==true)
			{
				html=//"<div class='row'><h3>No Records Found!</h3></div>"+
					//	"<hr>"+
						"<div class='row'><h3>Related Companies</h3></div>"+
						"<div class='row'><br></div>";	
			}
			$.each(data.companies, function(i, company) {
				html += '<div class="col-sm-6 col-md-6 col-lg-4">'+
							'<div class="ct-itemProducts ct-u-marginBottom30 ct-hover">';
						if(userRole) {
							html += "<a id='goComapny' href='{{ url ('/company') }}/" + company.id + "'>";
						}else {
							html += "<a id='goComapny' href='#user-modal' data-toggle='modal' data-target='#user-modal'>";
						}
						html += '<div class="ct-main-content">'+
    							'<div class="ct-imageBox">'+
        							"<div class='logo-middle-container'><img src='" + company.logo + "' style='height: 200px;'></div><i class='fa fa-eye'></i>"+
                                '</div>'+
            					'<div class="ct-main-text">'+
        							'<div class="ct-product--tilte one-line-elipsis" title="' + company.name + '">' + company.name + '</div>'+
        							'<div class="ct-product--price">'+
            							'<p class="one-line-elipsis">' + company.countries + '</p>'+
        							'</div>'+
        							'<div class="ct-product--description one-line-elipsis">' + company.tagline + '</div>' +
        						'</div>'+
							'</div>'+
							'<div class="ct-product--meta">'+
    							'<div class="ct-text">'+
        							'<span><i class="fa fa-user"></i> ' + company.users_name + '</span>'+
    							'</div>'+
							'</div>'+
						'</a>'+
					'</div>'+
				'</div>';
				});	
			$('#relatedCompany').empty().append(html);

			html = '';
			if(data.relatedOpportunities == true) {
				html += //"<div class='row'><h3>No Records Found!</h3></div>"+
						//"<hr>"+
						"<div class='row'><h3>Related Opportunities</h3></div>"+
						"<div class='row'><br></div>";	
			}
			html += '<table class="table"><tr>\
					<th style="width: 10%;">Picture</th>\
					<th style="width: 48%%;">Headline</th>\
					<th style="width: 27%;">Companies</th>\
					<th style="width: 15%;">Actions</th>\
					</tr>';
			$.each(data.opportunities, function(i, opportunity) {
				html += '<tr><td><img src="' + opportunity.logo + '" style="width: 100%"></td>\
						<td>' + opportunity.headline + '</td><td>\
						' + opportunity.companies + '</td><td>\
						<button class="btn btn-sm btn-info details" data-id="' + opportunity.id + '" >View Details</button></td><tr>';
			});
			html += '</table>';

			$('#relatedOpportunities').empty().append(html);
			initOpportunities();
		});
	}

	function initOpportunities() {
		if(!userRole) {
			$('.details').on('click',function(e) {
				e.preventDefault();
				$('#user-modal').modal('show');
			});
		}else {
			$('.details').on('click',function(e){
				e.preventDefault();
		    	var data = {'_token':'{{csrf_token()}}'};
		    	$.ajax({
					type:'POST',
					url: "{{ url('/opportunity-details')}}/" + $(this).data('id'),
					data : 	data,
		            success: function(data){
		            	var html ="";
		            	html=				
		            				"<div class='row'>"+
		            					"<div class='col-md-4'>"+
		            						"<h4>Headline</h4>"+
	            						"</div>"+
	            						"<div class='col-md-8'>"+
		            						"<h4>"+data.opportunity.headline+"</h4>"+
	            						"</div>"+
									"</div>"+

									"<div class='row'>"+
										"<div class='col-md-4'>"+
		            						"<h4>Description</h4>"+
	            						"</div>"+
	            						"<div class='col-md-8'>"+
		            						"<h4>"+data.opportunity.description+"</h4>"+
	            						"</div>"+
									"</div>"+
									
									"<div class='row'>"+
										"<div class='col-md-4'>"+
		            						"<h4>Capital</h4>"+
	            						"</div>"+
	            						"<div class='col-md-8'>"+
		            						"<h4>"+data.opportunity.capital+"</h4>"+
	            						"</div>"+
									"</div>"+
									
									"<div class='row'>"+
										"<div class='col-md-4'>"+
		            						"<h4>Revenue</h4>"+
	            						"</div>"+
	            						"<div class='col-md-8'>"+
		            						"<h4>"+data.opportunity.revenue+"</h4>"+
	            						"</div>"+
									"</div>"+

									"<div class='row'>"+
										"<div class='col-md-4'>"+
		            						"<h4>Location</h4>"+
	            						"</div>"+
	            						"<div class='col-md-8'>"+
	            							"<ul>";
		            							for(var i=0;i<data.country.length;i++)
		            							{
		            								html+="<li><h4>"+data.country[i].country+"</h4></li>";
		            							}
	            							html+="</ul>"+
	            						"</div>"+
									"</div>"+
									"<div class='row'>"+
										"<div class='col-md-4'>"+
		            						"<h4>Company</h4>"+
	            						"</div>"+
	            						"<div class='col-md-8'>"+
	            							"<ul>";
		            							for(var i=0;i<data.company.length;i++)
		            							{
		            								html+="<li><a href='{{ url('/comapny') }}/" + data.company[i].company_id + "'><h4>"+data.company[i].company_name+"</h4></a></li>";
		            							}
	            							html+="</ul>"+
	            						"</div>"+
									"</div>"+
									"<div class='row'>"+
										"<div class='col-md-4'>"+
		            						"<h4>Tags</h4>"+
	            						"</div>"+
	            						"<div class='col-md-8'>"+
	            							"<ul>";
		            							for(var i=0;i<data.tags.length;i++)
		            							{
		            								html+="<li><h4>"+data.tags[i].tags+"</h4></li>";
		            							}
	            							html+="</ul>"+
	            						"</div>"+
									"</div>"+
									"<div class='row'>"+
										"<div class='col-md-8 col-md-offset-4'>";
											if(parseInt(data.interested.current)) {
												html += "<button class='btn btn-info ct-u-text--blue vtalk-interested-button' data-entity-id='" + data.opportunity.id + "' data-interested='" + data.interested.current + "' data-entity-type='" + data.interested.entity_type + "'><i class='fa fa-star'></i> Not Interested</button>";
											}else {
												html += "<button class='btn btn-info ct-u-text--blue vtalk-interested-button' data-entity-id='" + data.opportunity.id + "' data-interested='" + data.interested.current + "' data-entity-type='" + data.interested.entity_type + "'><i class='fa fa-star-o'></i> Interested</button>";

											}
										"</div>"+
									"</div>";


		            	$('#modalBody').append(html);
		            	$('#opportunity-description-modal').modal('toggle');
						$('#opportunity-description-modal').modal('show');
						initInterested();
		            },
		            error:function(){
					}
				});
			});
		}
	}

	$(function(){
		initPage();

		$('#search-button').on('click', function(e) {
			e.preventDefault();
			search();
		});

		$('#slider').on('slide', function() {
			var slider = $('#slider').data('slider').getValue();
			$('#minPrice').val(slider[0]);
			$('#maxPrice').val(slider[1]);
		});

		/*$(window).on('popstate', function() {
			search();
		});*/

		$(".modal").on("hidden.bs.modal", function(){
		    $("#modalBody").html("");
		});		
	});
</script>
@stop

@section('modals')
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="opportunity-description-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                    Opportunity Description
                </h4>
            </div>
            <div class='modal-body' id="modalBody">
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="user-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h3 class="modal-title">
                    Please <a class="btn btn-info" href="{{ url('/login') }}">Login</a> OR <a class="btn btn-info" href="{{ url('/register') }}">Register</a> To Continue!!!
                </h3>
            </div>
        </div>
    </div>
</div>
@stop