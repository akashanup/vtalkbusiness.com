@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg">
    <div class="ct-mediaSection-inner">
        <div class="container">
            <div class="ct-steps--submission center-block ct-u-marginBottom20">
                <div class="ct-steps--item">
                    <div class="ct-steps--counter ct-steps--active">
                        <span>1</span>
                    </div>
                </div>
                <div class="ct-steps--item">
                    <div class="ct-steps--counter">
                        <span>2</span>
                    </div>
                </div>
                <div class="ct-steps--item">
                    <div class="ct-steps--counter">
                        <span>3</span>
                    </div>
                </div>
                <div class="ct-steps--item">
                    <div class="ct-steps--counter">
                        <span>4</span>
                    </div>
                </div>
            </div>
            <div class="ct-u-displayTable ct-steps--title text-center">
                <div class="ct-u-displayTableCell">
                    <span>Details</span>
                </div>
                <div class="ct-u-displayTableCell">
                    <span>Images & Tags</span>
                </div>
                <div class="ct-u-displayTableCell">
                    <span>Team Members & Products</span>
                </div>
                <div class="ct-u-displayTableCell">
                    <span>Additional Details</span>
                </div>
            </div>
        </div>
    </div>
</header>
@stop