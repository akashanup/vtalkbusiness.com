@extends('master')

@section('title')
	<title>Company Registration</title>
@stop

@section('breadcrumb')
	<!-- BreadCrumbs -->
	<div class="ct-site--map">
	    <div class="container">
	        <a href="index.html">Features</a>
	        <a href="features-buttons.html">Buttons</a>
	    </div>
	</div>
	<!-- BreadCrumb Ends -->
@stop

@section('page-header')
	<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
	    <div class="ct-mediaSection-inner">
	        <div class="container">
				<div class="ct-heading--main text-center">
				    <h3 class="text-uppercase ct-u-text--white">Company Form</h3>
				</div>
	        </div>
	    </div>
	</header>
@stop

@section('content')
	<section class="ct-u-paddingBoth70 ct-js-section text-left">
	    <div class="container">
			<div class="ct-heading text-center ct-u-marginBottom60">
			    <h3 class="text-uppercase">Company Details</h3>
			</div>

			
			@if($errors->any())
	        	@foreach($errors->all() as $error)
	        	<div class="errorMessage alert alert-danger">
	            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            	{{ $error }}
	        	</div>
	        	@endforeach
	        @endif

			<form method="post" name="companyRegistration" id="frm"><!-- action="{{ url('/company/create') }}">-->
				<div class="form-group col-md-6">
			        <label for="helptext">Company Name</label>
			        <input type="text" class="form-control ct-input--border ct-u-text--dark" id="name" placeholder="Company Name" name="name">
			    </div>
			    <div class="form-group col-md-6">
			        <label for="helptext">Company Email</label>
			        <input type="email" class="form-control ct-input--border ct-u-text--dark" id="email" placeholder="Company Email" name="email">
			    </div>
			    <div class="col-md-12"><br></div>
			    <div class="form-group col-md-6">
			        <label for="helptext">Company CIN</label>
			        <input type="text" class="form-control ct-input--border ct-u-text--dark" id="cin" placeholder="Company CIN" name="cin">
			    </div>
			    <div class="form-group col-md-6">
			        <label for="helptext">Company Phone No.</label>
			        <input type="text" class="form-control ct-input--border ct-u-text--dark" id="phone" placeholder="Company Phone No" name="phone">
			    </div>
			    <div class="col-md-12"><br></div>
			    <div class="form-group col-md-6">
			        <label for="helptext">Your Designation</label>
			        <input type="text" class="form-control ct-input--border ct-u-text--dark" id="designation" placeholder="Your Designation" name="designation">
			    </div>
			    <div class="form-group col-md-6">
			        <label for="helptext">Employee Range</label><br>
		          	<select id="employee" name="employee" class="form-control ct-input--border ct-u-text--dark">
					  <option value="">Select</option>
					  <option value="1-10">1-10</option>
					  <option value="11-50">11-50</option>
					  <option value="51-100">51-100</option>
					</select>
			    </div>
			    <div class="col-md-12"><br></div>
			    <div class="form-group col-md-6">
			        <label for="helptext">Company Tag</label><br>
		          	<select id="tags" name="tags[]" class="ct-input--border ct-u-text--dark" multiple="multiple">
					  @foreach($tags as $tags)
					  	<option value="{{$tags->id}}">{{$tags->tags}}</option>
					  @endforeach
					</select>
			    </div>
			    <div class="form-group col-md-6">
			        <label for="helptext">Company Evaluation</label>
			        <input type="number" class="form-control ct-input--border ct-u-text--dark" id="evaluation" placeholder="Company Evaluation" name="evaluation">
			    </div>
			    <div class="col-md-12"><br></div>
			    
			    <div class="form-group col-md-6">
			        <label for="helptext">Company Category</label>
			        <select class="ct-input--border ct-u-text--dark" id="category" placeholder="Company Category" name="category[]" multiple>
			        	@foreach($categories as $category)
			 			<option value="{{ $category->id }}">{{ $category->category }}</option>
			 			@endforeach
			 		</select>
			    </div>
			    <div class="col-md-12 adjust" style="display:none" id="address">
			    	<!--Address Form-->
			    </div>

			    <div class="col-md-6 adjust" style="display:none"></div>
			    <div class=" form-group col-md-4">
			    	<label for="helptext">&nbsp;</label>
			    	<a class="btn btn-success btn-block" id="addAddress"><i class="fa fa-plus"></i> Add Address</a>
			    </div>


			    <input type="hidden" name="_token" value={{csrf_token()}}>
			    
			    
			    <div class="form-group col-md-12 button">
			        <a id="submit" class="btn btn-info btn-block">Save</a>
			    </div>
			</form>
	    </div>
	</section>
@stop


@section('scripts')
	<script type="text/javascript">
		var AddressNo=0;	
		$('#addAddress').click(function(){
			AddressNo+=1;
			var htmlAddress="<div class='col-md-12'><br></div>"		+
					"<div class='form-group col-md-12'>"			+
					"<label>Address "+AddressNo+"</label><br>"		+	
					"</div>"										+
					"<div class='form-group col-md-6'>"				+	
					"<label for='helptext'>Street</label>"			+
					"<input type='text' class='form-control ct-input--border ct-u-text--dark' name='street[]' placeholder='Street'>"	+
					"</div>"										+
					"<div class='form-group col-md-6'>"				+	
					"<label for='helptext'>City</label>"			+
					"<input type='text' class='form-control ct-input--border ct-u-text--dark' name='city[]' placeholder='City'>"	+
					"</div>"										+
					"<div class='col-md-12'><br></div>"				+
					"<div class='form-group col-md-6'>"				+	
					"<label for='helptext'>State</label>"			+
					"<input type='text' class='form-control ct-input--border ct-u-text--dark' name='state[]' placeholder='State'>"	+
					"</div>"										+
					"<div class='form-group col-md-6'>"				+	
					"<label for='helptext'>Pincode</label>"			+
					"<input type='number' class='form-control ct-input--border ct-u-text--dark' name='pincode[]' placeholder='Pincode'>"	+
					"</div>"										+
					"<div class='col-md-12'><br></div>"				+
					"<div class='form-group col-md-6'>"				+	
					"<label for='helptext'>Country</label>"			+
					"<input type='text' class='form-control ct-input--border ct-u-text--dark' name='country[]' placeholder='Country'>"	+
					"</div>";
			$('.adjust').css('display','block');
			$('#address').append(htmlAddress);
		});

		$(".dropdown-menu li a").click(function(){
			var selText = $(this).text();
  			console.log(selText);
		});

		$('#submit').click(function(){
			var formData = JSON.parse(JSON.stringify(jQuery('#frm').serializeArray())) ;
	    	$.ajax({
				type:'POST',
				url: "{{ url('/company-registration') }}",
				data:formData,
	            success: function(data){
	            	window.location = '{{ url("/dashboard") }}';
	            },
	            error:function(){
	        		if(!($("#name").val()))    	
	        		{
	        			setError($("#name"), "Name field can't be blank.");
	        		}
	        		else
	        		{
	        			unsetError($("#name"));			
	        		}
	        		if(!($("#email").val()))    	
	        		{
	        			setError($("#email"), "Email field can't be blank.");
	        		}
	        		else
	        		{
	        			unsetError($("#email"));			
	        		}
	        		if(!($("#cin").val()))    	
	        		{
	        			setError($("#cin"), "SIN field can't be blank.");
	        		}
	        		else
	        		{
	        			unsetError($("#cin"));			
	        		}
	        		if(!($("#phone").val()))    	
	        		{
	        			setError($("#phone"), "Phone field can't be blank.");
	        		}
	        		else
	        		{
	        			unsetError($("#phone"));			
	        		}
	        		if(!($("#designation").val()))    	
	        		{
	        			setError($("#designation"), "Designation field can't be blank.");
	        		}
	        		else
	        		{
	        			unsetError($("#designation"));			
	        		}
	        		if(!($("#employee").val()))    	
	        		{
	        			setError($("#employee"), "Employee field can't be blank.");
	        		}
	        		else
	        		{
	        			unsetError($("#employee"));			
	        		}
	            }
	   		});
		});

	function setError(element, message) {
		var parent = element.parents('.form-group:eq(0)');
		if(parent.hasClass('has-error'))
			return;
		parent.addClass('has-error').append('<span class="help-block">' + message + '</span>')
	}
	function unsetError(element) {
		var parent = element.parents('.form-group:eq(0)');
		parent.removeClass('has-error');	
		parent.find('.help-block').remove();
	}

	$(function(){
		$('#email').on('blur', function(){
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if(!regex.test($(this).val()))
			{
		 		setError($(this), "Please provide a valid email address.");
			}
			else
			{
				unsetError($(this));
			}
		});


		$('#phone').on('blur', function(){
			var regex = /^(\+\d{1,3}[- ]?)?\d{10}$/;
			if(!regex.test($(this).val()))
			{
			 	setError($(this), "Please provide a valid Phone No.");
			}
			else
			{
				unsetError($(this));
			}
		});

		$('#tags, #category').select2({});
	});
	</script>
@stop

















