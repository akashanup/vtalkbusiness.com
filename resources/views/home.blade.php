@extends('master')

@section('title')
<title>Home</title>
@stop

@section('page-header')
<header>
    <div class="ct-js-owl" data-items="1" data-navigation="true" data-autoPlay="true" data-lazyLoad="true" data-animations="true">
        <div class="item">
            <div class="ct-owl--background">
                <img src="{{ asset('/images/banner1.jpg') }}" alt="">
            </div>
            <div class="ct-owl--description">
                <div class="ct-owl--description--inner">
                    <div class="ct-owl--descriptionItem">
                        <div class="container">
                            <!-- <img class="ct-u-marginBottom20 animated" data-fx="fadeInDown" data-time="300" src="assets/images/content/logo-slider.png" alt=""> -->
                            <h2 class="ct-u-marginBottom20 animated" data-fx="fadeInDown" data-time="300">Your <span>Growth</span> Partner</h2>
                            <a class="btn btn-default btn-transparent--border ct-u-text--white animated" data-fx="fadeInUp" data-time="300" href="{{ url('/register') }}">Register Now</a>
                            <!-- <a class="btn btn-success ct-btn--green animated" data-fx="fadeInUp" data-time="300" href="submission.html">List Property</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="ct-owl--background">
                <img src="http://estato.html.themeforest.createit.pl/assets/images/demo-content/header-slider-1.jpg" alt="">
            </div>
            <div class="ct-owl--description">
                <div class="ct-owl--description--inner">
                    <div class="ct-owl--descriptionItem">
                        <div class="container">
                            <div class="ct-textHeader">
                                <h1 class="text-capitalize ct-u-marginBottom30 animated" data-fx="fadeInDown">Get 1 year free subscription of Business Account</h1>
                                <a class="btn btn-success ct-btn--green animated" data-fx="fadeInLeft" href="{{ url('/register') }}">Register Now</a>
                                <!-- <a class="btn btn-default btn-transparent--border ct-u-text--white animated" data-fx="fadeInRight" href="agency.html">See Agents</a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="item">
            <div class="ct-owl--background">
                <img src="{{ asset('/images/banner2.jpg') }}">
            </div>
            <div class="ct-owl--description">
                <div class="ct-owl--description--inner">
                    <div class="ct-owl--descriptionItem">
                        <div class="container">
                            <div class="ct-textHeader ct-textHeader--Square animated" data-fx="zoomIn">
                                <img src="{{ asset('/images/vtalk-logo.png') }}" alt="Logo">
                                <h2 class="text-uppercase" data-fx="fadeInUp">Together, We will do Great</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</header>
@stop

@section('content')
<section>
	<div class="container" id="search">
		<form class="ct-formSearch--extended ct-u-marginBottom50" role="form" method="GET">
		    <div class="form-group ">
		        <div class="ct-form--label--type1">
		            <div class="ct-u-displayTableVertical">
		                <div class="ct-u-displayTableCell">
		                    <div class="ct-input-group-btn">
		                        <button class="btn btn-primary">
		                            <i class="fa fa-search"></i>
		                        </button>
		                    </div>
		                </div>
		                <div class="ct-u-displayTableCell text-center">
		                    <span class="text-uppercase">Search for Business/Oppurtunities</span>
		                </div>
		            </div>
		        </div>
		        <div class="ct-u-displayTableVertical ct-u-marginBottom20">
		            
		            <div class="ct-u-displayTableCell">
		                <div class="ct-form--item">
		                    <label>Name</label>
		                    <input type="text" class="form-control" id="name" class="name" name="name">
		                </div>
		            </div>
		            <div class="ct-u-displayTableCell">
		                <div class="ct-form--item">
		                    <label>Industry Categories</label>
		                    <select id="category" name="category[]" class="ct-js-select ct-select-lg" multiple="multiple">
					  			@foreach($category as $category)
					  				<option value="{{$category->id}}">{{$category->category}}</option>
					  			@endforeach
							</select>
		                </div>
		            </div>
		            <div class="ct-u-displayTableCell">
		                <div class="ct-form--item">
		                    <label>Country</label>
		                    <select id="country" name="country[]" class="ct-js-select ct-select-lg" multiple>
					  			@foreach($countries as $country)
					  				<option value="{{$country->countryId}}">{{$country->country}}</option>
					  			@endforeach
							</select>
		                </div>
		            </div>
		        </div>
		        <div class="ct-u-displayTableVertical ct-slider--row ct-sliderAmount">
		            <div class="ct-u-displayTableCell">
		                <div class="ct-form--item">
		                    <label>Tags</label>
		                    <select id="tags" name="tags[]" class="ct-js-select ct-select-lg" multiple="multiple">
					  			@foreach($tags as $tag)
					  				<option value="{{$tag->id}}">{{$tag->tags}}</option>
					  			@endforeach
							</select>
		                </div>
		            </div>
		            <div class="ct-u-displayTableCell">
		                <div class="ct-form--item">
		                    <label>Price (INR)</label>
		                    <input type="text" value="100000" required class="form-control input-lg ct-js-slider-min" placeholder="" name="minPrice" id="minPrice">
		                </div>
		            </div>
		            <div class="ct-u-displayTableCell">
		                <input type="text" class="slider ct-js-sliderAmount" value="" data-slider-tooltip="hide" data-slider-handle="square" data-slider-min="100000" data-slider-max="1000000" data-slider-step="50000" data-slider-value="[100000, 1000000]">
		                <label>Min</label>
		                <label class="pull-right">Max</label>
		            </div>
		            <div class="ct-u-displayTableCell">
		                <div class="ct-form--item">
		                    <input type="text" value="1000000" required class="form-control input-lg ct-js-slider-max" placeholder="" name="maxPrice" id="maxPrice">
		                </div>
		            </div>
		            <div class="ct-u-displayTableCell">
		                <input type="button" id="searchNow" class="btn btn-warning text-capitalize pull-right" value="search now">
		            </div>
		        </div>
		    </div>
		</form>
		<div class="ct-section--products"><br><br><br>
		    <div class="row">
		        <div class="col-md-8 col-lg-9">
		            <div class="ct-heading ct-u-marginBottom30">
		                <h3 class="text-uppercase">featured listings</h3>
		                <!-- <a href="search-list.html" class="pull-right"><i class="fa fa-angle-right"></i>View More</a> -->
		            </div>
		            <div class="row">
		            	@foreach($featured_companies as $company)
		            	<div class="col-sm-6 col-md-6 col-lg-4">
		                    <div class="ct-itemProducts ct-u-marginBottom30 ct-hover">
		                    	@if(Session::has('user_role'))
		                        <a href="{{ url("/company/{$company->id}") }}">
		                        @else
		                        <a id='goComapny' href='#user-modal' data-toggle='modal' data-target='#user-modal'>
		                        @endif
		                            <div class="ct-main-content">
		                                <div class="ct-imageBox">
		                                   <div class="logo-middle-container"><img src="{{ $company->logo }}" alt=""></div><i class="fa fa-eye"></i>
		                                </div>
		                                <div class="ct-main-text">
		                                    <div class="ct-product--tilte one-line-elipsis" titile="{{ $company->name }}">
		                                        {{ $company->name }}
		                                    </div>
		                                    <div class="ct-product--price">
		                                        <p class="one-line-elipsis">{{ $company->countries }}</p>
		                                    </div>
		                                    <div class="ct-product--description one-line-elipsis">
		                                        {{ $company->tagline }}
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="ct-product--meta">
		                                <div class="ct-text">
		                                    <span><i class="fa fa-user"></i> {{ $company->user_name }}</span>
		                                </div>
		                            </div>
		                        </a>
		                    </div>
		                </div>
		            	@endforeach
		            </div>
		        </div>
		        <div class="col-md-4 col-lg-3">
		            <div class="ct-js-sidebar">
		                <div class="row">
		                    <div class="col-sm-6 col-md-12">
		                    	<h4>Latest Videos</h4><hr>
		                    	@foreach($videos as $video)
		                    	<p>{{ $video->Title }}</p>
		                    	<iframe src="{{ $video->URL }}"></iframe><hr>
		                    	@endforeach
		                    	<!-- <p>Make In India Global Business Summit 2015, Singapore - Vijay Thakur Singh, High Comm. of India...</p>
		                    	<iframe src="http://www.youtube.com/embed/8DaWCZBnDxo"></iframe><hr>
		                    	<p>Make in India Global Business Summit 2015, Singapore - Ravindra Nath, CMD, NSIC - Govt of India</p>
		                    	<iframe src="http://www.youtube.com/embed/oeB_gOrlFaU"></iframe><hr> -->
		                        <!-- <div class="widget ct-widget--calculator">
		                            <div class="widget-inner">
		                                <h4 class="text-uppercase">Calculator</h4>
		                                <form role="form" class="ct-form--darkStyle">
		                                    <div class="form-group">
		                                        <div class="ct-form--label--type2">
		                                            <div class="ct-u-displayTableVertical">
		                                                <div class="ct-u-displayTableCell">
		                                                    <div class="ct-input-group-btn">
		                                                        <button class="btn btn-primary">
		                                                            <i class="fa fa-calculator"></i>
		                                                        </button>
		                                                    </div>
		                                                </div>
		                                                <div class="ct-u-displayTableCell text-center">
		                                                    <span class="text-uppercase">Calculate Loan</span>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        <div class="ct-form--item">
		                                            <label>Property Price ($)</label>
		                                            <input type="text" required class="form-control input-lg" placeholder="Any">
		                                        </div>
		                                        <div class="ct-form--item">
		                                            <label>Procent Down</label>
		                                            <input type="text" required class="form-control input-lg" placeholder="Any">
		                                        </div>
		                                        <div class="ct-form--item">
		                                            <label>Term (Years)</label>
		                                            <input type="text" required class="form-control input-lg" placeholder="Any">
		                                        </div>
		                                        <div class="ct-form--item">
		                                            <label>Interest Rate in %</label>
		                                            <input type="text" required class="form-control input-lg" placeholder="Any">
		                                        </div>
		                                        <div class="ct-form--item last">
		                                            <button type="submit" class="btn btn-warning">Calculate My Mortgage</button>
		                                        </div>
		                                    </div>
		                                </form>
		                            </div>
		                        </div> -->
		                    </div>
		                    <!-- <div class="col-sm-6 col-md-12">
		                        <div class="widget ct-widget--recentlyReduced">
		                            <div class="widget-inner">
		                                <h4 class="text-uppercase">Recently Reduced</h4>
		                                <div class="ct-itemProducts--small ct-itemProducts--small-type1">
		                                    <a href="product-single.html">
		                                        <div class="ct-main-content">
		                                            <div class="ct-imageBox">
		                                               <img src="assets/images/content/recently-reduced-thumbnail-1.jpg" alt="">
		                                            </div>
		                                            <div class="ct-main-text">
		                                                <div class="ct-product--tilte">
		                                                    4079 Broad Falls, Turkeytown
		                                                </div>
		                                                <div class="ct-product--price">
		                                                    <span class="ct-price--Old">$ 450,000</span>
		                                                    <span>$ 390,000</span>
		                                                </div>
		                                            </div>
		                                        </div>
		                                    </a>
		                                </div>
		                                <div class="ct-itemProducts--small ct-itemProducts--small-type1">
		                                    <a href="product-single.html">
		                                        <div class="ct-main-content">
		                                            <div class="ct-imageBox">
		                                                <img src="assets/images/content/recently-reduced-thumbnail-2.jpg" alt="">
		                                            </div>
		                                            <div class="ct-main-text">
		                                                <div class="ct-product--tilte">
		                                                    5469 Clear Rise, Little Otter
		                                                </div>
		                                                <div class="ct-product--price">
		                                                    <span class="ct-price--Old">$ 450,000</span>
		                                                    <span>$ 390,000</span>
		                                                </div>
		                                            </div>
		                                        </div>
		                                    </a>
		                                </div>
		                                <div class="ct-itemProducts--small ct-itemProducts--small-type1">
		                                    <a href="product-single.html">
		                                        <div class="ct-main-content">
		                                            <div class="ct-imageBox">
		                                                <img src="assets/images/content/recently-reduced-thumbnail-3.jpg" alt="">
		                                            </div>
		                                            <div class="ct-main-text">
		                                                <div class="ct-product--tilte">
		                                                    4770 Cotton Berry Point, Carrot River
		                                                </div>
		                                                <div class="ct-product--price">
		                                                    <span class="ct-price--Old">$ 450,000</span>
		                                                    <span>$ 390,000</span>
		                                                </div>
		                                            </div>
		                                        </div>
		                                    </a>
		                                </div>
		                                <a class="btn btn-primary btn-transparent--border ct-u-text--motive" href="properties.html">View More Reduced Properties</a>
		                            </div>
		                        </div>
		                    </div> -->
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</section>
<section class="ct-u-paddingBoth30 ct-js-section ct-section--inline" data-bg-color="#60a7d4">
    <div class="container">
        <div class="ct-u-displayTableVertical">
            <div class="ct-u-displayTableCell">
                <div class="ct-u-displayTableCell">
                    <img src="{{ asset('/images/vtalk-logo.png') }}" alt="">
                </div>
                <div class="ct-u-displayTableCell">
                    <h4 class="text-uppercase">We Sell Opportunities</h4>
                </div>
            </div>
            <div class="ct-u-displayTableCell">
                <div class="ct-u-displayTableCell pull-right">
                    <!-- <a class="btn btn-default btn-transparent--border ct-u-text--white btn-hoverWhite" href="blog-single.html">Read More</a> -->
                    <a class="btn btn-dark btn-hoverWhite" href="{{ url('/register') }}">Register Now</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ct-u-paddingBoth60 ct-js-section" data-bg-color="#f3f3f3">
    <div class="container">
        <div class="ct-heading ct-u-marginBottom30">
            <h3 class="text-uppercase">Latest Listings</h3>
            <a href="{{ url('/search') }}" class="pull-right"><i class="fa fa-angle-right"></i>View More</a>
        </div>
        <div class="ct-js-owl ct-owl-controls--type2" data-single="false" data-items="4" data-pagination="false">
        	@foreach($companies as $company)
            <div class="item">
                <div class="ct-itemProducts ct-u-marginBottom30 ct-hover">
                    <!-- <label class="control-label sale">
                        Sale
                    </label> -->
                    @if(Session::has('user_role'))
                    <a href="{{ url("/company/{$company->id}") }}">
                    @else
                    <a id='goComapny' href='#user-modal' data-toggle='modal' data-target='#user-modal'>
                    @endif
                        <div class="ct-main-content">
                            <div class="ct-imageBox">
                                <div class="logo-middle-container"><img src="{{ $company->logo }}" alt=""></div><i class="fa fa-eye"></i>
                            </div>
                            <div class="ct-main-text">
                                <div class="ct-product--tilte one-line-elipsis" titile="{{ $company->name }}">
                                    {{ $company->name }}
                                </div>
                                <div class="ct-product--price">
                                    <!-- <span class="ct-price--Old">$ 450,000</span> -->
                                    <p class="one-line-elipsis">{{ $company->countries }}</p>
                                </div>
                                <div class="ct-product--description one-line-elipsis">
                                    {{ $company->tagline }}
                                </div>
                            </div>
                        </div>
                        <div class="ct-product--meta">
                            <!-- <div class="ct-icons">
                                <span>
                                    <i class="fa fa-bed"></i> 3
                                </span>
                                <span>
                                    <i class="fa fa-cutlery"></i> 2
                                </span>
                            </div> -->
                            <div class="ct-text">
                                <span><i class="fa fa-user"></i> <span>{{ $company->user_name }}</span></span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<section class="ct-u-paddingBoth60 ct-blog--small">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="ct-heading ct-u-marginBottom30">
                    <h3 class="text-uppercase">Latest Opportunities</h3>
                </div>
                <div class="ct-u-marginBottom30">
                	@foreach($opportunities as $opportunity)
                    <div class="ct-articleBox ct-articleBox--list">
                        <div class="ct-articleBox-media">
                            <img src="{{ $opportunity->logo }}" alt="">
                        </div>
                        <div class="ct-articleBox-titleBox text-uppercase">

                            <span href="#" class="one-line-elipsis">{{ $opportunity->headline }}</span>
                            <div class="ct-articleBox-meta">
                                <span>Posted By <a href="">{{ $opportunity->user_name }}</a></span>
                            </div>
							<div class="ct-articleComments">
                                <span>Value: INR&nbsp;{{ $opportunity->capital }}</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @endforeach
                    <a href="{{ url('showOpportunities') }}">View all Opportunities</a>
                    <!-- <div class="ct-articleBox ct-articleBox--list">
                        <div class="ct-calendar--day text-center">
                            <div class="ct-day">
                                <span>05</span>
                            </div>
                            <div class="ct-month">
                                <span class="text-uppercase">jan</span>
                            </div>
                        </div>
                        <div class="ct-articleBox-media">
                            <img src="http://image.airlineratings.com/logos/IndiGo_Logo.jpg" alt="">
                        </div>
                        <div class="ct-articleBox-titleBox text-uppercase">
                    
                            <a href="http://economictimes.indiatimes.com/markets/ipos/fpos/after-indigos-success-goair-eyes-up-to-150-million-via-ipo-sources/articleshow/49571675.cms">After IndiGo's success, GoAir eyes up to $150 million via IPO: Sources</a>
                            <div class="ct-articleBox-meta">
                                <span>Posted on Economic Times</span>
                            </div>
                            <div class="ct-articleComments">
                                <a href="http://economictimes.indiatimes.com/markets/ipos/fpos/after-indigos-success-goair-eyes-up-to-150-million-via-ipo-sources/articleshow/49571675.cms">Read more...</a>
                            </div>
                    
                        </div>
                        <div class="clearfix"></div>
                    </div> -->
                </div>
                <!-- <a href="blog.html" class="btn btn-primary btn-transparent--border ct-u-text--motive ct-u-marginBottom30">Read More On Blog</a> -->
            </div>
            <div class="col-md-4">
                <div class="ct-heading ct-u-marginBottom30">
                    <h3 class="text-uppercase">Questions ?</h3>
                </div>
				<div class="errorMessage" id="contact-error">
                @if($errors->any())
                	@foreach($errors->all() as $error)
                	<div class="errorMessage alert alert-danger">
                    	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    	{{ $error }}
                	</div>
                	@endforeach
                @endif
                </div>
                <form role="form" action="{{ url('/contact') }}" method="post" class="contactForm ct-contactForm--small" id="contact-form">
                	<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <div class="ct-form--label--type2">
                            <div class="ct-u-displayTableVertical">
                                <div class="ct-u-displayTableCell">
                                    <div class="ct-input-group-btn">
                                        <button class="btn btn-primary">
                                            <i class="fa fa-envelope-o"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="ct-u-displayTableCell text-center">
                                    <span class="text-uppercase">Feel free to contact us</span>
                                </div>
                            </div>
                        </div>
                        <div class="ct-image--section">
                            <img src="{{ asset('/images/vtalk-logo.png') }}" alt="">
                        </div>
                        <div class="ct-form--item">
                            <label>Name</label>
                            <input id="contact_name" type="text" required class="form-control input-lg" name="name" placeholder="">
                        </div>
                        <div class="ct-form--item">
                            <label>Email</label>
                            <input id="contact_email" type="email" class="form-control input-lg" name="email" required>
                        </div>
                        <div class="ct-form--item">
                            <label>Phone</label>
                            <input id="contact_phone" type="tel" class="form-control input-lg" name="phone" placeholder="" required>
                        </div>
                        <div class="ct-form--item">
                            <label>Message</label>
                            <textarea id="contact_message" placeholder="" class="form-control input-lg" rows="4" name="message" required></textarea>
                        </div>
                        <div class="ct-form--item last">
                            <button type="submit" id="messageSend" class="btn btn-warning text-capitalize center-block">Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- <section class="ct-mediaSection text-center ct-u-paddingBoth100" data-stellar-background-ratio="0.3" data-height="765" data-type="parallax" data-bg-image="assets/images/content/iconBox-parallax.jpg" data-bg-image-mobile="assets/images/content/iconBox-parallax.jpg">
    <div class="ct-mediaSection-inner">
        <div class="container">
            <div class="ct-heading ct-u-marginBottom60">
                <h3 class="text-uppercase ct-u-text--white">Why estato?</h3>
            </div>
            <div class="ct-decoration center-block ct-u-marginBottom60">
                <div class="ct-decoration--inner center-block"></div>
            </div>
            <div class="row ct-u-text--white">
                <div class="col-sm-6 col-md-3">
                    <div class="ct-iconBox">
                        <div class="ct-icon ct-iconContainer--circle ct-iconContainer--circleHoverLight center-block ct-u-marginBottom30">
                            <i class="fa fa-bolt"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title ct-u-marginBottom20 ct-fw-600">Powerfull</span>
                            <span class="ct-text ct-u-marginBottom20">Estato is designed a way that it was flexible for different types of business.</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="ct-iconBox">
                        <div class="ct-icon ct-iconContainer--circle ct-iconContainer--circleHoverLight center-block ct-u-marginBottom30">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title ct-u-marginBottom20 ct-fw-600">User Oriented</span>
                            <span class="ct-text ct-u-marginBottom20">Estato is designed a way that it was flexible for different types of business.</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="ct-iconBox">
                        <div class="ct-icon ct-iconContainer--circle ct-iconContainer--circleHoverLight center-block ct-u-marginBottom30">
                            <i class="fa fa-heart-o"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title ct-u-marginBottom20 ct-fw-600">Beautiful</span>
                            <span class="ct-text ct-u-marginBottom20">Estato is designed a way that it was flexible for different types of business.</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="ct-iconBox">
                        <div class="ct-icon ct-iconContainer--circle ct-iconContainer--circleHoverLight center-block ct-u-marginBottom30">
                            <i class="fa fa-life-ring"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title ct-u-marginBottom20 ct-fw-600">Fast support</span>
                            <span class="ct-text ct-u-marginBottom20">Estato is designed a way that it was flexible for different types of business.</span>
                        </div>
                    </div>
                </div>
            </div>
            <a class="btn btn-primary btn-transparent--border ct-u-text--motive ct-u-marginTop80" href="">More Reasons to Buy Estato</a>
        </div>
    </div>
</section>
<section class="ct-js-section ct-u-paddingBoth100 text-center" data-bg-color="#60a7d4">
    <div class="container">
        <div class="ct-heading ct-u-marginBottom60">
            <h3 class="text-uppercase ct-u-text--white">Testimonials</h3>
        </div>
        <div class="ct-testimonials">
            <div class="row ct-u-marginBottom60">
                <ul class="list-unstyled" role="tablist">
                    <li role="presentation" class="col-xs-2 active ct-u-paddingBottom60"><a href="#testimonials-01" role="tab" data-toggle="tab"><img class="img-responsive" draggable="false" src="assets/images/content/content-testimonials-1.png" alt="Team Member"></a></li>
                    <li role="presentation" class="col-xs-2 ct-u-paddingBottom60"><a href="#testimonials-02" role="tab" data-toggle="tab"><img class="img-responsive" src="assets/images/content/content-testimonials-2.png" alt="Team Member"></a></li>
                    <li role="presentation" class="col-xs-2 ct-u-paddingBottom60"><a href="#testimonials-03" role="tab" data-toggle="tab"><img class="img-responsive" draggable="false" src="assets/images/content/content-testimonials-3.png" alt="Team Member"></a></li>
                    <li role="presentation" class="col-xs-2 ct-u-paddingBottom60"><a href="#testimonials-04" role="tab" data-toggle="tab"><img class="img-responsive" src="assets/images/content/content-testimonials-4.png" alt="Team Member"></a></li>
                    <li role="presentation" class="col-xs-2 ct-u-paddingBottom60"><a href="#testimonials-05" role="tab" data-toggle="tab"><img class="img-responsive" draggable="false" src="assets/images/content/content-testimonials-5.png" alt="Team Member"></a></li>
                    <li role="presentation" class="col-xs-2 ct-u-paddingBottom60"><a href="#testimonials-06" role="tab" data-toggle="tab"><img class="img-responsive" src="assets/images/content/content-testimonials-6.png" alt="Team Member"></a></li>
                </ul>
            </div>
            <div class="tab-content center-block">
                <div role="tabpanel" class="tab-pane row" id="testimonials-01">
                    <div class="animated fadeIn">
                        <p class="">Totally impressed with Estato. Very easy to use and it really has a lot of features.</p>
                        <span class="">John Doe - Company inc.</span>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane active row" id="testimonials-02">
                    <div class="animated fadeIn">
                        <p class="">Estato is exactly what our business has been lacking, impressed me on multiple levels. Estato should be nominated for service of the year.</p>
                        <span class="">Anna Ferguson - Company inc.</span>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane row" id="testimonials-03">
                    <div class="animated fadeIn">
                        <p class="">Estato is the most valuable business resource we have EVER purchased. Great job, I will definitely be ordering again!</p>
                        <span class="">Jane Foster - Company inc.</span>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane row" id="testimonials-04">
                    <div class="animated fadeIn">
                        <p class=" ct-u-marginBottom0">Thanks for the great service. Estato is both attractive and highly adaptable. I will recommend you to my colleagues.</p>
                        <span class="">Raine Doe - Company inc.</span>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane row" id="testimonials-05">
                    <div class="animated fadeIn">
                        <p class="">I didn't even need training. Estato has completely surpassed our expectations, surpassed our expectations. We were treated like royalty.</p>
                        <span class="">Alvera  Foster - Company inc.</span>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane row" id="testimonials-06">
                    <div class=" animated fadeIn">
                        <p class="">Estato is worth much more than I paid. If you want real marketing that works and effective implementation - Estato's got you covered</p>
                        <span class="">Morena Doe - Company inc.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ct-u-paddingBottom60 text-center">
    <div class="container">
        <div class="ct-heading ct-u-marginBoth60">
            <h3 class="text-uppercase">our partners</h3>
        </div>
        <div class="ct-js-owl" data-items="6" data-navigation="false" data-autoPlay="true" data-single="false" data-pagination="false">
            <div class="item"><img src="assets/images/content/content-partners-1.png" alt=""></div>
            <div class="item"><img src="assets/images/content/content-partners-2.png" alt=""></div>
            <div class="item"><img src="assets/images/content/content-partners-3.png" alt=""></div>
            <div class="item"><img src="assets/images/content/content-partners-4.png" alt=""></div>
            <div class="item"><img src="assets/images/content/content-partners-5.png" alt=""></div>
            <div class="item"><img src="assets/images/content/content-partners-6.png" alt=""></div>
            <div class="item"><img src="assets/images/content/content-partners-1.png" alt=""></div>
            <div class="item"><img src="assets/images/content/content-partners-2.png" alt=""></div>
        </div>
    </div>
</section>
 -->

@stop

@section('scripts')
	<script type="text/javascript">
		$(function(){
			$('#tags').select2({});

			$('#contact-form').on('submit', function(e) {
				e.preventDefault();
				var data = $('#contact-form').serialize();
				$.ajax({
					type: 'post',
					url : '{{ url("/contact") }}',
					data: data
				}).done(function(response) {
					if(response.status) {
						bootbox.alert('Message send Successfully. Our Customer Executive will contact you soon.');
					}
				}).fail(function(xhr) {
					if(xhr.status == 422) {
						errors = JSON.parse(xhr.responseText);
						myErrors = [];
						$.each(errors, function(i, v) {
							$.each(v, function(j, y) {
								myErrors.push(y);
							});
						});
						bootbox.alert('You have some errors in contact form please review and try again.');
						var html = '';
						$.each(myErrors, function(i, v) {
							html += '<div class="errorMessage alert alert-danger">\
		                    	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + v +
		                	'</div>';
						});
						$('#contact-error').empty().append(html);
					}
				});
			});

			$('#searchNow').click(function(){
				var formData 	= 	"name=";
				 if($('#name').val())
		  		{
		  			formData 	+=	encodeURIComponent($('#name').val());
		  		}

				formData 		+= 	"&category=" ;

				if($('#category').val())	
				{
					formData 	+= 	encodeURIComponent($('#category').val().toString());
				} 	

				formData 		+= 	"&country="	;

				if($('#country').val())	
				{
					formData 	+= 	encodeURIComponent($('#country').val().toString());
				}
				formData 		+= 	"&tags=" ;

				if($('#tags').val())
				{
					formData 	+= 	encodeURIComponent($('#tags').val().toString());
				}

				formData 		+= 	"&minPrice=" ;

				if($('#minPrice').val())	
				{
					formData 	+= 	encodeURIComponent($('#minPrice').val());
				}

				formData 		+= 	"&maxPrice=" ;

				if($('#maxPrice').val())	
				{
					formData 	+= 	encodeURIComponent($('#maxPrice').val());
				}				
				window.location = '{{ url("/search")}}' + "?" +formData;
			});
			
			
		});
	</script>
@stop

@section('modals')
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="user-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h3 class="modal-title">
                    Please <a class="btn btn-info" href="{{ url('/login') }}">Login</a> OR <a class="btn btn-info" href="{{ url('/register') }}">Register</a> To Continue!!!
                </h3>
            </div>
        </div>
    </div>
</div>


@stop
