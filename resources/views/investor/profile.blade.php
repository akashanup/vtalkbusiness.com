@extends('master')

@section('title')
    <title>Investor Profile</title>
@stop

@section('breadcrumb')
<div class="ct-site--map">
    <div class="container">
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <a href="{{ url('/investor') }}">Investor Profile</a>
    </div>
</div>
@stop



@section('content')
<header class="ct-mediaSection ct-u-paddingBoth100" data-stellar-background-ratio="0.3" data-height="470" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg">
    <div class="container">
        <div class="ct-personBox ct-personBox--extended ct-personBox--extendedLight text-left">
            <div class="ct-imagePerson">
                <img src="{{ $investor->image }}">
                @if(Session::get('user_id') == $investor->user_id)
                <a class="btn btn-xs btn-edit text-uppercase ct-js-btnEdit--Agents" data-toggle="modal" data-target="#image-upload-modal">edit</a>
                @endif
                @if(Session::get('user_role') == 2)
                <ul class="ct-panel--socials list-inline list-unstyled">
                    <li><a href="{{ $investor->facebook }}" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-facebook"></i></div></a></li>
                    <li><a href="{{ $investor->twitter }}" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-twitter"></i></div></a></li>
                    <li><a href="{{ $investor->linkedin }}" target="_blank"><div class="ct-socials ct-socials--circle"><i class="fa fa-linkedin"></i></div></a></li>
                </ul>
                @endif
            </div>
            <div class="ct-personContent">
                <div class="ct-personName ct-u-paddingBoth10 text-uppercase">
                    <h2>{{ $investor->investor_name }}</h2>
                    <a href="">{{ $investor->venture_name }}</a>
                </div>
                <div class="ct-personDescription pull-right ct-u-paddingBoth20">
                    <ul class="list-unstyled ct-contactPerson pull-right">
                        @if(Session::get('user_role') == 2)
                        <li>
                            <i class="fa fa-phone"></i>
                            <span>{{ $investor->phone }}</span>
                        </li>
                        <li>
                            <i class="fa fa-skype"></i> {{ $investor->skype }}
                        </li>
                        @endif
                        <li>
                            INR 
                            <span>{{ $investor->min_capital }} - {{ $investor->max_capital }}</span>
                        </li>
                        @if(Session::get('user_role') == 2)
                        <li>
                            <a href=""><i class="fa fa-envelope-o"></i></a> {{ $investor->email }}
                        </li>
                        @endif
                    </ul>
                </div>
                <div class="ct-buttonPanel text-capitalize ct-u-paddingBoth20 pull-right">
                    @if(Session::get('user_role') == 2)
                    <!-- <a href="mailto:{{ $investor->email }}" class="btn btn-default btn-transparent--border ct-u-text--white btn-hoverWhite">Contact User</a> -->
                    <!-- <a href="#" class="btn btn-default btn-transparent--border ct-u-text--white btn-hoverWhite">View Portfolio</a> -->
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>
    @if(Session::get('user_id') == $investor->user_id)
    d
    <section class="ct-u-paddingBoth70 ct-js-section text-left">
        <div class="container">
            <div class="ct-heading text-center ct-u-marginBottom60">
                <h3 class="text-uppercase">Edit Investor Profile</h3>
            </div>
            @if($errors->any())
                @foreach($errors->all() as $error)
                <div class="errorMessage alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $error }}
                </div>
                @endforeach
            @endif
            <form method="post" id="frm" action="{{ url('/investor') }}">            
                <div class="form-group col-md-4">
                    <label for="helptext">Contact Person</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="investor_name" placeholder="Contact Person Name" name="investor_name" value="{{ $investor->investor_name }}">
                </div>
                <div class="form-group col-md-4">
                    <label for="helptext">Venture Name</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="venture_name" placeholder="Venture Name" name="venture_name" value="{{ $investor->venture_name }}">
                </div>
                <div class="form-group col-md-4">
                    <label for="helptext">Phone</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="phone" placeholder="phone" name="phone" value="{{ $investor->phone }}">
                </div>  
                <div class="form-group col-md-4">
                    <label for="helptext">Email</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="email" placeholder="Contact Email" name="email" value="{{ $investor->email }}">
                </div>              
                <div class="form-group col-md-4">
                    <label for="helptext">Skype</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="skype" placeholder="skype" name="skype" value="{{ $investor->skype }}">
                </div>
                <div class="form-group col-md-4">
                    <label for="helptext">Facebook URL</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="facebook" placeholder="Facebook URL" name="facebook" value="{{ $investor->facebook }}">
                </div>
                <div class="form-group col-md-4">
                    <label for="helptext">Twitter Name</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="twitter" placeholder="Twitter Name" name="twitter" value="{{ $investor->twitter }}">
                </div>
                <div class="form-group col-md-4">
                    <label for="helptext">Linkedin URL</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="linkedin" placeholder="Linkedin URL" name="linkedin" value="{{ $investor->linkedin }}">
                </div>
                <div class="form-group col-md-2">
                    <label for="helptext">Minimum Capital</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="min_capital" placeholder="" name="min_capital" value="{{ $investor->min_capital }}">
                </div>
                <div class="form-group col-md-2">
                    <label for="helptext">Maximum Capital</label>
                    <input type="text" class="form-control ct-input--border ct-u-text--dark" id="max_capital" placeholder="" name="max_capital" value="{{ $investor->max_capital }}">
                </div>
                <div class="form-group col-md-12">
                    <label for="helptext">Interested Industries <span>*</span></label>
                    <select class="ct-input--border ct-u-text-dark" id="interests" name="interests[]" multiple>
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->category }}</option>
                        @endforeach
                    </select>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">             
                <div class="form-group col-md-12 button">
                    <input type="Submit"id="submit"class="btn btn-info btn-block"value="Save" >
                </div>
            </form>
        </div>
    </section>
    @endif
@stop

@section('modals')
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="image-upload-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<h4 class="modal-title">
					Upload Image
				</h4>
			</div>
			<div class="modal-body">
				<form method="post" action="{{ url('/investor/picture/upload') }}" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col-md-12 form-group">
							<span class="btn btn-default btn-block">
								<input type="file" id="image" name="image">
							</span>
						</div>
						<div class="col-md-6 form-group">
							<input type="submit" class="btn btn-sm btn-info" value="Save">
						</div>
						<div class="col-md-6 form-group">
							<button class="btn btn-default btn-sm btn-block" data-dismiss="modal" aria-hidden="true">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
    $(function() {
        $('#interests').val([{{ implode($userCategories, ',') }}]).select2();
    });
</script>
@stop