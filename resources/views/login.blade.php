@extends('master')

@section('title')
<title>Login</title>
@stop

@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
    <div class="ct-mediaSection-inner">
        <div class="container">
			<div class="ct-heading--main text-center">
			    <h3 class="text-uppercase ct-u-text--white">Login into your account</h3>
			</div>
        </div>
    </div>
</header>
@stop

@section('content')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="630" data-type="parallax" data-bg-image="assets/images/content/registration-parallax.jpg" data-bg-image-mobile="assets/images/content/registration-parallax.jpg">
    <div class="ct-mediaSection-inner">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="ct-headerText--normal">
                        <h2 class="text-uppercase ct-fw-600 ct-u-marginBottom70">
                            What will you
                            <span class="ct-u-text--motive">earn</span>
                            by registering?
                        </h2>
                    </div>
                    <div class="ct-iconBox ct-u-marginBottom40 ct-iconBox--2col">
                        <div class="ct-icon text-center ct-iconContainer--circle ct-iconContainer--circleHoverLight">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title text-uppercase ct-fw-600">Get more business</span>
                            <span class="ct-text">Thanks to our search you will get more investors drawn towards you.</span>
                        </div>
                    </div>
                    <div class="ct-iconBox ct-u-marginBottom40 ct-iconBox--2col">
                        <div class="ct-icon text-center ct-iconContainer--circle ct-iconContainer--circleHoverLight">
                            <i class="fa fa-bolt"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title text-uppercase ct-fw-600">Free business accont</span>
                            <span class="ct-text">By registering now you will get free business account for a year.</span>
                        </div>
                    </div>
                    <div class="ct-iconBox ct-iconBox--2col">
                        <div class="ct-icon text-center ct-iconContainer--circle ct-iconContainer--circleHoverLight">
                            <i class="fa fa-trophy"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title text-uppercase ct-fw-600">Reach your goal</span>
                            <span class="ct-text">VTalkBusiness was designed to become your business search engine. It’s not only a tool, it’s a working ecosystem.</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <form role="form" class="ct-formRegister pull-right" method="post" action="{{ url('/login') }}">
                        <div class="form-group">
                            <div class="ct-form--label--type2">
                                <div class="ct-u-displayTableVertical">
                                    <div class="ct-u-displayTableCell">
                                        <div class="ct-input-group-btn">
                                            <button class="btn btn-primary">
                                                <i class="fa fa-user"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="ct-u-displayTableCell text-center">
                                        <span class="text-uppercase">LOGIN TO ACCOUNT</span>
                                    </div>
                                </div>
                            </div>
                            @if($errors->any())
                                @foreach($errors->all() as $error)
                                <div class="errorMessage alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ $error }}
                                </div>
                                @endforeach
                            @endif
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="ct-form--item ct-u-marginBottom20 form-group">
                                <label>Your Email</label>
                                <input type="text" class="form-control input-lg" placeholder="Email" name="email" value="{{ old('email') }}" id="email">
                            </div>
                            <div class="ct-form--item ct-u-marginBottom20 form-group">
                                <label>Password</label>
                                <input type="password" class="form-control input-lg" name="password" id="password">
                            </div>
                             <div class="ct-form--item ct-u-marginBottom20">
                                <a href="{{ url('/forget-password') }}">Forgot Password ?</a>
                            </div>
                            <div class="ct-form--item">
                                <button type="submit" class="btn btn-warning center-block">Login</button>
                                <span class="center-block" style="color: white;text-align: center;">OR</span>
                                <a href="{{ url('/register') }}" class="btn btn-info center-block" style="width: 35%;">Sign up</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>
@stop

@section('scripts')

<script type="text/javascript">
$(function(){
	$('#email').on('blur', function(){
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!regex.test($(this).val()))
		{
		 	setError($(this), "Please provide a valid email address.");
		}
		else
		{
			unsetError($(this));
		}
	});


	$('#password').on('blur', function(){
		if($(this).val() == '')
		{
		 	setError($(this), "Password cannot be blank.");
		}
		else
		{
			unsetError($(this));
		}
	});
});
</script>
@stop