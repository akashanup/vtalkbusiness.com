@extends('master')

@section('title')
<title>Link Expired</title>
@stop

@section('page-header')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="140" data-type="parallax" data-bg-image="assets/images/content/agency-parallax.jpg" data-bg-image-mobile="assets/images/content/agency-parallax.jpg" style="min-height: 140px; height: 140px; background-image: url(http://vtalk.business/HTML/assets/images/content/agency-parallax.jpg); background-position: 50% 50%;">
    <div class="ct-mediaSection-inner">
        <div class="container">
			<div class="ct-heading--main text-center">
			    <h3 class="text-uppercase ct-u-text--white">Link Expired</h3>
			</div>
        </div>
    </div>
</header>
@stop

@section('content')
<section class="ct-u-paddingBoth70 ct-js-section text-left">
    <div class="container">
		@if($errors->any())
            @foreach($errors->all() as $error)
            <div class="errorMessage alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ $error }}
            </div>
            @endforeach
        @endif
		<h3>This link is expired.</h3>
    </div>
</section>
@stop
