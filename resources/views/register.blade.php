@extends('master')

@section('title')
<title>Register Now</title>
@stop

@section('breadcrumb')
<!-- BreadCrumbs -->
<div class="ct-site--map">
    <div class="container">
        <a href="{{ url() }}">Home</a>
        <a href="{{ url('/register') }}">Register</a>
    </div>
</div>
<!-- BreadCrumb Ends -->
@stop

@section('content')
<header class="ct-mediaSection" data-stellar-background-ratio="0.3" data-height="630" data-type="parallax" data-bg-image="assets/images/content/registration-parallax.jpg" data-bg-image-mobile="assets/images/content/registration-parallax.jpg">
    <div class="ct-mediaSection-inner">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="ct-headerText--normal">
                        <h2 class="text-uppercase ct-fw-600 ct-u-marginBottom70">
                            What will you
                            <span class="ct-u-text--motive">earn</span>
                            by registering?
                        </h2>
                    </div>
                    <div class="ct-iconBox ct-u-marginBottom40 ct-iconBox--2col">
                        <div class="ct-icon text-center ct-iconContainer--circle ct-iconContainer--circleHoverLight">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title text-uppercase ct-fw-600">Get more business</span>
                            <span class="ct-text">Thanks to our search you will get more investors drawn towards you.</span>
                        </div>
                    </div>
                    <div class="ct-iconBox ct-u-marginBottom40 ct-iconBox--2col">
                        <div class="ct-icon text-center ct-iconContainer--circle ct-iconContainer--circleHoverLight">
                            <i class="fa fa-bolt"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title text-uppercase ct-fw-600">Free business accont</span>
                            <span class="ct-text">By registering now you will get free business account for a year.</span>
                        </div>
                    </div>
                    <div class="ct-iconBox ct-iconBox--2col">
                        <div class="ct-icon text-center ct-iconContainer--circle ct-iconContainer--circleHoverLight">
                            <i class="fa fa-trophy"></i>
                        </div>
                        <div class="ct-iconBox--description">
                            <span class="ct-title text-uppercase ct-fw-600">Reach your goal</span>
                            <span class="ct-text">VTalkBusiness was designed to become your business search engine. It’s not only a tool, it’s a working ecosystem.</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <form role="form" class="ct-formRegister pull-right" method="post" action="{{ url('/register') }}">
                        <div class="form-group">
                            <div class="ct-form--label--type2">
                                <div class="ct-u-displayTableVertical">
                                    <div class="ct-u-displayTableCell">
                                        <div class="ct-input-group-btn">
                                            <button class="btn btn-primary">
                                                <i class="fa fa-user"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="ct-u-displayTableCell text-center">
                                        <span class="text-uppercase">Become a member</span>
                                    </div>
                                </div>
                            </div>
                            @if($errors->any())
                                @foreach($errors->all() as $error)
                                <div class="errorMessage alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ $error }}
                                </div>
                                @endforeach
                            @endif
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="ct-form--item ct-u-marginBottom20 form-group">
                                <label>Your Name</label>
                                <input type="text"  class="form-control input-lg" placeholder="Name" name="name" value="{{ old('name') }}">
                            </div>
                            <div class="ct-form--item ct-u-marginBottom20">
                                <label>Your Email</label>
                                <input type="text" class="form-control input-lg" placeholder="Email" name="email" value="{{ old('email') }}">
                            </div>
                            <div class="ct-form--item ct-u-marginBottom20">
                                <label>Password</label>
                                <input type="password" class="form-control input-lg" name="password">
                            </div>
                            <div class="ct-form--item ct-u-marginBottom20">
                                <label>Repeat Password</label>
                                <input type="password" class="form-control input-lg" name="password_confirmation">
                            </div>
                            <!-- <div class="ct-form--item ct-u-marginBottom20">
                                <label>Account Type</label>
                                <select class="ct-js-select ct-select-lg">
                                    <option value="month">Standard User</option>
                                    <option value="January">Premium User</option>
                                </select>
                            </div> -->
                            <div class="ct-form--item ct-u-marginBottom20">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="agree">
                                        I read and agree to<a href="terms.html"> terms & conditions</a>
                                    </label>
                                </div>
                            </div>
                            <div class="ct-form--item">
                                <button type="submit" class="btn btn-warning center-block">Register Now</button>
                                <span class="center-block" style="color: white; text-align: center;">Already a member?</span>
                                <a href="{{ url('/login') }}" class="btn btn-info center-block" style="width: 35%;">Login</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>
@stop

@section ('scripts')

@stop
