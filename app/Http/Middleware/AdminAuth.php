<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Session;
use App\Models\User;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Session::has('user_id')) {
            $user = User::find(Session::get('user_id'));
            if($user->role==4)
            {
                return $next($request);
            }
        }
        return redirect('/');
    }
}
