<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Contact;
use App\Models\Company;
use App\Models\Video;
use App\Models\InvestorProfile;
use App\Models\Enquiry;


class AdminController extends Controller
{
	public function __construct()
	{
		$this->middleware('isAdmin');
	}
    public function fetchAllCompanies()
    {
        $companies = Company::join('users', 'companies.user_id', '=', 'users.id')
                            ->select('companies.*', 'users.name AS users_name', 'users.email AS users_email')
                            ->where('companies.deleted', 0)
                            ->orderBy('companies.id', 'desc')->paginate(15);
        
        return view('admin.companies', compact(['companies']));
    }


    public function banCompany(Request $request)
    {
        $json = [];
        try {
            // dd($request->company_id);
            $company = Company::find($request->company_id);
            $company->ban = $request->status;
            $company->save();
            $json['status'] = 1;

            $json['message'] = ($request->status == 1)? "Company successfully banned.": "Company successfully unbanned.";
            
        } catch (Exception $e) {
            $json['status'] = 0;
            $json['message'] = $e->getMessage();
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }

    public function deleteCompany(Request $request)
    {
        $json = array();
        try {
            $company = Company::find($request->company_id);
            $company->deleted = 1;
            $company->save();
            $json['status'] = 1;
            $json['message'] = "Company deleted.";
        } catch (Exception $e) {
            $json['status'] = 0;
            $json['message'] = $e->getMessage();
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }

    public function addFeatured(Request $request)
    {
        $json = array();
        try {
            $company = Company::find($request->company_id);
            $company->featured = $request->status;
            $company->save();
            $json['status'] = 1;

            $json['message'] = ($request->status == 1)? "Company added to featured.": "Company added to unfeatured.";
        } catch (Exception $e) {
            $json['status'] = 0;
            $json['message'] = $e->getMessage();
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }
    
    public function fetchAllUsers()
    {
        $users = User::where('deleted', 0)->orderBy('id', 'DESC')->paginate(15);
        return view('admin.users', compact(['users']));
    }

    public function banUser(Request $request)
    {
        $json = [];
        try {
            // dd($request->user_id);
            $user = User::find($request->user_id);
            $user->ban = $request->status;
            $user->save();
            $json['status'] = 1;

            $json['message'] = ($request->status == 1)? "User successfully banned.": "User successfully unbanned.";
            
        } catch (Exception $e) {
            $json['status'] = 0;
            $json['message'] = $e->getMessage();
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }

    public function deleteUser(Request $request)
    {
        $json = array();
        try {
            $user = User::find($request->user_id);
            $user->deleted = 1;
            $user->save();
            $json['status'] = 1;
            $json['message'] = "User deleted.";
        } catch (Exception $e) {
            $json['status'] = 0;
            $json['message'] = $e->getMessage();
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }

    public function fetchAllContacts()
    {
        $contacts = Contact::orderBy('id', 'DESC')->paginate(15);
        return view('admin.contacts', compact(['contacts']));
    }
 
    public function fetchAllVideos()
    {
        $videos = Video::where('deleted', 0)->orderBy('id', 'DESC')->paginate(15);
        return view('admin.videos', compact(['videos']));
    }
    
    public function saveVideo(Request $data)
    {
        try {
            $video=new Video();
            $video->Title= $data->title;
            $video->URL= $data->url;
            $video->save();
            
        } catch (Exception $e) {
            
        }
    }

    public function deleteVideo(Request $request)
    {
        try {
            $video = Video::find($request->video_id);
            $video->deleted = 1;
            $video->save();
            $json['status'] = 1;
            $json['message'] = "Video deleted.";
        } catch (Exception $e) {
            $json['status'] = 0;
            $json['message'] = $e->getMessage();
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }

    public function fetchAllInvestors()
    {
        $investors = InvestorProfile::join('users', 'investor_profiles.user_id', '=', 'users.id')
                            ->select('investor_profiles.*', 'users.name AS users_name', 'users.email AS users_email')
                            ->where('investor_profiles.deleted', 0)
                            ->orderBy('investor_profiles.id', 'desc')->paginate(15);
        return view('admin.investors', compact(['investors']));
    }

    public function banInvestor(Request $request)
    {
        $json = [];
        try {
            // dd($request->investors_id);
            $investor = InvestorProfile::find($request->investor_id);
            $investor->ban = $request->status;
            $investor->save();
            $json['status'] = 1;

            $json['message'] = ($request->status == 1)? "Investor successfully banned.": "Investor successfully unbanned.";
            
        } catch (Exception $e) {
            $json['status'] = 0;
            $json['message'] = $e->getMessage();
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }

    public function deleteInvestor(Request $request)
    {
        $json = array();
        try {
            $investor = InvestorProfile::find($request->investor_id);
            $investor->deleted = 1;
            $investor->save();
            $json['status'] = 1;
            $json['message'] = "Investor deleted.";
        } catch (Exception $e) {
            $json['status'] = 0;
            $json['message'] = $e->getMessage();
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }

    public function fetchAllEnquiries()
    {
        try {
            $enquiries = Enquiry::orderBy('id', 'desc')->paginate(15);
            return view('admin.enquiries', compact(['enquiries']));
        }catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

}