<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Interest;
use App\Models\InterestRelation;
use Illuminate\Support\Facades\Session;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function interestedOrNot($entity_id, $entity_name)
    {
        $json = [];
        $json['current'] = 0;
    	try {
    		$entity = Interest::where('interested_table', $entity_name)->first();
            $json['entity_type'] = $entity->id;
    		$interest = InterestRelation::where('user_id', Session::get('user_id'))->where('entity_id', $entity_id)->where('entity_type', $entity->id)->first();
    		if($interest) {
                $json['current'] = 1;
    		}
    	}catch(Exception $e) {
    		$json['message'] = 'Something went wrong';
    	}
    	return $json;
    }

    public function tweakImage($image, $height, $width)
    {
        $h = $image->height();
        $w = $image->width();
        $ar = $w/$h;
        if($h > $w) {
            return false;
            /*$iw = $ar * $height;
            $image->resize((int)$iw, (int)$height);*/
        }else if($w > $h) {
            $ih = $width/$ar;
            $image->resize((int)$width, (int)$ih);
        }else {
            $image->resize($width, $height);
        }
        return $image;
    }
}
