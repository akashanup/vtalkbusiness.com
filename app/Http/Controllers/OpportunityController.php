<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use App\Models\User;
use App\Models\Opportunity;
use App\Models\OpportunityTagsRelationship;
use App\Models\OpportunityCountriesRelationship;
use App\Models\OpportunityCompanyRelationship; 
use App\Models\Country;
use App\Models\Tags;
use App\Models\Company;
use App\Http\Requests\OpportunityRequest;

class OpportunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('isUser');
    }

    public function getOpportunity()
    {
        $country    =   Country::all();
        $tags       =   Tags::all();
        $company    =   Company::where('user_id', Session::get('user_id'))->get();
        return view('opportunity.postOpportunity')->with([
                'tags'          =>  $tags,
                'countries'     =>  $country,
                'companies'     =>  $company,
            ]);
    }

    public function postOpportunity(OpportunityRequest $request)
    {
        try 
        {
            if(Session::has('user_id'))
            {   $user                       =   Session::get('user_id');
                $opportunity                =   new Opportunity();
                $opportunity->headline      =   $request->headline;
                $opportunity->description   =   $request->description;
                $opportunity->capital       =   $request->capital;
                $opportunity->revenue       =   $request->revenue;
                $opportunity->status        =   0;
                $opportunity->user_id       =   $user;
                if($request->logo->getMimeType() == 'image/png' || $request->logo->getMimeType() == 'image/jpeg' || $request->logo->getMimeType() == 'image/gif') {
                    $location = env('LOCATION').'public/images';
                    $ext = $request->logo->guessExtension();
                    $name = "logo_$user"."_".time().".$ext";
                    $url = url()."/images/$name";
                    $image = Image::make($request->logo)->resize(280, 280)->save("images/{$name}");
                    //$request->logo->move($location, $name);
                    $opportunity->logo = $url;
                }
                else {
                    $errors[] = 'Opportunity logo should be a image';
                }
                $opportunity->save();

                for($a = 0; $a < count($request->companies); $a++) {
                    $company                    = new OpportunityCompanyRelationship();
                    $company->opportunity_id    = $opportunity->id;
                    $company->company_id        = $request->companies[$a];
                    $company->save();
                }
                for($i = 0; $i < count($request->tags); $i++) {
                    $tag                    = new OpportunityTagsRelationship();
                    $tag->opportunity_id    = $opportunity->id;
                    $tag->tags              = $request->tags[$i];
                    $tag->save();
                }
                for($i = 0; $i < count($request->country); $i++) {
                    $country                    = new OpportunityCountriesRelationship();
                    $country->opportunity_id    = $opportunity->id;
                    $country->country_id        = $request->country[$i];
                    $country->save();
                }
            }
            return redirect('/make-payment/'.$opportunity->id);
        } 
        catch (Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');   
        }
    }

    public function getPayment($id)
    {
        return view('opportunity.payment')->with('opportunity_id',$id);
    }

    
    public function postPayment($id)
    {
        try {
            $opportunity = Opportunity::where('id',$id)->first()->UPDATE(array('status'=>1));
            return redirect('/');
           
        } catch (Exception $e) {
            
        }

    }

    
    public function showOpportunities()
    {
        $opportunity = Opportunity::JOIN('opportunity_company_relationships','opportunities.id','=','opportunity_company_relationships.opportunity_id')->
                                    JOIN('companies','opportunity_company_relationships.company_id','=','companies.id')->
                                    //JOIN('opportunity_countries_relationships','opportunities.id','=','opportunity_countries_relationships.opportunity_id')->
                                    //JOIN('countries','opportunity_countries_relationships.country_id','=','countries.id')->
                                    //SELECT('opportunities.*','companies.name AS company_name','countries.country AS country')->
                                    SELECT(['opportunities.id','opportunities.headline','companies.name AS company_name', 'companies.id AS company_id'])->
                                    get();
        return view('opportunity.showOpportunities')->with('opportunities',$opportunity);
    }

    
    public function detailsOpportunity($id)
    {
        try {
            /*$opportunity = Opportunity::where('opportunities.id',$id)->
                                JOIN('opportunity_company_relationships','opportunities.id','=','opportunity_company_relationships.opportunity_id')->
                                JOIN('companies','opportunity_company_relationships.company_id','=','companies.id')->
                                JOIN('opportunity_countries_relationships','opportunities.id','=','opportunity_countries_relationships.opportunity_id')->
                                JOIN('countries','opportunity_countries_relationships.country_id','=','countries.id')->
                                SELECT('opportunities.*','companies.name AS company_name','countries.country AS country')->
                                get();
            return $opportunity;*/
            $opportunity    = Opportunity::WHERE('id',$id)->first();
            $company        = OpportunityCompanyRelationship::where('opportunity_company_relationships.opportunity_id',$opportunity->id)->
                                JOIN('companies','opportunity_company_relationships.company_id','=','companies.id')->
                                SELECT(['companies.name AS company_name', 'companies.id AS company_id'])->get();
            $country        = OpportunityCountriesRelationship::where('opportunity_countries_relationships.opportunity_id',$opportunity->id)->
                                JOIN('countries','opportunity_countries_relationships.country_id','=','countries.id')->
                                SELECT('countries.country AS country')->get();
            $tags           = OpportunityTagsRelationship::where('opportunity_tags_relationships.opportunity_id',$opportunity->id)->
                                JOIN('tags','opportunity_tags_relationships.tags','=','tags.id')->
                                SELECT('tags.tags AS tags')->get();

            $interested = $this->interestedOrNot($opportunity->id, 'opportunities');

            
            $data                   =   [];
            $data['opportunity']    =   $opportunity;
            $data['company']        =   $company;
            $data['country']        =   $country;
            $data['tags']           =   $tags;
            $data['interested']     =   $interested;
            
            return response()->json($data)->header('Content-Type', 'application/json');
        } 
        catch (Exception $e) {
            
        }
    }

    public function myOpportunities()
    {
        try {
            $opportunities = Opportunity::where('user_id', Session::get('user_id'))->get();
            return view('opportunity.myOpportunities', compact(['opportunities']));
        }catch(Exception $e) {
            return redirect('/dashboard');
        }
    }
}
