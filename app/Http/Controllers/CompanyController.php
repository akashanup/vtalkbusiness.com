<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\User;
use App\Models\Company;
use App\Models\Address;
use App\Models\Tags;
use App\Models\Category;
use App\Models\Country;
use App\Models\CompanyTagsRelationship;
use App\Models\TeamMember;
use App\Models\CompanyTurnover;
use App\Models\CompanyProduct;
use App\Models\CategoryRelation;
use App\Models\Currency;
use App\Models\Opportunity;
use App\Models\OpportunityTagsRelationship;
use App\Models\OpportunityCountriesRelationship;
use App\Models\OpportunityCompanyRelationship; 
use App\Models\RegistrationNoType;
use App\Http\Requests\ContactRequest;
use App\Http\Requests\EditCompanyRequest;
use App\Http\Requests\CompanyStepOneRequest;
use App\Http\Requests\CompanyStepTwoRequest;
use App\Http\Requests\CompanyStepThreeRequest;
use App\Http\Requests\CompanyStepFourRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;



class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('isUser', ['except' => ['findCompany', 'showAllCompany', 'getCompany','showCompany']]);
    }

    public function index()
    {
        if(Session::has('user_id'))
        {
            $tags   = Tags::all();
            $categories = Category::all();
            return view('company.registration', compact(['tags', 'categories']));
        }
        else
            return redirect('dashboard');
    }

    public function myCompanies()
    {
        try {
            $company=Company::where('user_id', Session::get('user_id'))->get();
            return view('company.myCompanies', compact(['company']));
        }catch(Exception $e) {
            return redirect('/dashboard');
        }
    }

    public function step1()
    {
        $currency       =   Currency::all();
        $regType        =   RegistrationNoType::all();
        return view('company.step1')->with([
            'currency'  =>  $currency,
            'regType'   =>  $regType,
            ]);
    }

    public function saveStep1(CompanyStepOneRequest $request)
    {
        try {
            $company = Company::where('email', $request->email)->first();
            if($company) {
                return redirect()->back()->withErrors('Another Company With This Email Is already registered. If you think this is an error then please write to us on support@vtalkbusiness.com');
            }
            $company = Company::where('registration_type', $request->registration_type)->where('company_cin', $request->company_cin)->first();
            if($company) {
                return redirect()->back()->withErrors('Another Company With this CIN Is already registered. If you think this is an error then please write to us on support@vtalkbusiness.com');
            }

            $company                            =   new Company();
            $company->name                      =   $request->name;
            $company->email                     =   $request->email;
            $company->registration_type         =   $request->registration_type;
            $company->company_cin               =   $request->company_cin;
            $company->phone                     =   $request->phone;
            $company->website                   =   $request->website;
            $company->evaluation_currency_type  =   3;
            $company->evaluation                =   $request->evaluation;
            $company->tagline                   =   $request->tagline;
            $company->user_id                   =   Session::get('user_id');
            $company->logo                      =   url().'/images/default_company_logo.jpg';
            $company->cover                     =   url().'/images/default_company_cover.jpg';
            $company->save();

            $tags       = Tags::all();
            $categories = Category::all();
            $country    = Country::all();
            Session::put('company_id', $company->id);
            return response()->view('company.step2', ['tags' => $tags, 'categories' => $categories, 'country' => $country])->header('Content-Type', 'text/html');

        }catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');
        }
    }

    public function saveStep2(CompanyStepTwoRequest $request)
    {
        try {
            $errors = [];
            $company = Company::find($request->company_id);
            if($request->logo) {
                if($request->logo->getMimeType() == 'image/png' || $request->logo->getMimeType() == 'image/jpeg' || $request->logo->getMimeType() == 'image/gif') {
                    $location = env('LOCATION').'public/images';
                    $ext = $request->logo->guessExtension();
                    $name = "logo_$company->id_".time().".$ext";
                    $url = url()."/images/$name";
                    $image = Image::make($request->logo);
                    $image = $this->tweakImage($image, 280, 280);
                    if($image) {
                        $image->save("images/{$name}");
                        $company->logo = $url;
                    }else {
                        $company->logo = url('/images/default_company_logo.jpg');
                        $errors[] = 'Company logo height should not be greater than width. Please select a different logo afterwards';    
                    }
                    //$request->logo->move($location, $name);
                }else {
                    $company->logo = url('/images/default_company_logo.jpg');
                    $errors[] = 'Company logo should be a image';
                }
            }else {
                $company->logo = url('/images/default_company_logo.jpg');
            }
            if($request->cover) {
                if($request->cover->getMimeType() == 'image/png' || $request->cover->getMimeType() == 'image/jpeg' || $request->cover->getMimeType() == 'image/gif') {
                    $location = env('LOCATION').'public/images';
                    $ext = $request->cover->guessExtension();
                    $name = "cover_$company->id_".time().".$ext";
                    $url = url()."/images/$name";
                    $image = Image::make($request->cover)->crop((int)$request->w, (int)$request->h, (int)$request->x, (int)$request->y)->save("images/$name");
                    $company->cover = $url;
                }else {
                    $errors[] = 'Company cover should be a image';
                }
            }else {
                $company->cover = url('/images/default_company_cover.jpg');
            }

            $company->save();

            //saving categories
            for($i = 0; $i < count($request->category); $i++) {
                $category = new CategoryRelation();
                $category->category_id = $request->category[$i];
                $category->company_id = $company->id;
                $category->save();
            }

            //saving tags
            for($i = 0; $i < count($request->tags); $i++) {
                $tag = new CompanyTagsRelationship();
                $tag->tags = $request->tags[$i];
                $tag->company_id = $company->id;
                $tag->save();
            }

            //saving addresses
            for($i = 0; $i < count($request->street); $i++) {
                if($request->country[$i] == '') {
                    continue;
                }
                $address = new Address();
                $address->street = $request->street[$i];
                $address->city = $request->city[$i];
                $address->state = $request->state[$i];
                $address->pin = $request->pin[$i];
                $address->country = $request->country[$i];
                $address->company_id = $company->id;
                $address->save();
            }

            //saving turnover
            foreach($request->year as $key => $d) {
                if($request->turnover[$key] == '') {
                    continue;
                }
                $turnover = new CompanyTurnover();
                $turnover->year = $request->year[$key];
                $turnover->turnover = $request->turnover[$key];
                $turnover->company_id = $company->id;
                $turnover->save();
            }

            if(count($errors)) {
                return view('company.step3')->withErrors($errors);
            }else {
                return view('company.step3');
            }
        }catch(Exception $e) {
            return view('company.step2')->withErrors('Something went wrong.');
        }
    }

    public function saveStep3(CompanyStepThreeRequest $request)
    {
        try {
            $company = Company::find($request->company_id);

            //saving designation
            $company->designation = $request->designation;
            $company->save();

            
            //saving products
            for($i = 0; $i < count($request->products); $i++) {
                if($request->products[$i] == '') {
                    continue;
                }
                $product = new CompanyProduct();
                $product->product_name = $request->products[$i];
                $product->product_information = $request->info[$i];
                $product->company_id = $company->id;
                $product->save();
            }

            //saving members
            for($i = 0; $i < count($request->names); $i++) {
                if($request->names[$i] == '') {
                    continue;
                }
                $member = new TeamMember();
                $member->name = $request->names[$i];
                $member->email = $request->emails[$i];
                $member->phone = $request->phones[$i];
                $member->designation = $request->designations[$i];
                $member->company_id = $company->id;
                $member->save();
            }

            $user                               = User::WHERE('id', Session::get('user_id'))->first();
            $member                             = new TeamMember();
            $member->name                       = $user->name;
            $member->email                      = $user->email;
            $member->phone                      = $company->phone;
            $member->designation                = $request->designation;
            $member->company_id                 = $company->id;
            $member->save();


            return view('company.step4');

        }catch(Exception $e) {
            return view('company.step3')->withErrors('Something went wrong.');
        }
    }

    public function saveStep4(CompanyStepFourRequest $request)
    {
        try {
            $company                = Company::find($request->company_id);
            $company->employee      = $request->employee;
            $company->facebook      = $request->facebook;
            $company->linkedin      = $request->linkedin;
            $company->twitter       = $request->twitter;
            $company->description   = $request->description;
            $company->save();

            return redirect('/dashboard');

        }catch(Exception $e) {
            return view('company.step4')->withErrors('Something went wrong.');
        }
    }

    public function saveCompany(EditCompanyRequest  $data,$id)
    {
        try {
            $company    =   Company::where('id',$id)->first()
                            ->  UPDATE(array(
                                'name'           =>  $data->name,
                                'email'          =>  $data->email,
                                'phone'          =>  $data->phone,
                                'employee'       =>  $data->employee,
                                'designation'    =>  $data->designation,
                                'evaluation'     =>  $data->evaluation
                            ));
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');
        }

        try {
            for ($indx = 0 ; $indx < count($data->city); $indx ++) {
                if ($data->country[$indx]==''&&$data->state[$indx]==''&&$data->city[$indx]==''&&$data->street[$indx]=='') {
                    continue;
                }
                $address                =   new Address();
                $address->company_id    =   $id;
                $address->country       =   $data->country[$indx];
                $address->state         =   $data->state[$indx];
                $address->city          =   $data->city[$indx];
                $address->pin           =   $data->pincode[$indx];
                $address->street        =   $data->street[$indx];
                $address->save();
            }
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');   
        }
    }

    public function editCompany(EditCompanyRequest  $data,$id)
    {
        try {
            $company    =   Company::where('id',$id)->first()
                            ->  UPDATE(array(
                                'name'           =>  $data->name,
                                'email'          =>  $data->email,
                                'phone'          =>  $data->phone,
                                'evaluation'     =>  $data->evaluation,
                                'company_cin'    =>  $data->cin,
                                'facebook'       =>  $data->facebook,
                                'twitter'        =>  $data->twitter,
                                'linkedin'       =>  $data->linkedin
                            ));
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');
        }
        return redirect('/company/'.$id);
    }

    public function showCompany($id)
    {
        if(!(Session::has('user_id')))
        {
            return redirect()->back()->withErrors('Please Login To View Company Details.');
        }
        $company=Company::find($id);
        if($company)
        {
            $company_id     =   $company->id;
            $tags           =   Tags::all();
            $leadership     =   TeamMember::where('company_id',$company_id)->get();
            $turnover       =   CompanyTurnover::where('company_id', $company_id)->orderBy('id', 'desc')->orderBy('year', 'desc')->limit(3)->get();
            $address        =   Address::where('company_id',$company_id)->get();
            $product        =   CompanyProduct::where('company_id',$company_id)->get();
            $tmp            =   CompanyTagsRelationship::where('company_id',$company_id)->get();
            $user           =   User::find($company->user_id);
            $currentUser    =   User::find(Session::get('user_id'));
            $companyTags    =   [];
            foreach($tmp as $tag) {
                $companyTags[]  = $tag->tags;
            }
            $user               =   User::where('users.id', $company->user_id)->join('user_profiles', 'user_profiles.user_id', '=', 'users.id')->first();
            return view('company.companyDetails')->with([
                'company'       =>  $company,
                'address'       =>  $address,
                'leadership'    =>  $leadership,
                'turnover'      =>  $turnover,
                'product'       =>  $product,
                'companyTags'   =>  $companyTags,
                'user'          =>  $user,
                'tags'          =>  $tags,
                'currentUser'   =>  $currentUser,
                'interested'    =>  $this->interestedOrNot($company->id, 'companies')
            ]);
        }
        else
        {
            return redirect('dashboard');
        }
    }

    public function editTurnover(Request $request, $id)
    {
        try {
                CompanyTurnover::where('company_id', $id)->delete();
                foreach($request->year as $key => $d) {
                    if($request->turnover[$key] == '') {
                        continue;
                    }
                    $turnover = new CompanyTurnover();
                    $turnover->year = $request->year[$key];
                    $turnover->turnover = $request->turnover[$key];
                    $turnover->company_id = $id;
                    $turnover->save();
                }
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');
        }   
        return redirect('/company/'.$id);                         
    }

    public function editAddress(Request $data, $id)
    {
        try{
            $address    =   Address::where('id', $id)
                            ->UPDATE(array(
                                'country'    =>  $data->country,
                                'state'      =>  $data->state,
                                'city'       =>  $data->city,    
                                'pin'        =>  $data->pincode,    
                                'street'     =>  $data->street,    
                            ));    
        }
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');
        }        
    }

    public function editTags(Request $request, $id)
    {
        try {
            CompanyTagsRelationship::where('company_id', $id)->delete();
            for($i = 0; $i < count($request->tags); $i++) {
                $tag = new CompanyTagsRelationship();
                $tag->tags = $request->tags[$i];
                $tag->company_id = $id;
                $tag->save();
            }    
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');   
        }
        return redirect('/company/'.$id);
    }

    public function saveEditedTurnover(Request $data,$id)
    {
        try {
            CompanyTurnover::where('company_id', $id)->delete();
            foreach($data->year as $key => $d) {
                $turnover               =   new CompanyTurnover();
                $turnover->year         =   $data->year[$key];
                $turnover->turnover     =   $data->turnover[$key];
                $turnover->company_id   =   $id;
                $turnover->save();
            }
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');
        }
    }

    public function editTeam(Request $request,$id)
    {
        try {
            TeamMember::where('company_id',$id)->delete();    
            for($i = 0; $i < count($request->names); $i++) {
                if($request->names[$i] == '') {
                    continue;
                }
                $member = new TeamMember();
                $member->name = $request->names[$i];
                $member->email = $request->emails[$i];
                $member->phone = $request->phones[$i];
                $member->designation = $request->designations[$i];
                $member->company_id = $id;
                $member->save();
            }
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');
        }
        return redirect('/company/'.$id);
    }

    public function saveProduct(Request $data,$id)
    {
       try {
            //delete products and then add again
            CompanyProduct::where('company_id', $id)->delete();
            foreach($data->productName as $key => $name) {
                if($data->productName[$key] == '' && $data->productInformation[$key] == '')
                    continue;
                $product                        =   new CompanyProduct();
                $product->company_id            =   $id;
                $product->product_name          =   $data->productName[$key];
                $product->product_information   =   $data->productInformation[$key]; 
                $product->save();
            }
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');
        }
    }

    public function addMemeber($company_id)
    {
        $members = TeamMember::where('company_id', $company_id)->get();
        $company = Company::find($company_id);
        return view('company.members', compact(['members', 'company']));
    }

    public function saveMemeber(Request $request, $company_id)
    {
        #Validation through request class pending
        if(count($request->name)) {
            try {
                foreach($request->name as $key => $name) {
                    $member                 =   new TeamMember();
                    $member->name           =   $name;
                    $member->email          =   $request->email[$key];
                    $member->phone          =   $request->phone[$key];
                    $member->designation    =   $request->designation[$key];
                    $member->company_id     =   $company_id;
                    $member->save();
                }
                return redirect('dashboard');
            }catch(Exception $e) {
                return redirect()->back()->withErrors('Something went wrong.');
            }
        }else {
            return redirect()->back()->withErrors('Please add at least one member');
        }
    }

    public function findCompany(Request $data)
    {
        $json = [];
        try 
        {
            $data->category         =  explode(',', $data->category);
            $data->city             =  explode(',', $data->city);
            $data->country          =  explode(',', $data->country);
            $data->tags             =  explode(',', $data->tags);

            $opportunities          = [];
            $companies              = [];
            $tags                   = [];
            $opportunityTags        = [];
            $city                   = [];
            $country                = [];
            $opportunityCountry     = [];
            $name                   = [];
            $opportunityName        = [];
            $category               = [];
            $relatedCompanies       = [];
            $relatedOpportunities   = [];

            //now searching by min and max price or evaluation

            if(!is_numeric(intval($data->minPrice,10))) 
            {
                $data->minPrice             =   100000;
            }

            if(!is_numeric(intval($data->maxPrice,10)))
            {
                $data->maxPrice             =   1000000;
            }

            $company                        =   Company::whereBetween('evaluation', [(intval($data->minPrice,10)), (intval($data->maxPrice,10))])
                                                        ->select('id')->get();

            foreach ($company as $key => $value) {
                $relatedCompanies[]         =   $value->id;
                //$companies[]                =   $value->id;
            }

            $opportunity                    =   Opportunity::whereBetween('capital', [(intval($data->minPrice,10)), (intval($data->maxPrice,10))])
                                                            ->select('id')->get();
            foreach ($opportunity as $key => $value) {
                $relatedOpportunities[]     =   $value->id;
                //$opportunities[]            =   $value->id;
            }


            //now searching by tags

            if(!empty($data->tags[0]))
            {
                $company_tags               =   CompanyTagsRelationship::select('company_id')->whereIn('tags',$data->tags)->get();
                foreach ($company_tags as $key => $value) {
                    //$relatedCompanies[]     =   $value->company_id;
                    $tags[]                 =   $value->company_id;
                }
                //$companies                  =   array_intersect($companies, $tags);
                $relatedCompanies           =   array_intersect($relatedCompanies, $tags);

                $opportunity_tags           =   OpportunityTagsRelationship::select('opportunity_id')->whereIn('tags',$data->tags)->get();
                foreach ($opportunity_tags as $key => $value) {
                    //$relatedOpportunities[] =   $value->opportunity_id;
                    $opportunityTags[]      =   $value->opportunity_id;
                }
                //$opportunities              =   array_intersect($opportunities, $opportunityTags);                
                $relatedOpportunities       =   array_intersect($relatedOpportunities, $opportunityTags);                
            }
                

            //now searching by country

            if(!empty($data->country[0]))
            {
                $company_country            =   Address::select('company_id')->whereIn('country', $data->country)->get();
                foreach ($company_country as $key => $value) {
                    //$relatedCompanies[]     =   $value->company_id;
                    $country[]              =   $value->company_id;
                }
                //$companies                  =   array_intersect($companies, $country);
                $relatedCompanies             =   array_intersect($relatedCompanies, $country);

                $opportunity_country        =   Country::WHERE('country',$data->country)
                                                        ->JOIN('opportunity_countries_relationships','countries.id','=','opportunity_countries_relationships.country_id')
                                                        ->SELECT('opportunity_countries_relationships.opportunity_id')
                                                        ->get();
                foreach ($opportunity_country as $key => $value) {
                    //$relatedOpportunities[] =   $value->opportunity_id;
                    $opportunityCountry[]   =   $value->opportunity_id;
                }
                //$opportunities              =   array_intersect($opportunities, $opportunityCountry);
                $relatedOpportunities       =   array_intersect($relatedOpportunities, $opportunityCountry);
            }
            

            //now searching by name

            if(!empty($data->name))
            {
                /*$company_id_name            =   Company::select('id')->where('name',$data->name)->get();
                foreach ($company_id_name as $key => $value) {
                    $name[]                 =   $value->id;
                }
                $companies=array_intersect($companies, $name);*/

                
                $company_id_name            =   Company::select('id')->where('name', 'LIKE', '%'.$data->name.'%')->get();
                foreach ($company_id_name as $key => $value) {
                    //$relatedCompanies[]     =   $value->id;
                    $name[]                 =   $value->id;
                }
                $relatedCompanies           =   array_intersect($relatedCompanies, $name);

                /*$opportunity_id_name        =   Company::where('name',$data->name)
                                                        ->JOIN('opportunity_company_relationships','companies.id','=','opportunity_company_relationships.company_id')
                                                        ->SELECT('opportunity_company_relationships.opportunity_id')
                                                        ->get();
                foreach ($opportunity_id_name as $key => $value) {
                    $opportunityName[]      =   $value->id;
                }
                
                $opportunities              =   array_intersect($opportunities, $opportunityName);*/
                
                $opportunity_id_name        =   Company::where('name', 'LIKE', '%'.$data->name.'%')
                                                        ->JOIN('opportunity_company_relationships','companies.id','=','opportunity_company_relationships.company_id')
                                                        ->SELECT('opportunity_company_relationships.opportunity_id')
                                                        ->get();
                foreach ($opportunity_id_name as $key => $value) {
                    $opportunityName[]      =   $value->id;
                }
                //$opportunities              =   array_intersect($opportunities, $opportunityName);
                $relatedOpportunities       =   array_intersect($relatedOpportunities, $opportunityName);
            }


            //now searching by category

            if(!empty($data->category[0]))
            {
                $company_category           =   CategoryRelation::select('company_id')->whereIn('category_id', $data->category)->get();
                foreach ($company_category as $key => $value) {
                    //$relatedCompanies[]     =   $value->company_id;
                    $category[]             =   $value->company_id;
                }
                //$companies                  =   array_intersect($companies, $category);
                $relatedCompanies           =   array_intersect($relatedCompanies, $category);
            }

            //now optimizing search

            if(empty($companies))
            {
                $companies                  =   $relatedCompanies;
                $json['related']            =   true;

            }

            if(empty($opportunities))
            {
                $opportunities                  =   $relatedOpportunities;
                $json['relatedOpportunities']   =   true;
            }

            //$companies[]=array_intersect($tags, $city, $country, $name, $category);

            $company    = Company::whereIn('companies.id', $companies)
                            ->JOIN('users', 'companies.user_id', '=', 'users.id')
                            ->select('companies.*', 'users.name AS users_name', 'users.id AS users_id')
                            ->orderBy('companies.name')->get();

            foreach($company as $k => $c) {
                $co = Country::getCountries($c->id);
                if($co == '') {
                    $co = '&nbsp;';
                }
                $company[$k]->countries = $co;
            }

            $opportunity = Opportunity::whereIn('opportunities.id',$opportunities)
                            ->JOIN('users', 'opportunities.user_id', '=', 'users.id')
                            ->select('opportunities.*', 'users.name AS users_name', 'users.id AS users_id')
                            ->get();
            foreach($opportunity as $k => $o) {
                $co = Opportunity::getCompanies($o->id);
                if($co == '') {
                    $co = '&nbsp;';
                }
                $opportunity[$k]->companies = $co;
            }
            //$company[count($company)]="RelatedCompanies";
            $json['companies']      =   $company;
            $json['opportunities']  =   $opportunity;
            $json['status']         =   1;
        } 
        catch (Exception $e) 
        {
            $json['status'] = 0;
            $json['message'] = 'Something went wrong';
            $json['exception'] = $e->getMessage().' on CompanyController@findCompany';
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }

    public function getCompany()
    {
        $categories = Category::all();
        $tags = Tags::all();
        $countries = Country::select('countries.country as country','countries.id as countryId')->distinct()->get();
        return view('company.getCompany', compact(['categories', 'tags', 'cities', 'countries']));
    }

    public function showAllCompany()
    {
        $companies = Company::JOIN('users', 'companies.user_id', '=', 'users.id')
                        ->select('companies.*', 'users.name AS users_name', 'users.id AS users_id')
                        ->orderBy('companies.name')->paginate(16);
        foreach($companies as $k => $c) {
            $co = Country::getCountries($c->id);
            if($co == '') {
                $co = '&nbsp;';
            }
            $companies[$k]->countries = $co;
        }
        
        return view('company.showCompanies')->with('companies',$companies);
    }

    public function saveProfilePicture(Request $request, $id)
    {
        try 
        {
            $company  = Company::find($id);
            //$name     = substr($company->logo,strpos($company->logo,"images")+7);
            //$company->logo->delete($name);
            if($request->image->getMimeType() == 'image/png' || $request->image->getMimeType() == 'image/jpeg' || $request->image->getMimeType() == 'image/gif') 
            {
                $location = env('LOCATION').'public/images';
                $ext = $request->image->guessExtension();
                $name = "logo_{$company->id}_".time().".{$ext}";
                $url = url()."/images/$name";
                $image = Image::make($request->image);
                $image = $this->tweakImage($image, 280, 280);
                if($image) 
                {
                    $image->save("images/{$name}");
                    //$request->image->move($location, $name);
                    $company->logo = $url;
                    $company->save();
                }
                else 
                {
                    return redirect()->back()->withErrors('Image height should be smaler than image width.');
                }
            }
            else 
            {
                return redirect()->back()->withErrors('Image type not supported. All other details have been saved');
            }
            return redirect('/company/'.$company->id);
        }
        catch(Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong.');
        }
    }

    public function saveCoverImage(Request $request, $id)
    {
        try 
        {
            $company  = Company::find($id);
            if($request->image->getMimeType() == 'image/png' || $request->image->getMimeType() == 'image/jpeg' || $request->image->getMimeType() == 'image/gif') {
                $location = env('LOCATION').'public/images';
                $ext = $request->image->guessExtension();
                $name = "cover_$company->id_".time().".$ext";
                $url = url()."/images/$name";
                $image = Image::make($request->image)->crop((int)$request->w, (int)$request->h, (int)$request->x, (int)$request->y)->save("images/$name");
                $company->cover = $url;
                $company->save();
            }else {
                return redirect()->back()->withErrors('Image type not supported. All other details have been saved');
            }
            return redirect('/company/'.$company->id);
        }
        catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong.');
        }
    }

}