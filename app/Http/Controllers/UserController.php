<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\Company;
use App\Models\InvestorProfile;
use App\Models\User;
use App\Models\Opportunity;
use App\Models\InterestRelation;
use App\Http\Requests\ChangePassword;
use Illuminate\Support\Facades\Hash;
use App\Models\UserProfile;
use App\Http\Requests\UserProfileRequest;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
	public function __construct()
	{
		$this->middleware('isUser');
	}

    public function dashboard()
    {
        $user_id=Session::get('user_id');
        $graphData = [];
        $graphData['total_companies'] = Company::where('user_id', Session::get('user_id'))->count();
        $graphData['total_opportunities'] = Opportunity::where('user_id', Session::get('user_id'))->count();
        $graphData['total_interests'] = InterestRelation::ownInterest()->count();
        $graphData['ointerest'] = [];
        $graphData['cinterest'] = [];
        $graphData['total_cinterest'] = 0;
        $graphData['total_ointerest'] = 0;
        $tmp = Opportunity::where('user_id', Session::get('user_id'))->get(['id', 'headline']);
        foreach($tmp as $op) {
            $t = new \stdClass();
            $t->value = InterestRelation::opportunityInterest($op->id)->count();
            $t->label = $op->headline;
            $t->color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            $graphData['ointerest'][] = $t;
            if($t->value)
                $graphData['total_ointerest']++;
        }
        $tmp = Company::where('user_id', Session::get('user_id'))->where('deleted', 0)->get(['id', 'name']);
        foreach($tmp as $op) {
            $t = new \stdClass();
            $t->value = InterestRelation::companyInterest($op->id)->count();
            $t->label = $op->name;
            $t->color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            $graphData['cinterest'][] = $t;
            if($t->value)
                $graphData['total_cinterest']++;
        }
        $graphData['ointerest'] = json_encode($graphData['ointerest']);
        $graphData['cinterest'] = json_encode($graphData['cinterest']);
        return view('dashboard', compact(['graphData']));
    }

    public function userChangePassword()
    {
        try {
            $user_id=Session::get('user_id');
            if($user_id)
            {
                $user = User::where('id',$user_id)->get()->first();
                return view('changePassword')->with('user_id',$user->id);
            }
            return redirect('/');
        }
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');   
        }
    }

    public function userSaveChangePassword(ChangePassword $data)
    {
        try {
            $user_id    =   Session::get('user_id');
            $user       =   User::where('id',$user_id)->get()->first();
            if($user)
            {
                if(Hash::check($data->oldPassword, $user->password))
                {
                    $user->UPDATE(array(
                                'password'  => Hash::make($data->newPassword)
                            ));
                }
                else
                {
                    return redirect()->back()->withErrors('Old Password Incorrect');
                }
            }
            else
            {
                return redirect()->back()->withErrors('User not found');
            }
            return redirect('/dashboard');
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');   
        }
     }
        
    
    public function user_profile()
    {
        try{
            $user = User::find(Session::get('user_id'));
            $user_profile = UserProfile::where('user_id',Session::get('user_id'))->first();
            return view('user_profile' ,compact(['user','user_profile']));
        }
        catch(Exception $e)
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again');            
        } 
    }

    public function saveUser(Request $request)
    {
        try{
            $userProfile = UserProfile::where('user_id', Session::get('user_id'))->first();
            if(!$userProfile) {
                $userProfile = new UserProfile();
            }
            $userProfile->designation = $request->designation;
            $userProfile->phone = $request->phone;
            $userProfile->mobile = $request->mobile;
            $userProfile->skype = $request->skype;
            $userProfile->facebook = $request->facebook;
            $userProfile->instagram = $request->instagram;
            $userProfile->linkedin = $request->linkedin;
            $userProfile->twitter = $request->twitter;
            $userProfile->user_id = Session::get('user_id');
            $userProfile->save();
            return redirect('/user_profile');
        }
        catch(Exception $e)
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again');  
        }
    }

    public function upload(Request $request)
    {
        try {
            $userProfile = UserProfile::where('user_id', Session::get('user_id'))->first();
            if($request->image->getMimeType() == 'image/png' || $request->image->getMimeType() == 'image/jpeg' || $request->image->getMimeType() == 'image/gif') {
                $location = env('LOCATION').'public/images';
                $ext = $request->image->guessExtension();
                $name = "user_{$userProfile->id}_".time().".{$ext}";
                $url = url()."/images/$name";
                $image = Image::make($request->image);
                $image = $this->tweakImage($image, 280, 280);
                $image->save("images/{$name}");
                //$request->image->move($location, $name);
                $userProfile->profile_picture = $url;
                $userProfile->save();
            }else {
                return redirect()->back()->withErrors('Image type not supported. All other details have been saved');
            }
            return redirect('/user_profile');
        }catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong.');
        }
    }
}
