<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\InvestorRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\InvestorProfile;
use App\Models\User;
use App\Models\Category;
use App\Models\InvestorInterest;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class InvestorController extends Controller
{

    public function __construct()
    {
        $this->middleware('isUser', ['except' => ['showAllInvestor']]);
    }

    public function index()
    {
        $user = User::find(Session::get('user_id'));
        $investor = InvestorProfile::where('user_id', Session::get('user_id'))->first();
        if($investor) {
            return redirect('/investor');
        }else {
            $categories = Category::all();
            return view('investor_profile', compact(['user', 'categories']));
        }
    }

    public function investor_profile(InvestorRequest $request)
    {
        try 
        {
            $investorprofile = InvestorProfile::where('user_id', Session::get('user_id'))->first();
            if(!$investorprofile) {
                $investorprofile = new InvestorProfile();
            }
            $investorprofile->investor_name = $request->investor_name;
            $investorprofile->email = $request->email;
            $investorprofile->phone = $request->phone;
            $investorprofile->skype = $request->skype;
            $investorprofile->facebook = $request->facebook;
            $investorprofile->twitter = $request->twitter;
            $investorprofile->linkedin = $request->linkedin;
            $investorprofile->min_capital = $request->min_capital;
            $investorprofile->max_capital = $request->max_capital;
            $investorprofile->venture_name = $request->venture_name;
            $investorprofile->user_id = Session::get('user_id');
            $investorprofile->save();


            //saving investor interests industries
            InvestorInterest::where('investor_id', Session::get('user_id'))->delete();
            foreach($request->interests as $interest) {
                $relation = new InvestorInterest();
                $relation->category_id = $interest;
                $relation->investor_id = Session::get('user_id');
                $relation->save();
            }

            //now uploading image and saving it to db
            if($request->image) {
                if($request->image->getMimeType() == 'image/png' || $request->image->getMimeType() == 'image/jpeg' || $request->image->getMimeType() == 'image/gif') {
                    $location = env('LOCATION').'public/images';
                    $ext = $request->image->guessExtension();
                    $name = "image_{$investorprofile->id}_".time().".{$ext}";
                    $url = url()."/images/$name";
                    $image = Image::make($request->image);
                    $image = $this->tweakImage($image, 280, 280);
                    if($image) {
                        $image->save("images/{$name}");
                        //$request->image->move($location, $name);
                        $investorprofile->image = $url;
                        $investorprofile->save();
                    }else {
                        return redirect('/investor')->withErrors('Image height should not be greater than image width');
                    }
                }else {
                    return redirect('/investor')->withErrors('Image type not supported. All other details have been saved');
                }
            }
            else {
                $investorprofile = '';
            }
            return redirect('/investor');

        }
        catch(Exception $e)
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again');            
        }
    }

    public function view()
    {
        try {
            $investor = InvestorProfile::where('user_id', Session::get('user_id'))->first();
            if($investor) {
                $categories = Category::all();
                $tmp = InvestorInterest::where('investor_id', Session::get('user_id'))->get(['category_id']);
                $userCategories = [];
                foreach($tmp as $t) {
                    $userCategories[] = $t->category_id;
                }
                return view('investor.profile', compact(['investor', 'categories', 'userCategories']));
            }else {
                return redirect('/investor-profile');
            }
        }catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

    public function update(InvestorRequest $request)
    {
        try {
            $investorprofile = InvestorProfile::where('user_id', Session::get('user_id'))->first();
            $investorprofile->investor_name = $request->investor_name;
            $investorprofile->email = $request->email;
            $investorprofile->phone = $request->phone;
            $investorprofile->skype = $request->skype;
            $investorprofile->facebook = $request->facebook;
            $investorprofile->twitter = $request->twitter;
            $investorprofile->linkedin = $request->linkedin;
            $investorprofile->min_capital = $request->min_capital;
            $investorprofile->max_capital = $request->max_capital;
            $investorprofile->venture_name = $request->venture_name;
            $investorprofile->user_id = Session::get('user_id');
            $investorprofile->save();
            //saving investor interests
            InvestorInterest::where('investor_id', Session::get('user_id'))->delete();
            foreach($request->interests as $interest) {
                $relation = new InvestorInterest();
                $relation->category_id = $interest;
                $relation->investor_id = Session::get('user_id');
                $relation->save();
            }
            
            return redirect('/investor');
        }catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong.');
        }
    }

    public function upload(Request $request)
    {
        try {
            $investorprofile = InvestorProfile::where('user_id', Session::get('user_id'))->first();
            if($request->image->getMimeType() == 'image/png' || $request->image->getMimeType() == 'image/jpeg' || $request->image->getMimeType() == 'image/gif') {
                $location = env('LOCATION').'public/images';
                $ext = $request->image->guessExtension();
                $name = "image_{$investorprofile->id}_".time().".{$ext}";
                $url = url()."/images/$name";
                $image = Image::make($request->image);
                $image = $this->tweakImage($image, 280, 280);
                if($image) {
                    $image->save("images/{$name}");
                    $investorprofile->image = $url;
                    $investorprofile->save();
                }else {
                    return redirect()->back()->withErrors('Image height should not be greater than image width');
                }
            }else {
                return redirect()->back()->withErrors('Image type not supported. All other details have been saved');
            }

            return redirect('/investor');
        }catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong.');
        }
    }

    public function showAllInvestor()
    {
        try {
            $investors = InvestorProfile::orderBy('id', 'desc')->paginate(16);
            return view('investor.all', compact(['investors']));
        }catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

    public function investorView($id)
    {
        try 
        {
            /*$investor = InvestorProfile::where('investor_profiles.id',$id)
                                        ->join('investor_interests','investor_interests.investor_id','=','investor_profiles.user_id')
                                        ->join('categories','categories.id','=','investor_interests.category_id')
                                        ->Select('investor_profiles.*','categories.category')
                                        ->get();*/
            if(Session::has('user_id')) 
            {
                $investor = InvestorProfile::where('user_id', $id)->first();
                $categories = Category::all();
                $tmp = InvestorInterest::where('investor_id', $id)->get(['category_id']);
                $userCategories = [];
                foreach($tmp as $t) 
                {
                    $userCategories[] = $t->category_id;
                }
                return view('investor.profile', compact(['investor', 'categories', 'userCategories']));
            }
            else 
            {
                return redirect('/investor-profile');
            }   
        }
        catch (Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }     
}