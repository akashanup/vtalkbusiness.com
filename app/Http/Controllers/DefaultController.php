<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\ResetPasswordLink;
use App\Http\Requests\ContactRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\ForgetPasswordRequest;
use App\Http\Requests\InterestRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\EnquiryRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Models\Tags;
use App\Models\Category;
use App\Models\Address;
use App\Models\Company;
use App\Models\Video;
use App\Models\Enquiry;
use App\Models\Interest;
use App\Models\InterestRelation;
use App\Models\Opportunity;
use App\Models\Country;

class DefaultController extends Controller
{

    public function __construct()
    {
        $this->middleware('isNotUser', ['only' => ['register', 'login']]);
        $this->middleware('isUser', ['only' => ['interested']]);
    }

    public function home()
    {
        try{
            $tags           =   Tags::all();
            $category       =   Category::all();
            $cities           =   Address::select('city')->distinct()->get();
            $countries = Country::select('countries.country as country','countries.id as countryId')->distinct()->get();
            $companies = Company::orderBy('id', 'desc')->limit(6)
                                ->join('users', 'users.id', '=', 'companies.user_id')
                                ->select('companies.*', 'users.id AS user_id', 'users.name AS user_name')->get();
            foreach($companies as $k => $company) {
                $co = Country::getCountries($company->id);
                if($co == '') {
                    $co = '&nbsp;';
                }
                $companies[$k]->countries = $co;
            }
            $featured_companies = Company::where('featured', 1)->orderBy('id', 'desc')->limit(6)->join('users', 'users.id', '=', 'companies.user_id')
                                ->select('companies.*', 'users.id AS user_id', 'users.name AS user_name')->get();
            foreach($featured_companies as $k => $company) {
                $co = Country::getCountries($company->id);
                if($co == '') {
                    $co = '&nbsp;';
                }
                $featured_companies[$k]->countries = $co;
            }
            $videos = Video::limit(3)->get();
            //for converting into youtube embed format
            foreach ($videos as $x => $video) {
                $videos[$x]->URL = 'https://youtube.com/embed/'.$this->getYouTubeIdFromURL($videos[$x]->URL);
            }
            $opportunities = Opportunity::join('users', 'users.id', '=', 'opportunities.user_id')->limit(5)->get(['opportunities.id', 'opportunities.headline', 'opportunities.logo', 'users.name AS user_name', 'users.id AS user_id', 'opportunities.capital']);

            return view('home')->with([
                'tags'=>$tags,
                'category' =>$category,
                'cities' => $cities,
                'countries' => $countries, 
                'companies' =>  $companies,
                'featured_companies' => $featured_companies,
                'videos'        =>  $videos,
                'opportunities' =>  $opportunities
            ]);
        }
        catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again');
        }        
    }

    public function company()
    {
        return view('company.fill');
    }

    public function contact()
    {
        return view('contact');
    }

    public function makeContact(ContactRequest $request)
    {
        $json = [];
        try {
            $contact = new Contact();
            $contact->name = $request->name;
            $contact->email = $request->email;
            $contact->phone = $request->phone;
            $contact->message = $request->message;
            $contact->save();
            $json['status'] = 1;
        }catch(Exception $e) {
            $json['status'] = 0;
            $json['message'] = 'Something went wrong';
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }

    public function register()
    {
        return view('/register');
    }

    public function createUser(RegisterRequest $request)
    {
        try {
            $user = User::where('email', $request->email)->first();
            if($user) {
                return redirect()->back()->withErrors('Email already registered.');
            }
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->role = 2;
            $user->save();
            //now crating same user profile
            $userProfile = new UserProfile();
            $userProfile->designation = '';
            $userProfile->phone = '';
            $userProfile->mobile = '';
            $userProfile->skype = '';
            $userProfile->facebook = '';
            $userProfile->instagram = '';
            $userProfile->linkedin = '';
            $userProfile->twitter = '';
            $userProfile->user_id = $user->id;
            $userProfile->profile_picture = url('/images/default_user_image.jpg');
            $userProfile->save();
            //logging in user if regisstration is successful
            Session::put('user_id', $user->id);
            Session::put('user_role', $user->role);
            return redirect('/dashboard');
        }catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');
        }
    }

    public function login()
    {
        return view('/login');
    }

    public function logout()
    {
        Session::flush();
        return redirect('/');
    }

    public function auth(LoginRequest $request)
    {
        try
        {
            $user=User::where('email', $request->email)->first();
            if($user){
                if(Hash::check($request->password, $user->password))
                {
                    Session::put('user_id', $user->id);
                    Session::put('user_role', $user->role);
                    return redirect('dashboard');
                }
                else
                {
                    return redirect()->back()->withErrors('Username and password does not match');
                }
            }
            return redirect()->back()->withErrors('User not found');
        }
        catch(Exception $e)
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');
        }
    }

    public function userForgetPassword()
    {
        return view('forgetPassword');
    }

    public function userGetNewPassword(ForgetPasswordRequest $data)
    {
        try
        {
            $user=User::where('email', $data->email)->first();
            if(count($user))
            {
                $encrypt=md5($user->email.date("d.m.y").date("H:i:s"));
                $string = url().'/reset-password/'.$encrypt;
                try {                    
                    $forgetpassword                 =   new ResetPasswordLink();
                    $forgetpassword->user_id        =   $user->id;
                    $forgetpassword->unique_key     =   $encrypt;
                    $forgetpassword->save();

                    Mail::send('emails.reset-password', array('string' => $string), function ($message) use ($user) {
                        $message->from('vinayksingh2@gmail.com', 'VTalk Business Support'); 
                        $message->to($user->email, $user->name)->subject('VTalkBusiness - Reset Password!');
                        //$message->bcc('akashanup.1993@hotmail.com', 'Akash Anup')->bcc('vicky13b@gmail.com', 'Rupesh Pandey');
                    });

                    return ("Check your mail box to reset your Password!");
                } 
                catch (Exception $e) {
                    return ('Something went wrong. Please try again.');
                }
            }
            else
            {
                return ('Email Id not registered!');
            }
        }
        catch(Exception $e)
        {
            return ('Something went wrong. Please try again.');
        }
    }

    public function userResetPassword($id)
    {
        try {
            $user_id = ResetPasswordLink::where('unique_key',$id)->get()->first();
            if($user_id)
            {
                $expiry_date = $user_id->created_at->addHours(720);
                if(Carbon::now()->lte($expiry_date))
                {
                    $user   =   User::where('id',$user_id->user_id)->get()->first();
                    if($user)
                    {
                        $user_id->UPDATE(array(
                            'created_at'    =>  $user_id->created_at->addHours(-720)
                        ));
                        return view('resetPassword')->with('id',$user->id);
                    }
                    else
                    {
                        return redirect()->back()->withErrors('User not found');
                    }    
                }
                else
                {
                    return view("session-expires");
                }
            }
            else
            {
                return redirect('/dashboard');
            }
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');   
        }
    }

    public function userSaveResetPassword(ResetPasswordRequest $data,$id)
    {
        try {
            $user   =   User::where('id',$id)->first();
            if($user)
            {
                $user  ->  UPDATE(array(
                    'password'  => Hash::make($data->password)
                ));
            }
            else
            {
                return redirect()->back()->withErrors('User not found');
            }            
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');   
        }
    }

    public function enquiry(EnquiryRequest $request)
    {
        try {
            $enquiry = new Enquiry();
            $enquiry->name = $request->name;
            $enquiry->email = $request->email;
            $enquiry->phone = $request->phone;
            $enquiry->enquiry = $request->enquiry;
            $enquiry->requirement = $request->requirement;
            $enquiry->save();
            return redirect('/');
        }catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong.');
        }
    }

    public function interested(InterestRequest $request)
    {
        $json = [];
        try {
            //check if interest exists then return
            if(InterestRelation::userInterestIn($request->entity_id, $request->entity_type)) {
                $json['status'] = 0;
                $json['message'] = 'Interest Already Exists';
            }else {
                $interest = new InterestRelation();
                $interest->user_id = Session::get('user_id');
                $interest->entity_id = $request->entity_id;
                $interest->entity_type = $request->entity_type;
                $interest->save();
                $json['status'] = 1;
                $josn['message'] = 'Interest Recorded';
            }
        }catch(Exception $e) {
            $json['status'] = 0;
            $json['message'] = 'Something went wrong';
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }

    public function notInterested(InterestRequest $request)
    {
        $json = [];
        try {
            //check if interest exists then return
            if(!InterestRelation::userInterestIn($request->entity_id, $request->entity_type)) {
                $json['status'] = 0;
                $json['message'] = 'No Interest Exists';
            }else {
                $interest = InterestRelation::userInterest($request->entity_id, $request->entity_type)->delete();
                $json['status'] = 1;
                $josn['message'] = 'Interest Recorded';
            }
        }catch(Exception $e) {
            $json['status'] = 0;
            $json['message'] = 'Something went wrong';
        }
        return response()->json($json)->header('Content-Type', 'application/json');
    }

    public function getYouTubeIdFromURL($url)
    {
        preg_match_all("#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#", $url, $matches);
        return $matches[0][0];
    }
}

