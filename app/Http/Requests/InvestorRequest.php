<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InvestorRequest extends Request
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'min_capital'       =>  'required|numeric',
			'max_capital'       =>  'required|numeric',
			'investor_name'     =>  'required|string',
			'venture_name'      =>  'string',
			'email'             =>  'email',
			'number'            =>  'string',
			'skype'             =>  'string',
			'facebook'          =>  'url',
			'twitter'           =>  'url',
			'linkedin'          =>  'url',
			'interests'			=>	'required|array'
		];
	}
}
