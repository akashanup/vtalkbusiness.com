<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CompanyStepThreeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products'      =>  'required|array',
            'info'          =>  'required|array',
            'names'         =>  'required|array',
            'designations'  =>  'required|array',
            'emails'        =>  'required|array',
            'phones'        =>  'required|array',
            'company_id'    =>  'required|numeric',
            'designation'   =>  'required|string',
        ];
    }

    public function response(array $errors)
    {
        return response()->view('company.step3', ['errors' => $errors])->header('Content-Type', 'text/html');
    }
}
