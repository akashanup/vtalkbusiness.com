<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CompanyStepFourRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee'      =>  'required|string',
            'facebook'      =>  'url',
            'linkedin'      =>  'url',
            'twitter'       =>  'url',
            'company_id'    =>  'required|numeric',
            'description'   =>  'required|string'
        ];
    }

    public function response(array $errors)
    {
        return response()->view('company.step4', ['errors' => $errors])->header('Content-Type', 'text/html');
    }
}
