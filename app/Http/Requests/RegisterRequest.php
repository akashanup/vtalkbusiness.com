<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      =>  'required|string',
            'email'     =>  'required|email',
            'password'  =>  'required|string|min:8',
            'password_confirmation' =>  'required|same:password',
            'agree'     =>  'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'agree.required' => 'You must agree to our terms and conditions.',
            'password_confirmation.same'    =>  'Both Password should match',
        ];
    }
}
