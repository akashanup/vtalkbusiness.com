<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditCompanyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             =>  'required|string'    ,
            'email'            =>  'required|email'     ,
            'phone'            =>  'required|numeric'   ,
            'evaluation'       =>  'required|numeric'   ,
            'facebook'         =>  'required|url'       ,
            'linkedin'         =>  'required|url'       ,
            'twitter'          =>  'required|url'       
        ];
    }
}
