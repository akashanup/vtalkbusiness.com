<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CompanyStepOneRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  =>  'required|string'    ,
            'email'                 =>  'required|email'     ,
            'company_cin'           =>  'required|string'    ,
            'phone'                 =>  'required|string'           ,
            'evaluation'            =>  'required|numeric|min:0'    ,
            'tagline'               =>  'required|string'
        ];
    }
}
