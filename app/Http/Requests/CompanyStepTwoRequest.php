<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Tags;
use App\Models\Category;
use App\Models\Country;

class CompanyStepTwoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category'  =>  'required|array',
            'company_id'=>  'required|numeric',
            'tags'      =>  'required|array',
            'x'         =>  'required_with:cover',
            'y'         =>  'required_with:cover',
            'w'         =>  'required_with:cover',
            'h'         =>  'required_with:cover',
            'street'    =>  'array',
            'city'      =>  'array',
            'state'     =>  'array',
            'pin'       =>  'array',
            'country'   =>  'required',
            'turnover'  =>  'array',
            'year'      =>  'array'
        ];
    }

    public function response(array $errors)
    {
        $tags       = Tags::all();
        $categories = Category::all();
        $country    = Country::all();   
        return response()->view('company.step2', ['errors' => $errors, 'tags' => $tags, 'categories' => $categories, 'country' => $country])->header('Content-Type', 'text/html');
    }
}
