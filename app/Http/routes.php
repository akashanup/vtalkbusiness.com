<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'DefaultController@home');

Route::get('/company/add', 'CompanyController@step1');
Route::post('/company/add/1', 'CompanyController@saveStep1');
Route::post('/company/add/2', 'CompanyController@saveStep2');
Route::post('/company/add/3', 'CompanyController@saveStep3');
Route::post('/company/add/4', 'CompanyController@saveStep4');


Route::get('/contact', 'DefaultController@contact');
Route::post('/contact', 'DefaultController@makeContact');

Route::get('/register', 'DefaultController@register');
Route::post('/register', 'DefaultController@createUser');

Route::get('/login', 'DefaultController@login');
Route::post('/login','DefaultController@auth');
Route::any('/logout', 'DefaultController@logout');
Route::post('/enquiry', 'DefaultController@enquiry');

Route::get('/dashboard', 'UserController@dashboard');

Route::get('/company/{id}', 'CompanyController@showCompany');
Route::post('/company/{id}','CompanyController@saveCompany');
Route::post('/company/{id}/editAddress', 'CompanyController@editAddress');
Route::post('/company/{id}/saveEditedTurnover', 'CompanyController@saveEditedTurnover');
Route::post('/company/{id}/saveProduct', 'CompanyController@saveProduct');
Route::post('/company/{id}/saveProfilePicture', 'CompanyController@saveProfilePicture');
Route::post('/company/{id}/saveCoverImage', 'CompanyController@saveCoverImage');
Route::post('/company/{id}/editCompany', 'CompanyController@editCompany');
Route::post('/company/{id}/editTeam', 'CompanyController@editTeam');
Route::post('/company/{id}/editTags', 'CompanyController@editTags');
Route::post('/company/{id}/editTurnover', 'CompanyController@editTurnover');
Route::get('/my-companies', 'CompanyController@myCompanies');

Route::get('/change-password','UserController@userChangePassword');
Route::post('/change-password','UserController@userSaveChangePassword');

Route::get('/forget-password','DefaultController@userForgetPassword');
Route::post('/forget-password','DefaultController@userGetNewPassword');

Route::get('/reset-password/{id}','DefaultController@userResetPassword');
Route::post('/reset-password/{id}','DefaultController@userSaveResetPassword');

Route::get('/company/{id}/add-member', 'CompanyController@addMemeber');
Route::post('/company/{id}/add-member', 'CompanyController@saveMemeber');

Route::get('/investor-profile', 'InvestorController@index');
Route::post('/investor-profile', 'InvestorController@investor_profile');
Route::get('/investor', 'InvestorController@view');
Route::post('/investor', 'InvestorController@update');
Route::get('/investor/{id}', 'InvestorController@investorView');

Route::post('/investor/picture/upload', 'InvestorController@upload');

Route::get('/search','CompanyController@getCompany');
Route::post('/findCompany','CompanyController@findCompany');
Route::get('/user_profile', 'UserController@user_profile');
Route::post('/user_profile', 'UserController@saveUser');
Route::post('/user/picture/upload', 'UserController@upload');


Route::get('/admin/companies', 'AdminController@fetchAllCompanies');
Route::post('/admin/delete-companies', 'AdminController@deleteCompany');
Route::post('/admin/company-ban-status', 'AdminController@banCompany');
Route::post('/admin/add-featured', 'AdminController@addFeatured');

Route::get('/admin/investors', 'AdminController@fetchAllInvestors');
Route::post('/admin/delete-investor', 'AdminController@deleteInvestor');
Route::post('/admin/investor-ban-status', 'AdminController@banInvestor');


Route::get('/admin/users', 'AdminController@fetchAllUsers');
Route::post('/admin/delete-user', 'AdminController@deleteUser');
Route::post('/admin/user-ban-status', 'AdminController@banUser');

Route::get('/admin/contacts', 'AdminController@fetchAllContacts');
Route::get('/admin/enquiries', 'AdminController@fetchAllEnquiries');
Route::get('/admin/videos', 'AdminController@fetchAllVideos');
Route::post('/admin/delete-video', 'AdminController@deleteVideo');


Route::POST('/admin/videos/save', 'AdminController@saveVideo');

Route::get('/companies','CompanyController@showAllCompany');
Route::get('/investors','InvestorController@showAllInvestor');

Route::post('/interested', 'DefaultController@interested');
Route::post('/not-interested', 'DefaultController@notInterested');

Route::get('/opportunity','OpportunityController@getOpportunity');
Route::post('/opportunity','OpportunityController@postOpportunity');
Route::get('/showOpportunities','OpportunityController@showOpportunities');
Route::post('/opportunity-details/{id}','OpportunityController@detailsOpportunity');
Route::get('/my-opportunities','OpportunityController@myOpportunities');

Route::get('/make-payment/{id}','OpportunityController@getPayment');
Route::post('/make-payment/{id}','OpportunityController@postPayment');
/*Event::listen('illuminate.query', function($query)
{
    var_dump($query);
});*/

