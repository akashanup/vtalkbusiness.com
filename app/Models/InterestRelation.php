<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interest;
use App\Models\InterestRelation;
use Illuminate\Support\Facades\Session;

class InterestRelation extends Model
{
    public static function userInterestIn($entityId, $entityType)
    {
    	try {
    		$interest = InterestRelation::where('user_id', Session::get('user_id'))->where('entity_id', $entityId)->where('entity_type', $entityType)->first();
    		if($interest) {
    			return true;
    		}
    	}catch(Exception $e) {
    		$json = 'Something went wrong';
    	}
    	return false;
    }

    public function scopeUserInterest($query, $entityId, $entityType)
    {
    	$query->where('user_id', Session::get('user_id'))->where('entity_id', $entityId)->where('entity_type', $entityType);
    }

    public function scopeOwnInterest($query)
    {
        $query->where('entity_id', Session::get('user_id'))->where('entity_type', 1);
    }

    public function scopeOpportunityInterest($query, $entityId)
    {
        $query->where('entity_id', $entityId)->where('entity_type', 5);
    }

    public function scopeCompanyInterest($query, $entityId)
    {
        $query->where('entity_id', $entityId)->where('entity_type', 2);
    }
}
