<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Address;

class Country extends Model
{
    public static function getCountries($companyId)
    {
    	$countries = [];
    	$c = Address::where('company_id', $companyId)->join('countries', 'countries.id', '=', 'addresses.country')->get(['countries.country']);
    	foreach($c as $s) {
    		$countries[] = $s->country;
    	}
    	return implode($countries, ', ');
    }
}
