<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyTurnover extends Model
{
    protected $fillable=['last_turnover','second_last_turnover','third_last_turnover'];
}
