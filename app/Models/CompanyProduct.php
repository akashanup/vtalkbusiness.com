<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyProduct extends Model
{
 	protected $fillable=['product_name','product_information'];
}
