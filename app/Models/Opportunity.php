<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\OpportunityCompanyRelationship;

class Opportunity extends Model
{
 	protected $fillable=['status'];

 	public static function getCompanies($opportunityId)
    {
    	$companies = [];
    	$c = OpportunityCompanyRelationship::where('opportunity_id', $opportunityId)->join('companies', 'companies.id', '=', 'opportunity_company_relationships.company_id')->get(['companies.name']);
    	foreach($c as $s) {
    		$companies[] = $s->name;
    	}
    	return implode($companies, ', ');
    }
}
